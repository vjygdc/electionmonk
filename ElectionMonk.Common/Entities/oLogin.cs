﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oLogin
/// </summary>
public class oLogin
{
    public oLogin()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region User Properties
    private int userId;
    private string userName;
    private string userMailId;
    private Int64 userMobile;
    private string userGender;
    private string userType;
    private string userPwd;
    private Int64 usersCount;
    private int userStatus;
    private DateTime creaDate;
    private DateTime updaDate;
    private string creaBy;
    private string updaBy;

    public int UserId
    {
        get
        {
            return userId;
        }

        set
        {
            userId = value;
        }
    }

    public string UserName
    {
        get
        {
            return userName;
        }

        set
        {
            userName = value;
        }
    }

    public string UserMailId
    {
        get
        {
            return userMailId;
        }

        set
        {
            userMailId = value;
        }
    }

    public long UserMobile
    {
        get
        {
            return userMobile;
        }

        set
        {
            userMobile = value;
        }
    }

    public string UserGender
    {
        get
        {
            return userGender;
        }

        set
        {
            userGender = value;
        }
    }

    public string UserType
    {
        get
        {
            return userType;
        }

        set
        {
            userType = value;
        }
    }

    public string UserPwd
    {
        get
        {
            return userPwd;
        }

        set
        {
            userPwd = value;
        }
    }

    public long UsersCount
    {
        get
        {
            return usersCount;
        }

        set
        {
            usersCount = value;
        }
    }

    public int UserStatus
    {
        get
        {
            return userStatus;
        }

        set
        {
            userStatus = value;
        }
    }

    public DateTime CreaDate
    {
        get
        {
            return creaDate;
        }

        set
        {
            creaDate = value;
        }
    }

    public DateTime UpdaDate
    {
        get
        {
            return updaDate;
        }

        set
        {
            updaDate = value;
        }
    }

    public string CreaBy
    {
        get
        {
            return creaBy;
        }

        set
        {
            creaBy = value;
        }
    }

    public string UpdaBy
    {
        get
        {
            return updaBy;
        }

        set
        {
            updaBy = value;
        }
    }


    #endregion
}