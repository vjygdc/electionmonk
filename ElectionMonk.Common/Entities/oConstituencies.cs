﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oConstituencies
/// </summary>
public class oConstituencies
{
	public oConstituencies()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    int _CONSTI_ID;
    String _CONSTI_NAME;
    int _DISTRICT_ID;
    int _STATUS;
    String _CREA_BY;
    String _CREA_DATE;
    String UPDA_BY;
    String _UPDA_DATE;
    string _filter;
    public int CONSTI_ID
    {
        get
        {
            return _CONSTI_ID;
        }

        set
        {
            _CONSTI_ID = value;
        }
    }

    public string CONSTI_NAME
    {
        get
        {
            return _CONSTI_NAME;
        }

        set
        {
            _CONSTI_NAME = value;
        }
    }

    public int DISTRICT_ID
    {
        get
        {
            return _DISTRICT_ID;
        }

        set
        {
            _DISTRICT_ID = value;
        }
    }

    public int STATUS
    {
        get
        {
            return _STATUS;
        }

        set
        {
            _STATUS = value;
        }
    }

    public string CREA_BY
    {
        get
        {
            return _CREA_BY;
        }

        set
        {
            _CREA_BY = value;
        }
    }

    public string CREA_DATE
    {
        get
        {
            return _CREA_DATE;
        }

        set
        {
            _CREA_DATE = value;
        }
    }

    public string UPDA_BY1
    {
        get
        {
            return UPDA_BY;
        }

        set
        {
            UPDA_BY = value;
        }
    }

    public string UPDA_DATE
    {
        get
        {
            return _UPDA_DATE;
        }

        set
        {
            _UPDA_DATE = value;
        }
    }

    public string Filter
    {
        get
        {
            return _filter;
        }

        set
        {
            _filter = value;
        }
    }
}