﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oDistrict
/// </summary>
public class oDistrict
{
    public oDistrict()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private int dISTRICT_ID;
    private string dISTRICT_NAME;
    private int sTATE_ID;
    private int sTATUS;
    private string cREA_BY;
    private DateTime cREA_DATE;
    private string uPDA_BY;
    private DateTime uPDA_DATE;

    public int DISTRICT_ID
    {
        get
        {
            return dISTRICT_ID;
        }

        set
        {
            dISTRICT_ID = value;
        }
    }

    public string DISTRICT_NAME
    {
        get
        {
            return dISTRICT_NAME;
        }

        set
        {
            dISTRICT_NAME = value;
        }
    }

    public int STATE_ID
    {
        get
        {
            return sTATE_ID;
        }

        set
        {
            sTATE_ID = value;
        }
    }

    public int STATUS
    {
        get
        {
            return sTATUS;
        }

        set
        {
            sTATUS = value;
        }
    }

    public string CREA_BY
    {
        get
        {
            return cREA_BY;
        }

        set
        {
            cREA_BY = value;
        }
    }

    public DateTime CREA_DATE
    {
        get
        {
            return cREA_DATE;
        }

        set
        {
            cREA_DATE = value;
        }
    }

    public string UPDA_BY
    {
        get
        {
            return uPDA_BY;
        }

        set
        {
            uPDA_BY = value;
        }
    }

    public DateTime UPDA_DATE
    {
        get
        {
            return uPDA_DATE;
        }

        set
        {
            uPDA_DATE = value;
        }
    }
    string _DISTRICT_IMG;
    public string DISTRICT_IMG
    {
        get
        {
            return _DISTRICT_IMG;
        }

        set
        {
            _DISTRICT_IMG = value;
        }
    }
}