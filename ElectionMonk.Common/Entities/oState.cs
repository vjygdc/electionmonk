﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oState
/// </summary>
public class oState
{
    public oState()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    int _STATE_ID;
    string _STATE_NAME;
    string _CREA_BY; string _CREA_DATE; string _UPDA_DATE; string _UPDA_BY;int _STATUS;

    public int STATE_ID
    {
        get { return _STATE_ID; }
        set { _STATE_ID = value; }
    }
    public string STATE_NAME
    {
        get { return _STATE_NAME; }
        set { _STATE_NAME = value; }
    }
    public string CREA_BY
    {
        get { return _CREA_BY; }
        set { _CREA_BY = value; }
    }
    public string CREA_DATE
    {
        get { return _CREA_DATE; }
        set { _CREA_DATE = value; }
    }
    public string UPDA_DATE
    {
        get { return _UPDA_DATE; }
        set { _UPDA_DATE = value; }
    }
    public string UPDA_BY
    {
        get { return _UPDA_BY; }
        set { _UPDA_BY = value; }
    }
    public int STATUS
    {
        get { return _STATUS; }
        set { _STATUS = value; }
    }

}