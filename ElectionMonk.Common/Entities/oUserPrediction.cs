﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oUserPrediction
/// </summary>
public class oUserPrediction
{
    public oUserPrediction()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private int uR_ID;
    private int uSER_LOGIN_ID;
    private string tRS;
    private string bJP;
    private string kUTAMI;
    private string oTHERS;
    private int uR_STATUS;
    private string cREA_DATE;

    public int UR_ID
    {
        get
        {
            return uR_ID;
        }

        set
        {
            uR_ID = value;
        }
    }

    public int USER_LOGIN_ID
    {
        get
        {
            return uSER_LOGIN_ID;
        }

        set
        {
            uSER_LOGIN_ID = value;
        }
    }

    public string TRS
    {
        get
        {
            return tRS;
        }

        set
        {
            tRS = value;
        }
    }

    public string BJP
    {
        get
        {
            return bJP;
        }

        set
        {
            bJP = value;
        }
    }

    public string KUTAMI
    {
        get
        {
            return kUTAMI;
        }

        set
        {
            kUTAMI = value;
        }
    }

    public string OTHERS
    {
        get
        {
            return oTHERS;
        }

        set
        {
            oTHERS = value;
        }
    }

    public int UR_STATUS
    {
        get
        {
            return uR_STATUS;
        }

        set
        {
            uR_STATUS = value;
        }
    }

    public string CREA_DATE
    {
        get
        {
            return cREA_DATE;
        }

        set
        {
            cREA_DATE = value;
        }
    }
}