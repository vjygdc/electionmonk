﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oConstitution
/// </summary>
public class oConstitution
{
    public oConstitution()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    

    private int _CONSTI_ID;
    private int _DISTRICT_ID;
    private int _STATE_ID;
    private string _CONSTI_NAME;
    private string _DISTRICT_NAME;
    private string _STATE_NAME;
    private int _STATUS;
    private string _CREA_BY;
    private DateTime _CREA_DATE;
    private string _UPDA_BY;
    private DateTime _UPDA_DATE;
    private int _CONST_NUMBER;
  



    public int CONSTI_ID
    {
        get
        {
            return _CONSTI_ID;
        }

        set
        {
            _CONSTI_ID = value;
        }
    }

    public string CONSTI_NAME
    {
        get
        {
            return _CONSTI_NAME;
        }

        set
        {
            _CONSTI_NAME = value;
        }
    }

    public int DISTRICT_ID
    {
        get
        {
            return _DISTRICT_ID;
        }

        set
        {
            _DISTRICT_ID = value;
        }
    }

    public int STATUS
    {
        get
        {
            return _STATUS;
        }

        set
        {
            _STATUS = value;
        }
    }

    public string CREA_BY
    {
        get
        {
            return _CREA_BY;
        }

        set
        {
            _CREA_BY = value;
        }
    }

    public DateTime CREA_DATE
    {
        get
        {
            return _CREA_DATE;
        }

        set
        {
            _CREA_DATE = value;
        }
    }

    public string UPDA_BY
    {
        get
        {
            return _UPDA_BY;
        }

        set
        {
            _UPDA_BY = value;
        }
    }

    public DateTime UPDA_DATE
    {
        get
        {
            return _UPDA_DATE;
        }

        set
        {
            _UPDA_DATE = value;
        }
    }

    public string DISTRICT_NAME
    {
        get
        {
            return _DISTRICT_NAME;
        }

        set
        {
            _DISTRICT_NAME = value;
        }
    }

    public int STATE_ID
    {
        get
        {
            return _STATE_ID;
        }

        set
        {
            _STATE_ID = value;
        }
    }

    public string STATE_NAME
    {
        get
        {
            return _STATE_NAME;
        }

        set
        {
            _STATE_NAME = value;
        }
    }

    public int CONST_NUMBER
    {
        get
        {
            return _CONST_NUMBER;
        }

        set
        {
            _CONST_NUMBER = value;
        }
    }
}