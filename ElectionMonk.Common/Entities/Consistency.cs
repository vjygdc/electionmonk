﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConsistencyDAL
/// </summary>
public class Consistency
{
    public Consistency()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    int Consistency_id;
    int _Constid;
    int _UserID;
    int _SHORT_CODE;
    int count;
    int party;
    int _STATE;
    int _DISTRICT;
    int _CONSISTENCY;
    string _TRS;
    string _BJP;
    string _KUTAMI;
    string _OTHERS;
    string _Status;
    string _Statename;
    string _Dist_name;
    string _Const_name;
    int _ID;

    public int STATE
    {
        get
        {
            return _STATE;
        }

        set
        {
            _STATE = value;
        }
    }

    public int DISTRICT
    {
        get
        {
            return _DISTRICT;
        }

        set
        {
            _DISTRICT = value;
        }
    }

    public int CONSISTENCY
    {
        get
        {
            return _CONSISTENCY;
        }

        set
        {
            _CONSISTENCY = value;
        }
    }

    public string TRS
    {
        get
        {
            return _TRS;
        }

        set
        {
            _TRS = value;
        }
    }

    public string BJP
    {
        get
        {
            return _BJP;
        }

        set
        {
            _BJP = value;
        }
    }

    public string KUTAMI
    {
        get
        {
            return _KUTAMI;
        }

        set
        {
            _KUTAMI = value;
        }
    }

    public string OTHERS
    {
        get
        {
            return _OTHERS;
        }

        set
        {
            _OTHERS = value;
        }
    }

    public string Status
    {
        get
        {
            return _Status;
        }

        set
        {
            _Status = value;
        }
    }

    public string Statename
    {
        get
        {
            return _Statename;
        }

        set
        {
            _Statename = value;
        }
    }

    public string Dist_name
    {
        get
        {
            return _Dist_name;
        }

        set
        {
            _Dist_name = value;
        }
    }

    public string Const_name
    {
        get
        {
            return _Const_name;
        }

        set
        {
            _Const_name = value;
        }
    }

    public int ID
    {
        get
        {
            return _ID;
        }

        set
        {
            _ID = value;
        }
    }

    public int Party
    {
        get
        {
            return party;
        }

        set
        {
            party = value;
        }
    }

    public int Count
    {
        get
        {
            return count;
        }

        set
        {
            count = value;
        }
    }

    public int SHORT_CODE
    {
        get
        {
            return _SHORT_CODE;
        }

        set
        {
            _SHORT_CODE = value;
        }
    }

    public int UserID
    {
        get
        {
            return _UserID;
        }

        set
        {
            _UserID = value;
        }
    }

    public int Consistency_id1
    {
        get
        {
            return Consistency_id;
        }

        set
        {
            Consistency_id = value;
        }
    }

    public int Constid
    {
        get
        {
            return _Constid;
        }

        set
        {
            _Constid = value;
        }
    }
}