﻿using System;

namespace ElectionMonk.Common.Entities
{
    public class Candidate
    {
        public int CandidateID { get; set; }
       
        public string CandidateName { get; set; }

        public string CandidateImg { get; set; }

        public int Status { get; set; }

        public int PartyID { get; set; }

        public int ConstituencyID { get; set; }

        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public string PartyName { get; set; }

        public string ConstituencyName { get; set; }

        public string CreatedBy { get; set; }

        //public string Constname
        //{
        //    get { return constname; }

        //    set { constname = value; }
        //}

        public string District { get; set; }
    }
}