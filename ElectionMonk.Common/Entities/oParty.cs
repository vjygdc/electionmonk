﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for oParty
/// </summary>
public class oParty
{
    public oParty()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private int pARTY_ID;
    private string pARTY_NAME;
    private string pARTY_LOGO;
    private int sTATUS;
    private string cREA_BY;
    private DateTime cREA_DATE;
    private string uPDA_BY;
    private DateTime uPDA_DATE;

    public int PARTY_ID
    {
        get
        {
            return pARTY_ID;
        }

        set
        {
            pARTY_ID = value;
        }
    }

    public string PARTY_NAME
    {
        get
        {
            return pARTY_NAME;
        }

        set
        {
            pARTY_NAME = value;
        }
    }

    public string PARTY_LOGO
    {
        get
        {
            return pARTY_LOGO;
        }

        set
        {
            pARTY_LOGO = value;
        }
    }

    public int STATUS
    {
        get
        {
            return sTATUS;
        }

        set
        {
            sTATUS = value;
        }
    }

    public string CREA_BY
    {
        get
        {
            return cREA_BY;
        }

        set
        {
            cREA_BY = value;
        }
    }

    public DateTime CREA_DATE
    {
        get
        {
            return cREA_DATE;
        }

        set
        {
            cREA_DATE = value;
        }
    }

    public string UPDA_BY
    {
        get
        {
            return uPDA_BY;
        }

        set
        {
            uPDA_BY = value;
        }
    }

    public DateTime UPDA_DATE
    {
        get
        {
            return uPDA_DATE;
        }

        set
        {
            uPDA_DATE = value;
        }
    }
}