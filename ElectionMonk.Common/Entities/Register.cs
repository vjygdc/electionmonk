﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Register
/// </summary>
public class Register
{
    public Register()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region 
    string _CASTE_CATEGORY;
    int _Reg_id;
    int count;
    string _EMAIL;
    string _NAME;
    string _DOB;
    string _GENDER;
    string _PASSWORDS;
    bool _UserNameInUse;
    string _Subject;
    string _BodyMsg;
    string _Mobile;

    public string EMAIL
    {
        get
        {
            return _EMAIL;
        }

        set
        {
            _EMAIL = value;
        }
    }

    public string NAME
    {
        get
        {
            return _NAME;
        }

        set
        {
            _NAME = value;
        }
    }

    public string DOB
    {
        get
        {
            return _DOB;
        }

        set
        {
            _DOB = value;
        }
    }

    public string GENDER
    {
        get
        {
            return _GENDER;
        }

        set
        {
            _GENDER = value;
        }
    }

    public string PASSWORDS
    {
        get
        {
            return _PASSWORDS;
        }

        set
        {
            _PASSWORDS = value;
        }
    }

    public bool UserNameInUse
    {
        get
        {
            return _UserNameInUse;
        }

        set
        {
            _UserNameInUse = value;
        }
    }

    public int Reg_id
    {
        get
        {
            return _Reg_id;
        }

        set
        {
            _Reg_id = value;
        }
    }
    public int Count
    {
        get
        {
            return count;
        }

        set
        {
            count = value;
        }
    }

    public string Subject
    {
        get
        {
            return _Subject;
        }

        set
        {
            _Subject = value;
        }
    }

    public string BodyMsg
    {
        get
        {
            return _BodyMsg;
        }

        set
        {
            _BodyMsg = value;
        }
    }

    public string Mobile
    {
        get
        {
            return _Mobile;
        }

        set
        {
            _Mobile = value;
        }
    }

    public string CASTE_CATEGORY
    {
        get
        {
            return _CASTE_CATEGORY;
        }

        set
        {
            _CASTE_CATEGORY = value;
        }
    }
    #endregion
}