﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserDAL
/// </summary>
public class UserDAL
{
    public UserDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void InsertGridview(Consistency register)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("USP_INSERT_CONSISTENCYS");
        DB.AddInParameter(dbCom, "@CONSISTENCY_ID", DbType.Int32, register.Constid);
        DB.AddInParameter(dbCom, "@TRS", DbType.String, register.TRS);
        DB.AddInParameter(dbCom, "@BJP", DbType.String, register.BJP);
        DB.AddInParameter(dbCom, "@KUTAMI", DbType.String, register.KUTAMI);
        DB.AddInParameter(dbCom, "@OTHRS", DbType.String, register.OTHERS);
        DB.AddInParameter(dbCom, "@E_STATUS", DbType.String, register.Status);
        DB.AddInParameter(dbCom, "@CONSISTUENCY_NAME", DbType.String, register.Const_name);
        DB.AddInParameter(dbCom, "@SHORT_CODE", DbType.Int32, register.SHORT_CODE);
        DB.AddInParameter(dbCom, "@USER_ID", DbType.Int32, register.UserID);
       

        DB.ExecuteNonQuery(dbCom);
    }

    public List<Consistency> RetrieveUserconsistencies(Consistency consistency)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();

            DbCommand dbCom = DB.GetStoredProcCommand("USP_RETRIEVE_TEMPRORYCONSISTENCYSS");
            DB.AddInParameter(dbCom, "@SHORT_CODE", DbType.Int32, consistency.SHORT_CODE);
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<Consistency> objEventslist = Objds.Tables[0].AsEnumerable().Select(row => new Consistency
            {

                Constid = row.Field<Int32?>("CONSISTENCY_ID").GetValueOrDefault(),
                CONSISTENCY = row.Field<Int32?>("CONSTITUENCY_NAME").GetValueOrDefault(),
                TRS = String.IsNullOrEmpty(row.Field<string>("TRS")) ? "" : row.Field<string>("TRS"),
                BJP = String.IsNullOrEmpty(row.Field<string>("BJP")) ? "" : row.Field<string>("BJP"),
                KUTAMI = String.IsNullOrEmpty(row.Field<string>("KUTAMI")) ? "" : row.Field<string>("KUTAMI"),
                OTHERS = String.IsNullOrEmpty(row.Field<string>("OTHERS")) ? "" : row.Field<string>("OTHERS"),
                Status = String.IsNullOrEmpty(row.Field<string>("E_STATUS")) ? "" : row.Field<string>("E_STATUS"),
                
                Const_name = String.IsNullOrEmpty(row.Field<string>("CONSTITUENCY_NAME")) ? "" : row.Field<string>("CONSTITUENCY_NAME"),


            }).ToList();
            return objEventslist;
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}