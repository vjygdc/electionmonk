﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConsistencyDAL
/// </summary>
public class ConsistencyDAL
{
    public ConsistencyDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<Consistency> GetConsistencies(oConstituencies obj)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_RETRIEVE_CONSTITENCY_ADMIN");

            DB.AddInParameter(dbCom, "@Filter", DbType.String,obj.Filter);
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<Consistency> objwishselect = Objds.Tables[0].AsEnumerable().Select(row => new Consistency
            {

                ID = row.Field<Int32?>("ID").GetValueOrDefault(),
                BJP = String.IsNullOrEmpty(row.Field<string>("BJP")) ? "" : row.Field<string>("BJP"),

                KUTAMI = String.IsNullOrEmpty(row.Field<string>("KUTAMI")) ? "" : row.Field<string>("KUTAMI"),
                TRS = String.IsNullOrEmpty(row.Field<string>("TRS")) ? "" : row.Field<string>("TRS"),
                OTHERS = String.IsNullOrEmpty(row.Field<string>("OTHERS")) ? "" : row.Field<string>("OTHERS"),
                Status = String.IsNullOrEmpty(row.Field<string>("E_STATUS")) ? "" : row.Field<string>("E_STATUS"),

                Statename = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
                Dist_name = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                Const_name = String.IsNullOrEmpty(row.Field<string>("CONSTITUENCY_NAME")) ? "" : row.Field<string>("CONSTITUENCY_NAME"),
            }).ToList();

            return objwishselect;
        }
        catch (Exception ex)
        {
            throw ex; ;
        }
    }

    public int UpdateConsistency(Consistency consistency)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_UPDATE_CONSISTENCY");
            DB.AddInParameter(dbCom, "@ID", DbType.Int32, consistency.ID);
            DB.AddInParameter(dbCom, "@E_STATUS", DbType.String, consistency.Status);
            DB.AddInParameter(dbCom, "@TRS", DbType.String, consistency.TRS);
            DB.AddInParameter(dbCom, "@BJP", DbType.String, consistency.BJP);
            DB.AddInParameter(dbCom, "@KUTAMI", DbType.String, consistency.KUTAMI);
            DB.AddInParameter(dbCom, "@STATE_NAME", DbType.String, consistency.Statename);
            DB.AddInParameter(dbCom, "@DISTRICT_NAME", DbType.String, consistency.Dist_name);
            DB.AddInParameter(dbCom, "@CONSTITUENCY_NAME", DbType.String, consistency.Const_name);
            DB.AddInParameter(dbCom, "@OTHERS", DbType.String, consistency.OTHERS);

            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void DeleteConsistencyyy(Consistency consistency)
    {

        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("ASP_DELETE_CONSISTENCY");
        DB.AddInParameter(dbCom, "@ID", DbType.Int32, consistency.ID);
        DB.ExecuteNonQuery(dbCom);
    }
    public void EditConsistency(Consistency consistency)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_EDIT_CONSISTENCY");
            DB.AddInParameter(editcmd, "@ID", DbType.String, consistency.ID);
            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {
                consistency.STATE = Convert.ToInt16(rdr["STATE"]);
                consistency.DISTRICT = Convert.ToInt16(rdr["DISTRICT"]);
                consistency.CONSISTENCY = Convert.ToInt16(rdr["CONSISTENCY"]);
                consistency.TRS = rdr["TRS"].ToString();
                consistency.BJP = rdr["BJP"].ToString();
                consistency.KUTAMI = rdr["KUTAMI"].ToString();
                consistency.OTHERS = rdr["OTHERS"].ToString();
                consistency.Status = rdr["E_STATUS"].ToString();
                consistency.Statename = rdr["STATE_NAME"].ToString();
                consistency.Dist_name = rdr["DISTRICT_NAME"].ToString();
                consistency.Const_name = rdr["CONSTITUENCY_NAME"].ToString();
               

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<Consistency> Retrieveconsistencies(Consistency consistency)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();

            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_CONSISTENCY_DETAILS");
            DB.AddInParameter(dbCom, "@DIST_ID", DbType.Int16, consistency.DISTRICT);
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<Consistency> objEventslist = Objds.Tables[0].AsEnumerable().Select(row => new Consistency
            {

                ID = row.Field<Int32?>("ID").GetValueOrDefault(),
                CONSISTENCY = row.Field<Int32?>("CONSISTENCY").GetValueOrDefault(),
                TRS = String.IsNullOrEmpty(row.Field<string>("TRS")) ? "" : row.Field<string>("TRS"),
                BJP = String.IsNullOrEmpty(row.Field<string>("BJP")) ? "" : row.Field<string>("BJP"),
                KUTAMI = String.IsNullOrEmpty(row.Field<string>("KUTAMI")) ? "" : row.Field<string>("KUTAMI"),
                OTHERS = String.IsNullOrEmpty(row.Field<string>("OTHERS")) ? "" : row.Field<string>("OTHERS"),
                Status = String.IsNullOrEmpty(row.Field<string>("E_STATUS")) ? "" : row.Field<string>("E_STATUS"),
                Statename = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
                Dist_name = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                Const_name = String.IsNullOrEmpty(row.Field<string>("CONSTITUENCY_NAME")) ? "" : row.Field<string>("CONSTITUENCY_NAME"),


            }).ToList();
            return objEventslist;
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public List<Consistency> RetrieveSTATES(Consistency consistency)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();

            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_STATES_DETAILS");
            
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<Consistency> objEventslist = Objds.Tables[0].AsEnumerable().Select(row => new Consistency
            {

                ID = row.Field<Int32?>("ID").GetValueOrDefault(),
                CONSISTENCY = row.Field<Int32?>("CONSISTENCY").GetValueOrDefault(),
                TRS = String.IsNullOrEmpty(row.Field<string>("TRS")) ? "" : row.Field<string>("TRS"),
                BJP = String.IsNullOrEmpty(row.Field<string>("BJP")) ? "" : row.Field<string>("BJP"),
                KUTAMI = String.IsNullOrEmpty(row.Field<string>("KUTAMI")) ? "" : row.Field<string>("KUTAMI"),
                OTHERS = String.IsNullOrEmpty(row.Field<string>("OTHERS")) ? "" : row.Field<string>("OTHERS"),
                Status = String.IsNullOrEmpty(row.Field<string>("E_STATUS")) ? "" : row.Field<string>("E_STATUS"),
                Statename = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
                Dist_name = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                Const_name = String.IsNullOrEmpty(row.Field<string>("CONSTITUENCY_NAME")) ? "" : row.Field<string>("CONSTITUENCY_NAME"),


            }).ToList();
            return objEventslist;
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public void Insert(Consistency register)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_CONSISTENCY");
        DB.AddInParameter(dbCom, "@STATE", DbType.Int16, register.STATE);
        DB.AddInParameter(dbCom, "@DISTRICT", DbType.Int16, register.DISTRICT);
        DB.AddInParameter(dbCom, "@CONSISTENCY", DbType.Int16, register.CONSISTENCY);
        DB.AddInParameter(dbCom, "@TRS", DbType.String, register.TRS);
        DB.AddInParameter(dbCom, "@BJP", DbType.String, register.BJP);
        DB.AddInParameter(dbCom, "@KUTAMI", DbType.String, register.KUTAMI);
        DB.AddInParameter(dbCom, "@OTHERS", DbType.String, register.OTHERS);
        DB.AddInParameter(dbCom, "@STATUS", DbType.String, register.Status);
        DB.AddInParameter(dbCom, "@STATE_NAME", DbType.String, register.Statename);
        DB.AddInParameter(dbCom, "@DIST", DbType.String, register.Dist_name);
        DB.AddInParameter(dbCom, "@CONST", DbType.String, register.Const_name);
        DB.ExecuteNonQuery(dbCom);
    }

    public static DataTable Getstates(oState omode)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_SELECT_STATES");

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;

            if (rdr.Read())
            {
                dt = new DataTable("STATE_NAME");
                dt = new DataTable("STATE_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }

    public static DataTable Getdist(oDistrict omode)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_SELECT_DISTRICTS");
            DB.AddInParameter(dbCom, "@STATE_ID", DbType.Int16, omode.STATE_ID);


            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;

            if (rdr.Read())
            {
                dt = new DataTable("DISTRICT_NAME");
                dt = new DataTable("DISTRICT_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void Edit(Consistency objSTATUS)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_CONSISTENCY_ESTATUS");
            DB.AddInParameter(editcmd, "@CONSTITUENCY_NAME", DbType.String, objSTATUS.Const_name);
           
            var rdr = DB.ExecuteReader(editcmd);


            if (rdr.Read())
            {
                objSTATUS.Status = (rdr["E_STATUS"].ToString());
                //  objforgot.PASSWORDS = rdr["PASSWORDS"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static DataTable GetConsistency(oConstituencies omode)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_SELECT_CONSTITUENCIES");
            DB.AddInParameter(dbCom, "@DISTRICT_ID", DbType.Int16, omode.DISTRICT_ID);

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;

            if (rdr.Read())
            {
                dt = new DataTable("CONSTI_NAME");
                dt = new DataTable("CONSTI_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }



    public void GetCountParties(Consistency ocon)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_COUNT_PARTIES");
            DB.AddInParameter(dbCom, "@E_STATUS", DbType.String, ocon.Status);

            var rdr = DB.ExecuteReader(dbCom);
            if (rdr.Read())
            {

                ocon.Count = int.Parse(rdr["party"].ToString());


            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);

        }
        catch (Exception ex)
        {
            throw ex;

        }
    }

    public void GetCountPartiesByDistrict(Consistency ocon)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_COUNT_PARTIES_BY_DISTRICT");
            DB.AddInParameter(dbCom, "@E_STATUS", DbType.String, ocon.Status);
            DB.AddInParameter(dbCom, "@DISTRICT", DbType.String, ocon.DISTRICT);
            var rdr = DB.ExecuteReader(dbCom);
            if (rdr.Read())
            {

                ocon.Count = int.Parse(rdr["party"].ToString());


            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);

        }
        catch (Exception ex)
        {
            throw ex;

        }
    }

}