﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for LoginDAL
/// </summary>
public class LoginDAL
{
    public LoginDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    #region User Methods
    public void UserCheckUsers(oLogin objUser)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_CHECK_USERS");
            DB.AddInParameter(dbCom, "@USER_MAIL_ID", DbType.String, objUser.UserMailId);
            var rdr = DB.ExecuteReader(dbCom);
            if (rdr.Read())
            {
                objUser.UserId   = int.Parse(rdr["ID"].ToString());
                objUser.UserName = rdr["NAME"].ToString();
                objUser.UserMailId = rdr["EMAIL"].ToString();
                objUser.UserStatus = int.Parse(rdr["REG_STATUS"].ToString());
                objUser.UserPwd = rdr["PASSWORDS"].ToString();
                objUser.UserType = rdr["USER_TYPE"].ToString();
            }
            else
            {
                //objUser.UserMailId = "NA";
                //objUser.UserMobile = 0;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            //return 0;
            throw ex;
        }
    }

    public void UserCheckLogin(oLogin objUser)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_CHECK_LOGIN");
            DB.AddInParameter(dbCom, "@USER_MAIL_ID", DbType.String, objUser.UserMailId);
            DB.AddInParameter(dbCom, "@USER_PASSWORD", DbType.String, objUser.UserPwd);
            var rdr = DB.ExecuteReader(dbCom);
            if (rdr.Read())
            {
                objUser.UserId = int.Parse(rdr["ID"].ToString());
                objUser.UserName = rdr["NAME"].ToString();
                objUser.UserMailId = rdr["EMAIL"].ToString();
                objUser.UserPwd = rdr["PASSWORDS"].ToString();
                objUser.UserStatus = int.Parse(rdr["REG_STATUS"].ToString());
                objUser.UserType = rdr["USER_TYPE"].ToString();
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            //return 0;
            throw ex;
        }
    }

    public List<oLogin> GetUsersList()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_USERS");
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oLogin> objUserslist = Objds.Tables[0].AsEnumerable().Select(row => new oLogin
            {
                UserId = row.Field<int?>("ID").GetValueOrDefault(),
                UserName = String.IsNullOrEmpty(row.Field<string>("NAME")) ? "" : row.Field<string>("NAME"),
                UserMailId= String.IsNullOrEmpty(row.Field<string>("EMAIL")) ? "" : row.Field<string>("EMAIL"),
                UserStatus = row.Field<int?>("REG_STATUS").GetValueOrDefault(),
                UserGender = String.IsNullOrEmpty(row.Field<string>("GENDER")) ? "" : row.Field<string>("GENDER"),
                UserType = String.IsNullOrEmpty(row.Field<string>("USER_TYPE")) ? "" : row.Field<string>("USER_TYPE"),
            }).ToList();
            return objUserslist;
            //DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //public void UserSavePwd(oLogin objUser)
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("USP_SAVE_PASSWORD");
    //        DB.AddInParameter(dbCom, "@USER_PASSWORD", DbType.String, objUser.UserPwd);
    //        DB.AddInParameter(dbCom, "@USER_ID", DbType.Int16, objUser.Id);
    //        DB.ExecuteNonQuery(dbCom);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //public void UserSaveEmail(oLogin objUser)
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("USP_SAVE_EMAIL");
    //        DB.AddInParameter(dbCom, "@USER_MAIL_ID", DbType.String, objUser.UserMailId);
    //        DB.AddInParameter(dbCom, "@USER_ID", DbType.Int16, objUser.Id);
    //        DB.ExecuteNonQuery(dbCom);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //public void UserSaveMobile(oLogin objUser)
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("USP_SAVE_MOBILE");
    //        DB.AddInParameter(dbCom, "@USER_MOBILE", DbType.Int64, objUser.UserMobile);
    //        DB.AddInParameter(dbCom, "@USER_ID", DbType.Int16, objUser.Id);
    //        DB.ExecuteNonQuery(dbCom);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //public void UsersCount(oLogin objUser)
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_USERS_COUNT");
    //        var rdr = DB.ExecuteReader(dbCom);
    //        if (rdr.Read())
    //        {
    //            objUser.Users_Count = Int64.Parse(rdr["USERS_COUNT"].ToString());
    //        }
    //        rdr.Close();
    //        DB.ExecuteNonQuery(dbCom);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    #endregion

}