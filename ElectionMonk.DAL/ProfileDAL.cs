﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProfileDAL
/// </summary>
public class ProfileDAL
{
    public ProfileDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    #region
    public void Getprofile(Register register)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_PROFILE_EDIT");
            DB.AddInParameter(editcmd, "@ID", DbType.Int16, register.Reg_id);
            var rdr = DB.ExecuteReader(editcmd);
            if (rdr.Read())
            {
                register.NAME = rdr["NAME"].ToString();
                register.EMAIL = rdr["EMAIL"].ToString();
                register.GENDER = rdr["GENDER"].ToString();
                register.DOB = rdr["DOB"].ToString();
                register.PASSWORDS = rdr["PASSWORDS"].ToString();
                register.Mobile = rdr["MOBILE_NUMBER"].ToString();
            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public void UpdateProfile (Register register)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand ucmd = DB.GetStoredProcCommand("ASP_UPDATE_PROFILE");
            DB.AddInParameter(ucmd, "@ID", DbType.String, register.Reg_id);
            DB.AddInParameter(ucmd, "@NAME", DbType.String, register.NAME);
            DB.AddInParameter(ucmd, "@EMAIL", DbType.String, register.EMAIL);

            DB.AddInParameter(ucmd, "@GENDER", DbType.String, register.GENDER);
            DB.AddInParameter(ucmd, "@DOB", DbType.String, register.DOB);
            DB.AddInParameter(ucmd, "@PASSWORDS", DbType.String, register.PASSWORDS);
            DB.AddInParameter(ucmd, "@mobile", DbType.String, register.Mobile);

            DB.ExecuteNonQuery(ucmd);
        }


        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
}