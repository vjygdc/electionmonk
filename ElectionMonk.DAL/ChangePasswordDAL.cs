﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChangePasswordDAL
/// </summary>
public class ChangePasswordDAL
{
    public ChangePasswordDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Edit(Register objforgot)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_PASSWORD");
            DB.AddInParameter(editcmd, "@Pwd", DbType.String, objforgot.PASSWORDS);
            DB.AddInParameter(editcmd, "@ID", DbType.String, objforgot.Reg_id);
            var rdr = DB.ExecuteReader(editcmd);


            if (rdr.Read())
            {
                objforgot.Count =Convert.ToInt16( rdr["pwdcount"].ToString());
              //  objforgot.PASSWORDS = rdr["PASSWORDS"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public int UpdatePassword(Register obj)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("UPDATE_PASSWORD");
            DB.AddInParameter(dbCom, "@Pwd", DbType.String, obj.PASSWORDS);
            DB.AddInParameter(dbCom, "@EmailID", DbType.String, obj.EMAIL);
            //DB.AddInParameter(dbCom, "@empid", DbType.String, objLogin.Id);
            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}