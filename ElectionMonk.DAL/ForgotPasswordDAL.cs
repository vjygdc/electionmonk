﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ForgotPassword
/// </summary>
public class ForgotPassword
{
    public ForgotPassword()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Edit(Register objforgot)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_DETAILS");
            DB.AddInParameter(editcmd, "@mailid", DbType.String, objforgot.EMAIL);
            var rdr = DB.ExecuteReader(editcmd);


            if (rdr.Read())
            {
                objforgot.EMAIL = rdr["EMAIL"].ToString();
                objforgot.PASSWORDS = rdr["PASSWORDS"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}