﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for constitutionDAL
/// </summary>
public class constitutionDAL
{
    public constitutionDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public static DataTable GetDistrictslist(oConstitution objdistr)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("GET_DISTRICTS_LIST");
            DB.AddInParameter(dbCom, "@state_name", DbType.String, objdistr.STATE_NAME);
            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;
            if (rdr.Read())
            {
                dt = new DataTable("DISTRICT_NAME");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }

    public static DataTable GetStatesList(oConstitution objstate)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("GET_STATES_LIST");

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;
            if (rdr.Read())
            {
                dt = new DataTable("STATE_NAME");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }



    public int InsertConstituency(oConstitution obj)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_CONSTITUENCY");
            DB.AddInParameter(dbCom, "@CONSTI_NAME", DbType.String, obj.CONSTI_NAME);
            DB.AddInParameter(dbCom, "@DISTRICT_NAME", DbType.String, obj.DISTRICT_NAME);
            DB.AddInParameter(dbCom, "@CONST_NUMBER", DbType.Int16, obj.CONST_NUMBER);
            //DB.AddInParameter(dbCom, "@CREA_BY", DbType.String, objcity.CREA_BY);

            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<oConstitution> GetCityList()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("USP_Grid_CITYS");

            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oConstitution> objCollections = Objds.Tables[0].AsEnumerable().Select(row => new oConstitution
            {
                CONST_NUMBER = row.Field<int?>("CONST_NUMBER").GetValueOrDefault(),
                STATE_NAME = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
                DISTRICT_NAME = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                CONSTI_NAME = String.IsNullOrEmpty(row.Field<string>("CONSTI_NAME")) ? "" : row.Field<string>("CONSTI_NAME"),
            }).ToList();

            return objCollections;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }
    public List<oConstitution> GetCitiesBySearch(oConstitution oConstitution)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("USP_GETCITIES_bySEARCH");
            DB.AddInParameter(dbCom, "@CONSTI_NAME", DbType.String, oConstitution.CONSTI_NAME);
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oConstitution> objCollections = Objds.Tables[0].AsEnumerable().Select(row => new oConstitution
            {
                CONSTI_ID = row.Field<Int32?>("CONSTI_ID").GetValueOrDefault(),
                CONSTI_NAME = String.IsNullOrEmpty(row.Field<string>("CONSTI_NAME")) ? "" : row.Field<string>("CONSTI_NAME"),
                DISTRICT_ID = row.Field<Int32?>("DISTRICT_ID").GetValueOrDefault(),

            }).ToList();

            return objCollections;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }
    //public List<oDistrict> GetCountry()
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("USP_Grid_COUNTRIES");
    //        DataSet Objds = DB.ExecuteDataSet(dbCom);
    //        List<oDistrict> objCollections = Objds.Tables[0].AsEnumerable().Select(row => new oDistrict
    //        {
    //            CTRY_ID = row.Field<Int32?>("CTRY_ID").GetValueOrDefault(),
    //            CRTY_NAME = String.IsNullOrEmpty(row.Field<string>("CRTY_NAME")) ? "" : row.Field<string>("CRTY_NAME"),
    //        }).ToList();

    //        return objCollections;
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //}
    public List<oConstitution> Getstatelist()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("USP_Grid_STATES");
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oConstitution> objCollections = Objds.Tables[0].AsEnumerable().Select(row => new oConstitution
            {


                DISTRICT_NAME = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
            }).ToList();

            return objCollections;
        }
        catch (Exception ex)
        {
            return null;
        }
    }
    //public List<oState> Getstate()
    //{
    //    try
    //    {
    //        Database DB = DatabaseFactory.CreateDatabase();
    //        DbCommand dbCom = DB.GetStoredProcCommand("USP_Grid_STATES");
    //        DataSet Objds = DB.ExecuteDataSet(dbCom);
    //        List<oState> objCollections = Objds.Tables[0].AsEnumerable().Select(row => new oState
    //        {
    //            CTRY_ID = row.Field<Int32?>("CTRY_ID").GetValueOrDefault(),
    //            STATE_ID = row.Field<Int32?>("STATE_ID").GetValueOrDefault(),
    //            STATE_NAME = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
    //        }).ToList();

    //        return objCollections;
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //}

    public void GetStateByID(oConstitution objstate)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_STATEBYID");
            DB.AddInParameter(editcmd, "@DISTRICT_ID", DbType.Int16, objstate.DISTRICT_ID);

            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {

                objstate.DISTRICT_NAME = rdr["DISTRICT_NAME"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void GetCityByID(oConstitution objcity)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_CITYBYID");
            DB.AddInParameter(editcmd, "@CONSTI_ID ", DbType.Int16, objcity.CONSTI_ID);

            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {

                objcity.CONSTI_NAME = rdr["CONSTI_NAME"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int Updatecity(oConstitution objcity, oConstitution oState)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_UPDATE_CITY");
            DB.AddInParameter(dbCom, "@CONSTI_ID", DbType.Int32, objcity.CONSTI_ID);
            DB.AddInParameter(dbCom, "@CONSTI_NAME", DbType.String, objcity.CONSTI_NAME);
            DB.AddInParameter(dbCom, "@DISTRICT_NAME", DbType.String, oState.DISTRICT_NAME);
          //  DB.AddInParameter(dbCom, "@UPDA_BY", DbType.String, oState.UPDA_BY);
            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void Delete_City(oConstitution oConstitution)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand delcmd = DB.GetStoredProcCommand("USP_DELETE_CONST");
        DB.AddInParameter(delcmd, "@CONSTI_ID", DbType.String, oConstitution.CONSTI_ID);
        DB.ExecuteNonQuery(delcmd);
    }
    public static DataTable GetConsName(oConstituencies objcons)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GETCONST");

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;
            if (rdr.Read())
            {
                dt = new DataTable("CONSTI_NAME");
                dt = new DataTable("CONSTI_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }

 
}
