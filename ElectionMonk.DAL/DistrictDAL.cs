﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DistrictDAl
/// </summary>
public class DistrictDAL
{
    public DistrictDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable GetStateList(oState objState)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_STATE");

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;
            if (rdr.Read())
            {
                dt = new DataTable("STATE_NAME");
                dt = new DataTable("STATE_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }


    public void InsertDistrict(oDistrict objDis)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_DISTRICTS");
            DB.AddInParameter(dbCom, "@DISTRICT_NAME", DbType.String, objDis.DISTRICT_NAME);
            DB.AddInParameter(dbCom, "@STATE_ID", DbType.String, objDis.STATE_ID);
            DB.AddInParameter(dbCom, "@DISTRICT_IMG", DbType.String, objDis.DISTRICT_IMG);
            DB.AddInParameter(dbCom, "@CREA_BY", DbType.String, objDis.CREA_BY);
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateDistrict(oDistrict objDis)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_UPDATE_DISTRICTS");
            DB.AddInParameter(dbCom, "@DISTRICT_ID", DbType.Int16, objDis.DISTRICT_ID);
            DB.AddInParameter(dbCom, "@DISTRICT_NAME", DbType.String, objDis.DISTRICT_NAME);
            DB.AddInParameter(dbCom, "@DISTRICT_IMG", DbType.String, objDis.DISTRICT_IMG);
            DB.AddInParameter(dbCom, "@STATE_ID", DbType.Int16, objDis.STATE_ID);
            DB.AddInParameter(dbCom, "@UPDA_BY", DbType.String, objDis.UPDA_BY);
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    public void GetDistrictDetailsByID(oDistrict objDis, oState objState)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_EDIT_DISTRICTS");
            DB.AddInParameter(editcmd, "@DISTRICT_ID", DbType.Int16, objDis.DISTRICT_ID);
            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {
                objState.STATE_NAME = rdr["STATE_NAME"].ToString();
                objDis.DISTRICT_NAME = rdr["DISTRICT_NAME"].ToString();
                objDis.DISTRICT_IMG = rdr["DISTRICT_IMG"].ToString();
            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<oDistrict> GetDistrictList()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_DISTRICTS");
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oDistrict> objCourseCollections = Objds.Tables[0].AsEnumerable().Select(row => new oDistrict
            {
                STATE_ID = row.Field<int?>("STATE_ID").GetValueOrDefault(),
                DISTRICT_ID = row.Field<int?>("DISTRICT_ID").GetValueOrDefault(),
                DISTRICT_NAME = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                DISTRICT_IMG = String.IsNullOrEmpty(row.Field<string>("DISTRICT_IMG")) ? "" : row.Field<string>("DISTRICT_IMG"),
            }).ToList();

            return objCourseCollections;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    

}