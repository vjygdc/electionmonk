﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Summary description for Register
/// </summary>
public class RegisterDAL
{
    public RegisterDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void RegisterInsert(Register register)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_REGISTER");
        DB.AddInParameter(dbCom, "@NAME", DbType.String, register.NAME);
        DB.AddInParameter(dbCom, "@EMAIL", DbType.String, register.EMAIL);
        DB.AddInParameter(dbCom, "@DOB", DbType.String, register.DOB);
        DB.AddInParameter(dbCom, "@GENDER", DbType.String, register.GENDER);
        DB.AddInParameter(dbCom, "@PASSWORD", DbType.String, register.PASSWORDS);
        DB.AddInParameter(dbCom, "@MOBILE_NUMBER", DbType.String, register.Mobile);
        DB.AddInParameter(dbCom, "@CASTE_CATEGORY", DbType.String, register.CASTE_CATEGORY);
        DB.ExecuteNonQuery(dbCom);
    }
    public int Userexistence(Register register)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("[ASP_CHECK_EMAIL]");
        DB.AddInParameter(dbCom, "@EMAIL", DbType.String, register.EMAIL);
        int count =Convert.ToInt16 (DB.ExecuteScalar(dbCom));
        return count;
    }
        public void MailsendtoJobseeker(Register register)
    {

        MailMessage myMessage = new MailMessage();
        myMessage.Subject = register.Subject;
        myMessage.Body = register.BodyMsg;
        myMessage.From = (new MailAddress("helpdesk.snpl@gmail.com"));
        myMessage.To.Add(new MailAddress(register.EMAIL));
        SmtpClient mySmtpClient = new SmtpClient();
        mySmtpClient.Send(myMessage);
    }

}