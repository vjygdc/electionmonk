﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;
using ElectionMonk.Common.Entities;

/// <summary>
/// Summary description for CandidatesDAL
/// </summary>
public class CandidatesDAL
{
    public CandidatesDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int InsertCandidate(Candidate objcandidates)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_CANDIDATES");
            DB.AddInParameter(dbCom, "@CANDIDATE_NAME", DbType.String, objcandidates.CANDIDATE_NAME);
            DB.AddInParameter(dbCom, "@CANDIDATE_IMG", DbType.String, objcandidates.CandidateImg);
            DB.AddInParameter(dbCom, "@STATUS", DbType.String, objcandidates.Status);
            DB.AddInParameter(dbCom, "@PARTY_ID", DbType.String, objcandidates.PartyName);
            DB.AddInParameter(dbCom, "@CONSTI_ID", DbType.Int32, objcandidates.ConstituencyID);
            DB.AddInParameter(dbCom, "@CREA_BY", DbType.String, objcandidates.CreatedBy);

            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<Candidate> GetCandidates()
    {

        try
        {
            Database DB = DatabaseFactory.CreateDatabase();

            DbCommand dbCom = DB.GetStoredProcCommand("ASP_BIND_CANDIDATES");

            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<Candidate> objCandidatesslist = Objds.Tables[0].AsEnumerable().Select(row => new Candidate
            {
                CandidateID = row.Field<Int32?>("CANDIDATE_ID").GetValueOrDefault(),
                CANDIDATE_NAME = String.IsNullOrEmpty(row.Field<string>("CANDIDATE_NAME")) ? "" : row.Field<string>("CANDIDATE_NAME"),
                District = String.IsNullOrEmpty(row.Field<string>("DISTRICT_NAME")) ? "" : row.Field<string>("DISTRICT_NAME"),
                CandidateImg = String.IsNullOrEmpty(row.Field<string>("CANDIDATE_IMG")) ? "" : row.Field<string>("CANDIDATE_IMG"),
                PartyName = String.IsNullOrEmpty(row.Field<string>("PARTY_NAME")) ? "" : row.Field<string>("PARTY_NAME"),
                ConstituencyName = String.IsNullOrEmpty(row.Field<string>("CONSTI_NAME")) ? "" : row.Field<string>("CONSTI_NAME"),
                ConstituencyID = row.Field<Int32?>("CONSTI_ID").GetValueOrDefault(),
                
                
            }).ToList();
            return objCandidatesslist;
            // DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    public void Edit(Candidate objcandidates)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_EDIT_CANDIDATES");
            DB.AddInParameter(editcmd, "@CANDIDATE_ID", DbType.String, objcandidates.CandidateID);
            var rdr = DB.ExecuteReader(editcmd);


            if (rdr.Read())
            {
                objcandidates.CANDIDATE_NAME = rdr["CANDIDATE_NAME"].ToString();
                //objcandidates.CANDIDATE_IMG = rdr["CANDIDATE_IMG"].ToString();
                objcandidates.PartyName = rdr["PARTY_NAME"].ToString();
                objcandidates.Constname =rdr["CONSTI_NAME"].ToString();
                
            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void UpdateCandidates(Candidate objcandidates)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand updatecmd = DB.GetStoredProcCommand("ASP_UPDATE_CANDIDATES");
            DB.AddInParameter(updatecmd, "@CANDIDATE_ID", DbType.Int32, objcandidates.CandidateID);
            DB.AddInParameter(updatecmd, "@CANDIDATE_NAME", DbType.String, objcandidates.CANDIDATE_NAME);
           // DB.AddInParameter(updatecmd, "@CANDIDATE_IMG", DbType.String, objcandidates.CANDIDATE_IMG);
            DB.AddInParameter(updatecmd, "@PARTY_ID", DbType.String, objcandidates.PartyName);
            DB.AddInParameter(updatecmd, "@CONSTI_ID", DbType.String, objcandidates.Constname);
            DB.AddInParameter(updatecmd, "@UPDA_BY", DbType.String, objcandidates.UpdatedBy);
            
            DB.ExecuteNonQuery(updatecmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void Delete(Candidate candidates)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand delcmd = DB.GetStoredProcCommand("ASP_DELETE_CANDIDATES");
        DB.AddInParameter(delcmd, "@candidate_id", DbType.Int32, candidates.CandidateID);
        DB.ExecuteNonQuery(delcmd);
    }

}