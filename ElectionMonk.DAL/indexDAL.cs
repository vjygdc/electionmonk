﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for indexDAL
/// </summary>
public class indexDAL
{
	public indexDAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public List<oConstituencies> Displayrequirementview(oConstituencies entity)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("[ASP_GET_CONSTITUENCIES]");
            DB.AddInParameter(dbCom, "@DISTRICT_ID", DbType.Int16, entity.DISTRICT_ID);
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oConstituencies> objRequireCollections = Objds.Tables[0].AsEnumerable().Select(row => new oConstituencies
            {
                CONSTI_ID = row.Field<Int32?>("CONSTITUENCIES.CONSTI_ID").GetValueOrDefault(),
                CONSTI_NAME = String.IsNullOrEmpty(row.Field<string>("CANDIDATE_NAME")) ? "" : row.Field<string>("CANDIDATE_NAME"),

            }).ToList();

            return objRequireCollections;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void Displaydistrictview(oDistrict entity)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand dbCom = DB.GetStoredProcCommand("[ASP_GET_DEMO_DISTRICTS]");
        DB.AddInParameter(dbCom, "@DISTRICT_ID", DbType.Int16, entity.DISTRICT_ID);
        var rdr = DB.ExecuteReader(dbCom);

        if (rdr.Read())
        {

           entity.DISTRICT_NAME = rdr["DISTRICT_NAME"].ToString();
            entity.DISTRICT_IMG = rdr["DISTRICT_IMG"].ToString();
        }
        rdr.Close();
        DB.ExecuteNonQuery(dbCom);
    }
    public int InsertUserPrediction(oUserPrediction objUP)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_USER_PREDICTION");
            DB.AddInParameter(dbCom, "@USER_LOGIN_ID", DbType.Int16, objUP.USER_LOGIN_ID);
            DB.AddInParameter(dbCom, "@TRS", DbType.String, objUP.TRS);
            DB.AddInParameter(dbCom, "@BJP", DbType.String, objUP.BJP);
            DB.AddInParameter(dbCom, "@KUTAMI", DbType.String, objUP.KUTAMI);
            DB.AddInParameter(dbCom, "@OTHERS", DbType.String, objUP.OTHERS);
            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
            //return 0;
        }
    }

    public void GetUserPredictionValuesByID(oUserPrediction objUP)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GetUserPrediction_By_ID");

            DB.AddInParameter(dbCom, "@USER_LOGIN_ID", DbType.Int16, objUP.USER_LOGIN_ID);
            var rdr = DB.ExecuteReader(dbCom);

            if (rdr.Read())
            {
                objUP.UR_ID = Convert.ToInt16(rdr["UR_ID"]);
                objUP.TRS = rdr["TRS"].ToString();
                objUP.BJP = rdr["BJP"].ToString();
                objUP.KUTAMI = rdr["KUTAMI"].ToString();
                objUP.OTHERS = rdr["OTHERS"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public int GetLoginCount(oUserPrediction objUP)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("GET_INSERT_COUNT");

            DB.AddInParameter(dbCom, "@USER_LOGIN_ID", DbType.Int16, objUP.USER_LOGIN_ID);
            var rdr = DB.ExecuteReader(dbCom);

            int count = Convert.ToInt16(dbCom.ExecuteScalar());
            return count;
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
}

    