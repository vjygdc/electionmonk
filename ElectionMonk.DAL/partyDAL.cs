﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for partyDAL
/// </summary>
public class partyDAL
{
    public partyDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int InsertParty(oParty objParty)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ADD_PARTY");
            DB.AddInParameter(dbCom, "@PARTY_NAME", DbType.String, objParty.PARTY_NAME);
            DB.AddInParameter(dbCom, "@PARTY_LOGO", DbType.String, objParty.PARTY_LOGO);
            DB.AddInParameter(dbCom, "@CREA_BY", DbType.String, objParty.CREA_BY);
            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
            //return 0;
        }
    }
    public List<oParty> GetPartyList()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("GET_PARTIES_LIST");
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oParty> objEducationCollections = Objds.Tables[0].AsEnumerable().Select(row => new oParty
            {
                PARTY_ID = row.Field<Int32?>("PARTY_ID").GetValueOrDefault(),
                PARTY_NAME = String.IsNullOrEmpty(row.Field<string>("PARTY_NAME")) ? "" : row.Field<string>("PARTY_NAME"),
                PARTY_LOGO = String.IsNullOrEmpty(row.Field<string>("PARTY_LOGO")) ? "" : row.Field<string>("PARTY_LOGO")
            }).ToList();

            return objEducationCollections;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GetPartyByID(oParty objParty)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_PARTY_BY_ID");
           DB.AddInParameter(dbCom, "@PARTY_ID", DbType.Int32, objParty.PARTY_ID);
            var rdr = DB.ExecuteReader(dbCom);

            if (rdr.Read())
            {

                objParty.PARTY_NAME = rdr["PARTY_NAME"].ToString();
                objParty.PARTY_LOGO = rdr["PARTY_LOGO"].ToString();

            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void DeleteParty(oParty objParty)
    {
        Database DB = DatabaseFactory.CreateDatabase();
        DbCommand delcmd = DB.GetStoredProcCommand("DELETE_PARTY");
        DB.AddInParameter(delcmd, "@PARTY_ID", DbType.Int32, objParty.PARTY_ID);
        DB.AddInParameter(delcmd, "@UPDA_BY", DbType.String, objParty.UPDA_BY);
        DB.ExecuteNonQuery(delcmd);
    }
    public int UpdateParty(oParty objParty)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("UPDATE_PARTY");
            DB.AddInParameter(dbCom, "@PARTY_ID", DbType.Int32, objParty.PARTY_ID);
            DB.AddInParameter(dbCom, "@PARTY_NAME", DbType.String, objParty.PARTY_NAME);
            DB.AddInParameter(dbCom, "@PARTY_LOGO", DbType.String, objParty.PARTY_LOGO);
            DB.AddInParameter(dbCom, "@UPDA_BY", DbType.String, objParty.UPDA_BY);
            return DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static DataTable GetPartyName(oParty objparty)
    {
        try
        {
            string returnValue = string.Empty;
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GETPARTY");

            var rdr = DB.ExecuteReader(dbCom);
            DataTable dt = null;
            if (rdr.Read())
            {
                dt = new DataTable("PARTY_NAME");
                dt = new DataTable("PARTY_ID");
                dt.Load(rdr);
                return dt;
            }
            rdr.Close();
            DB.ExecuteNonQuery(dbCom);
            //da.Fill(ds);
            return dt;
        }
        catch (Exception ex)
        {
            throw ex;

        }
    }
}
