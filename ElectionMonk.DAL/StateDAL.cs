﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StateDAL
/// </summary>
public class StateDAL
{
    public StateDAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void InsertState(oState obj)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_INSERT_STATE");
            DB.AddInParameter(dbCom, "@STATE_NAME", DbType.String, obj.STATE_NAME);
            DB.AddInParameter(dbCom, "@CREA_BY", DbType.String, obj.CREA_BY);
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void UpdateState(oState objStates)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_UPDATE_STATE");
            DB.AddInParameter(dbCom, "@STATE_ID", DbType.Int16, objStates.STATE_ID);
            DB.AddInParameter(dbCom, "@STATE_NAME", DbType.String, objStates.STATE_NAME);
            DB.AddInParameter(dbCom, "@UPDA_BY", DbType.String, objStates.UPDA_BY);
            DB.ExecuteNonQuery(dbCom);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    public void editstate(oState objcourse)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_STATE_BY_ID");
            DB.AddInParameter(editcmd, "@ID", DbType.Int16, objcourse.STATE_ID);
            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {
                objcourse.STATE_NAME = rdr["STATE_NAME"].ToString();
            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public List<oState> RetrieveStatesList()
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand dbCom = DB.GetStoredProcCommand("ASP_GET_STATE");
            DataSet Objds = DB.ExecuteDataSet(dbCom);
            List<oState> objCourseCollections = Objds.Tables[0].AsEnumerable().Select(row => new oState
            {
                STATE_ID = row.Field<int?>("STATE_ID").GetValueOrDefault(),
                STATE_NAME = String.IsNullOrEmpty(row.Field<string>("STATE_NAME")) ? "" : row.Field<string>("STATE_NAME"),
                CREA_BY = String.IsNullOrEmpty(row.Field<string>("CREA_BY")) ? "" : row.Field<string>("CREA_BY"),
                


            }).ToList();

            return objCourseCollections;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void getstate(oState objcourse)
    {
        try
        {
            Database DB = DatabaseFactory.CreateDatabase();
            DbCommand editcmd = DB.GetStoredProcCommand("ASP_GET_STATE");
            var rdr = DB.ExecuteReader(editcmd);

            if (rdr.Read())
            {
                objcourse.STATE_NAME = rdr["STATE_NAME"].ToString();
                objcourse.STATE_ID = int.Parse(rdr["STATE_ID"].ToString());
                objcourse.CREA_BY = rdr["CREA_BY"].ToString();
                objcourse.CREA_DATE = rdr["CREA_DATE"].ToString();
            }
            rdr.Close();
            DB.ExecuteNonQuery(editcmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}