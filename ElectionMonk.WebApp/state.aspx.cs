﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registration : System.Web.UI.Page
{
    List<oState> objCollection = new List<oState>();
    oState entites = new oState();
    StateBAL stateBAL = new StateBAL();
    StateDAL stateDAL = new StateDAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {
                Session["name"] = "admin";
                alert.Visible = false;
                BindStates();
                if (!IsPostBack)
                {
                    addstate.Visible = true;
                }
                addstate.Visible = true;
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
        

    }
    public void BindStates()
    {
        try
        {
            objCollection = stateBAL.RetrieveStatesList();
            if (objCollection != null)
            {
                if (objCollection.Count > 0)
                {
                    gvList.DataSource = objCollection;
                    gvList.DataBind();
                }
               
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gvList.Rows[index];
        Session["id"] = row.Cells[0].Text;
        if (e.CommandName == "Edit_Click")
        {

            edit();
            btnupdate.Visible = true;
            btnSubmit.Visible = false;
        }
    }

    void edit()
    {
        entites.STATE_ID = Convert.ToInt16(Session["id"]);
        stateBAL.get(entites);
       text.Text = entites.STATE_NAME;
        
    }

    protected void add_Click(object sender, EventArgs e)
    {
        addstate.Visible = true;
    }
    
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        entites.STATE_NAME = text.Text;
        entites.CREA_BY = Session["name"].ToString();
        stateBAL.Insert(entites);
        alertmod.Style.Add("background-color", "#d7ecc6");
        //alert.Style.Add("background-color", "#d7ecc6");
        Label3.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        Label3.Text = "Success";
        Label5.Text = "State added successfully";
        alert.Visible = true;
        BindStates();
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        entites.STATE_NAME = text.Text;
        entites.STATE_ID = Convert.ToInt16(Session["id"]);
        entites.UPDA_BY = Session["name"].ToString();
        stateBAL.update(entites);
        alertmod.Style.Add("background-color", "#d7ecc6");
        //alert.Style.Add("background-color", "#d7ecc6");
        Label3.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        Label3.Text = "Success";
        Label5.Text = "State Updated successfully";
        alert.Visible = true;
        BindStates();
        addstate.Visible = true;
    }

    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "ID");
            cell[1].Attributes.Add("data-title", "State Name"); 
            cell[2].Attributes.Add("data-title", "Actions");
        }
    }

    protected void brnclose_Click(object sender, EventArgs e)
    {

    }
}