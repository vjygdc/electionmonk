﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="userconsistencylist.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
              <div class="row">
                        <h2 id="nodata" runat="server" style="text-align:center"></h2>
                        <div class="table-responsive" id="no-more-tables">
                            <asp:GridView ID="gvConsCanList" runat="server" Width="100%"  AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab datatab">
                                <Columns>
                                     <asp:TemplateField HeaderText="Constituency No.">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Constid") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Constituency No.">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("CONSISTENCY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Constituency Name">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>

                                    
                                    <asp:TemplateField HeaderText="TRS">
                                        <ItemTemplate>
                                                 <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("TRS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="trs"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="KUTAMI">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="kutami" />
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BJP">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="bjp"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Others">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="other"  />
                                    </asp:TemplateField>
                                    
                                    
                                </Columns>
                                <HeaderStyle CssClass="thead-dark" />
                            </asp:GridView>
                        </div>
                    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
</asp:Content>

