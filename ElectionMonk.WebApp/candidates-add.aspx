﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="candidates-add.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="dynamic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
     <div class="jumbotron header-simple">
              <div class="container a" style="padding-right: 100px;" id="alert" visible="true" runat="server">
                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="Label5" runat="server" Text=""></asp:Label></strong>
                    <asp:Label ID="Label6" runat="server" Visible="true" Text=""></asp:Label>
                </div>
            </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#"  class="color1"> About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
       
        <!-- message content -->
        <div class="container fadesimple">
             
            <div class="col-sm-12 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                   
    <div class="table-pricing">
        <div class="row">

            <div class="default-title">
                        <h3>Adilabad - Candiate List</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci distinctio vel possimus aliquam.</p>
                    </div>
            
             <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-4">
                            <label class="control-label" style="color:black;font-size:15px" >Candidate Name</label>
                         
                            <asp:TextBox ID="txtcandidatename" class="form-control" placeholder="Enter EmpId" runat="server"></asp:TextBox>
          
                        </div>
                      <asp:ScriptManager runat="server"></asp:ScriptManager>
                        <div id="imgdiv" class="col-md-4" runat="server">
                            
                           <label class="control-label" style="color:black;font-size:15px" >Company Logo</label>
                         
                            <dynamic:AsyncFileUpload required="Logo Required"  OnClientUploadComplete="uploadComplete" runat="server" Style="background-color:darkblue; padding: 5px; border-radius: 10px;"
                                           ID="AsyncFileUpload1" Width="250px" UploaderStyle="Modern"
                                           CompleteBackColor="White"
                                           ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" ThrobberID="imgLoader" />
                                  <label style="color:skyblue">Upload Images with 300X300 and below 200kb</label>
                                    <asp:Label ID="llberrormsg" runat="server" Text="" ForeColor="Orange"></asp:Label>
                                       <asp:Label ID="lblMesg" runat="server" ForeColor="green" Text=""></asp:Label>
                                              
                      
                            </div>
                       <div class="col-md-4">
                                 <label class="control-label" style="color:black;font-size:15px"  >select constituences</label>
 <asp:DropDownList ID="ddlconst" ValidationGroup="sd" runat="server" AutoPostBack="false"  class="form-control mb-3 show-tick">
                            </asp:DropDownList>
                           </div>
                      
                      </div>
                           <div class="col-md-12" >
                            <div class="col-md-4">
                            <label class="control-label" style="color:black;font-size:15px" > select party name</label>
 <asp:DropDownList ID="ddlparty" ValidationGroup="sd" runat="server"  class="form-control mb-3 show-tick">
                            </asp:DropDownList>
                           </div>
                                <div class="col-md-4">
                                    </div>
                                <div class="col-md-4">
                                     <button type="button" id="btnadd" runat="server" style="color:white;font-size:15px" onserverclick="btnadd_ServerClick" class="btn btn-primary" ValidationGroup="sd">ADD CANDIDATE</button>
                                    <button type="button" id="update" runat="server" style="color:white;font-size:15px" onserverclick="update_ServerClick" class="btn btn-primary" ValidationGroup="sd">UPDATE CANDIDATE</button>
                                    </div>
                            </div>
                      
             </div>
        </div>
    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->
   
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
      <script type="text/javascript">
          var seconds = 3;
          setTimeout(function () {
              document.getElementById("<%=alert.ClientID %>").style.display = "none";
          }, seconds * 1000);
    </script>
     <script type="text/javascript">
           //ajax file upload functions
           function uploadComplete(sender) {
               $get("<%=lblMesg.ClientID%>").innerHTML = "File Uploaded Successfully";
               $get("<%=llberrormsg.ClientID%>").innerHTML = "";
           }

           function uploadError(sender) {
               $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";
        }


        var ifError = false;
        function UpLoadStarted(sender, e) {
            var fileName = e.get_fileName();
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'jpg' || fileExtension == 'png' || fileExtension == 'gif' || fileExtension == 'GIF' || fileExtension == 'PNG' || fileExtension == 'JPG' || fileExtension == 'jpeg' || fileExtension == 'JPEG') {
                //file is good -- go ahead
            }
            else {
                //stop upload
                ifError = true;
                sender._stopLoad();
            }
        }
        function UploadError(sender, e) {
            if (ifError) {
                alert("Please upload image files only!");
            }
            else {
                alert(e.get_message());
            }
        }
                //end 

    </script>
</asp:Content>

