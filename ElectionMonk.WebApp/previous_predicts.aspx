﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="previous_predicts.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">

    <section id="contact-us">

        <div class="container fadesimple">
            <div class="col-sm-12 contact-info">

                <div class="col-md-6">

                    <div class="default-title">
                        <h3>
                            <asp:Label ID="lblDistrict" runat="server"></asp:Label></h3>
                    </div>
                    <img src="" alt="image description" class="img-responsive" runat="server" id="imagedis"   />
                </div>
                  <div class="col-md-6">

                         <div class="col-md-12">
       
  
             <div class="box sl1 fadesimple mtb-10">
                       <h3> Seats  </h3>
                    <hr>
                    
                        <div class="color1 trstxt" style="width: 150px !important;"> TRS</div>  <asp:Label ID="txtTRS" runat="server" CssClass="trstxt" Text="Label"></asp:Label> <br />
                        <div class="color1 kutamitxt" style="width: 150px !important;"> KUTAMI</div>   <asp:Label ID="txtKUTAMI" runat="server" CssClass="kutamitxt"  Text="Label"></asp:Label> <br /> 
                       
                        <div  class="color1 bjptxt" style="width: 150px !important;"> BJP</div>   <asp:Label ID="txtBJP" runat="server" CssClass="bjptxt"  Text="Label"></asp:Label> <br />
                        <div  class="color1 othertxt" style="width: 150px !important;"> others</div>   <asp:Label ID="txtothers" runat="server" CssClass="othertxt"  Text="Label"></asp:Label> 
                    
                </div>     

        </div>

                   <%--    <div class="col-md-6">
       
  
                <div class="box sl1 fadesimple">
                       <h3> Seats  </h3>
                    <hr>
                    <ul class="wt-list">
                         <li class="wtl-2"> 
            <asp:Label ID="TRS" runat="server" CssClass="trstxt" Text="Label"></asp:Label></li>
                        <li class="wtl-1"> 
            <asp:Label ID="KUTAMI" runat="server" CssClass="kutamitxt"  Text="Label"></asp:Label></li>
                       
                        <li class="wtl-3"> 
            <asp:Label ID="BJP" runat="server" CssClass="bjptxt"  Text="Label"></asp:Label>    </li>
                        <li class="wtl-4"> 
            <asp:Label ID="others" runat="server" CssClass="othertxt"  Text="Label"></asp:Label>  </li>
                    </ul> 
                </div> 
                              

        </div>--%>

                    
                </div>
                <div class="col-sm-12">
                     <div class="row">

                              
                        <div class="design2" style="float: right">

                                   <div class="fb-share-button" data-href="http://supremenetsoft.com/" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
           
                         
                         <a class="twitter-share-button"
  href="https://twitter.com/intent/tweet">
Tweet</a>
 

                        </div>
                    </div>

                    <div class="row">
                        <h2 id="nodata" runat="server" style="text-align:center"></h2>
                        <div class="table-responsive" id="no-more-tables">
                            <asp:GridView ID="gvConsCanList" runat="server" Width="100%"  AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab datatab" OnRowDataBound="gvList_RowDataBound">
                                <Columns>
                                       <asp:TemplateField HeaderText="Constituency No.">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("CONSISTENCY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Constituency Name">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>

                                    
                                    <%--<asp:TemplateField HeaderText="TRS">
                                        <ItemTemplate>
                                              <span class="greencolor" id="spName">    <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("TRS") %>'></asp:Label> </span>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="trs"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="KUTAMI">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="kutami" />
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BJP">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="bjp"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Others">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="other"  />
                                    </asp:TemplateField>--%>
                                    
                                    <asp:BoundField DataField="TRS" HeaderText="TRS" HeaderStyle-CssClass="trs" />
                                    <asp:BoundField DataField="KUTAMI" HeaderText="KUTAMI" HeaderStyle-CssClass="kutami"/>
                                    <asp:BoundField DataField="BJP" HeaderText="BJP" HeaderStyle-CssClass="bjp"/>
                                    <asp:BoundField DataField="OTHERS" HeaderText="Others" HeaderStyle-CssClass="other"/>
                                    <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-CssClass="hidden" ControlStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                </Columns>
                                <HeaderStyle CssClass="thead-dark" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <!-- end of header info -->
            </div>
        </div>
        <!-- container -->


    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
</asp:Content>

