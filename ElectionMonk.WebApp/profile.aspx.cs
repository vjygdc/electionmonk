﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    Register register = new Register();
    ProfileBAL bAL = new ProfileBAL();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() != null && Session["SessionUserID"].ToString().ToUpper() != null)
            {
                if (!IsPostBack)
                    Profiledetails();
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
        
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        register.Reg_id = Convert.ToInt32(Session["SessionUserID"]);
        register.NAME = txtfname.Text + " " +txtlname.Text;
        register.EMAIL = txtemail.Text;
        register.DOB = txtdob.Text;
        register.EMAIL = txtemail.Text;
        register.GENDER = ddlgender.SelectedValue;
        register.PASSWORDS = txtpwd.Text;
        register.Mobile = txtmob.Text;
        
        
        bAL.Updateprofile(register);
        alert.Visible = true;
        alertlabel.Text = "Profile Updated Successfully";
    }

    public void Profiledetails()
    {
        register.Reg_id = Convert.ToInt32(Session["SessionUserID"]);
        bAL.EditProfile(register);
        string[] temp = register.NAME.Split(' ');
        txtfname.Text = temp[0];
        txtlname.Text = temp[1];
        txtemail.Text = register.EMAIL;
        txtpwd.Text = register.PASSWORDS;
        txtmob.Text = register.Mobile;
        ddlgender.SelectedValue = register.GENDER;
        txtdob.Text = register.DOB;
    }
}