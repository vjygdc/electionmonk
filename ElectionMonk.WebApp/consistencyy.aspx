﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="consistencyy.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
      <div class="jumbotron header-simple">
        <div class="container"> 
            <div class="row">
                <div class="col-md-12">
                    
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">Aboutus  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
        <div class="default-title">
            <h3>Constituency</h3>
            
        </div>
        <%--alert code--%>
        <div class="container a" style="padding-right: 100px;" id="alert" visible="false" runat="server">
            <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>
                    <asp:Label ID="alertlabel" runat="server" Text=""></asp:Label>
                </strong>
            </div>
        </div>
        <%--end--%>
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-8">
                
                    <div class="messages">
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">State</label>
                                    <asp:DropDownList ID="ddlstate" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged1"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="ddlstate"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                    <asp:Label ID="lblstate" runat="server" style="display:none"></asp:Label>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">District</label>
                                    <asp:DropDownList ID="ddldist" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddldist_SelectedIndexChanged1" ></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="ddldist"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">Consistency</label>
                                    <asp:DropDownList ID="ddlconst" runat="server" CssClass="form-control"></asp:DropDownList>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator37"  runat="server" ErrorMessage="*" ForeColor="#ff9933" Display="Dynamic" ControlToValidate="ddlconst" ></asp:RequiredFieldValidator>
                              
                                    <div class="help-block with-errors"></div>
                                </div>
                               
                            </div>

                            

                              <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">TRS</label>
                                    <asp:TextBox ID="txttrs" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txttrs"></asp:RequiredFieldValidator>
                                   

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_message">Kutami</label>
                                    <asp:TextBox ID="txtkutami" runat="server"  CssClass="form-control"></asp:TextBox>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                          <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">BJP</label>
                                    <asp:TextBox ID="txtbjp" runat="server"  CssClass="form-control"></asp:TextBox>
                                     

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtbjp"></asp:RequiredFieldValidator>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>


                               <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">Others</label>
                                    <asp:TextBox ID="txtothers" runat="server"  CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtothers"></asp:RequiredFieldValidator>
                                   


                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="form_phone">Status</label>
                                        <asp:DropDownList ID="ddlstatus" CssClass="form-control" runat="server" AutoPostBack="false">
                                            <asp:ListItem Value="TRS">TRS</asp:ListItem>
                                            <asp:ListItem Value="BJP">BJP</asp:ListItem>
                                            <asp:ListItem Value="KUTAMI">KUTAMI</asp:ListItem>
                                            <asp:ListItem Value="Others">Others</asp:ListItem>
                                        </asp:DropDownList>
                                        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit"  runat="server" style="float:right" Text="Submit" CssClass="btn btn-warning btn-send" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnupdate" runat="server" Text="Update" style="float:right" OnClick="btnupdate_Click" Visible="false" CssClass="btn btn-warning btn-send" />
                                <a href="consistencylist.aspx" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                        <div class="row"></div>
                    </div>
                        </div>
                
            </div>
            <!-- end of message content -->
            <div class="col-sm-4 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-12">
                        <p class="email">hello@themekolor.co</p>
                        <p class="phone">Phone : 1-855-435-8226</p>
                        <p class="chat"><a href="#">Live Chat</a></p>
                        <p class="ticket"><a href="/contact.html">Send a Ticket</a></p>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">

     <script type="text/javascript">
        var seconds = 3;
        setTimeout(function () {
            document.getElementById("<%=alert.ClientID %>").style.display = "none";
            window.location.href = "consistencylist.aspx";
         }, seconds * 1000);
         
    </script>
</asp:Content>

