﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registered_users : System.Web.UI.Page
{
    public string currentUrl;
    #region User Variables
    List<oLogin> objUserCollection = new List<oLogin>();
    oLogin objUser = new oLogin();
    LoginDAL objLoginDAL = new LoginDAL();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string authority = HttpContext.Current.Request.Url.Authority;
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            currentUrl = authority + path;
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {
                if (!IsPostBack)
                    BindGrid();
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
       
    }

    protected void BindGrid()
    {
        try
        {
            objUserCollection = objLoginDAL.GetUsersList();
            if (objUserCollection != null)
            {
                if (objUserCollection.Count > 0)
                {
                    gvList.DataSource = objUserCollection;
                    gvList.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvList.Rows[index];
            if (e.CommandName == "EditUser_Click")
            {
                //Session["state"] = "update";
                //Response.Redirect("hr-client-regisreation.aspx");
            }
            else if (e.CommandName == "DeleteUser_Click")
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#deleteconfirm').modal('show');</script>", false);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>confirm('Are you Sure want to Delete?');</script>", false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnyes_ServerClick(object sender, EventArgs e)
    {
        //objclient.CLIENT_REG_ID = Convert.ToInt16(Session["clientID"]);
        //objClient_bal.Delete_Client(objclient);
        //Bind_Clients_Grid();
        //alertmod.Style.Add("background-color", "#d7ecc6");
        //alert.Style.Add("background-color", "#d7ecc6");
        //Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        //Label6.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        //Label5.Text = "Success!";
        //Label6.Text = Session["Testmonial"] + " Record has been deleted";
        //alert.Visible = true;
    }

    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "USER_ID");
            cell[1].Attributes.Add("data-title", "NAME");
            cell[2].Attributes.Add("data-title", "EMAIL ID");
            cell[3].Attributes.Add("data-title", "GENDER");
            cell[4].Attributes.Add("data-title", "ACTIONS");
        }
    }
}