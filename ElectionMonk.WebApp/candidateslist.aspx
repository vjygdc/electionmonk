﻿<%@ Page Title="" Language="C#" MasterPageFile="cpc.master" AutoEventWireup="true" CodeFile="candidateslist.aspx.cs" Inherits="candidateslist" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style>
 

            .trs{    background-color: #ff00ff !important;
    text-align: center;
    color: #fff !important;}
.kutami{    background-color: #008000 !important;
    text-align: center;
    color: #fff !important;}
.bjp{    background-color: #ff6600 !important;
    text-align: center;
    color: #fff !important;}
.other{    background-color: #7030a0 !important;
    text-align: center;
    color: #fff !important;}

 
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
        <div class="container">
            <div class="Col-md-12" >

                 <div class="default-title">
            <h3>Candidates List</h3>

        </div>

            <div style="margin-top: 20px;"   id="no-more-tables">
                 <asp:GridView ID="gvCandidatelist" AutoGenerateColumns="false" runat="server"   class="table table-striped custab" OnRowDataBound="gvCandidatelist_RowDataBound"  >
                      <Columns>
                                       <asp:TemplateField HeaderText="Constituency No.">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("CONSISTENCY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="TRS">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("TRS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="trs"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="KUTAMI">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="kutami" />
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BJP">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="bjp"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Others">
                                        <ItemTemplate>
                                            <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="other"  />
                                    </asp:TemplateField>


                                </Columns>
                 </asp:GridView>

      
    </div>
                </div>

            </div> 
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
 

</asp:Content>