﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="party.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="dynamic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Party List</h2>
                    <p><a href="index.aspx" class="color1">Home</a> / <a href="#" class="color1">About us  </a></p>
                </div>
            </div>
        </div>
    </div>

    <section id="contact-us">
        <div class="container fadesimple">
            <div class="container" id="alert" visible="true" runat="server">
                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label></strong>
                    <asp:Label ID="lblMessage" runat="server" Visible="true" Text=""></asp:Label>
                </div>
            </div>
            <div class="header">
                <div id="updateparty" runat="server">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="title" id="defaultModalLabel" style="text-align:center;">Update Party</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row clearfix">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="form_name">Party *</label>
                                            <asp:TextBox ID="txtPartyNameUpdate" runat="server" placeholder="Enter party Name" CssClass="form-control"></asp:TextBox>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <img src="" alt="logo preview" id="imgPreview" runat="server" style="width: 100px" visible="false" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form_name">Party Logo</label>
                                            <asp:FileUpload ID="FUupdatelogo" Width="400px" runat="server" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:LinkButton ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" Style="float: right; color: white;" CssClass="col-md-4 btn btn-warning btn-send"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" Style="float: left; color: white;" CssClass="col-md-4 btn btn-warning btn-send"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="table-pricing">
                    <div class="row">
                        <div class="default-title">
                            <h3>Party List</h3>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mtb-10">
                                <asp:Button ID="btnAddParty" runat="server" data-toggle="modal" data-target="#addparty" Text="Add Party" CssClass="btn btn-warning btn-send   pull-right" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="table-responsive" id="no-more-tables">
                                <asp:GridView ID="gvPartyList" runat="server" Width="100%"
                                    AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab datatab" OnRowCommand="gvList_RowCommand" OnRowDataBound="gvList_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="PARTY_ID" HeaderText="PARTY ID" SortExpression="PARTY_ID" />
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyName" runat="server" Text='<%#Eval("PARTY_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Party Logo">
                                            <ItemTemplate>
                                                <asp:Image ID="imglogo" runat="server" ImageUrl='<%#Eval("PARTY_LOGO") %>' Text='<%#Eval("PARTY_NAME") %>' AlternateText='<%#Eval("PARTY_NAME") %>' ToolTip='<%#Eval("PARTY_NAME") %>' Width="50" Height="30" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actions">
                                            <ItemTemplate>
                                                    <asp:LinkButton ID="lbtnEdit" class="btn btn-info button btn-sm" runat="server" CommandName="Edit_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-pencil" style="color:white" ></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbtnDelete" class="btn btn-danger button btn-sm" runat="server" CommandName="Delete_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-trash" style="color:white"></i></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <HeaderStyle CssClass="thead-dark" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="addparty" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6 class="title" id="popUpTitle" runat="server">Add party</h6>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Party *</label>
                                        <asp:TextBox ID="txtPartyName" runat="server" class="form-control" placeholder="Enter Party Name *" required="required"></asp:TextBox>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="form_name">Party Logo</label>
                                        <dynamic:AsyncFileUpload OnClientUploadComplete="uploadComplete" runat="server" Style="background-color: #2DD61D; padding: 10px; border-radius: 10px;"
                                            ID="FUpartylogo" Width="400px" UploaderStyle="Modern"
                                            CompleteBackColor="White"
                                            ClientIDMode="AutoID" UploadingBackColor="Yellow" OnClientUploadStarted="UpLoadStarted" OnClientUploadError="UploadError" ThrobberID="imgLoader" />
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <asp:Label ID="lblMesg" runat="server" Text=""></asp:Label>
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <asp:Label ID="Note" runat="server" Text="Please upload .jpeg,.jpg & .png files only!" ForeColor="Black"></asp:Label>
                                </div>

                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" id="btnAdd" class="btn btn-warning btn-send " runat="server" validationgroup="one" onserverclick="btnAdd_ServerClick">Submit</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
    <script type="text/javascript">
        window.onload = function () {
            var seconds = 4;
            setTimeout(function () {
                document.getElementById("<%=alert.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script type="text/javascript">
        //ajax file upload functions
        function uploadComplete(sender) {
            document.getElementById("<%=Note.ClientID%>").style.color = "skyblue";
            $get("<%=lblMesg.ClientID%>").innerHTML = "File Uploaded Successfully!!!";
            $get("<%=lblMesg.ClientID%>").style.color = "green";
        }

        function uploadError(sender) {
            $get("<%=lblMesg.ClientID%>").innerHTML = "File upload failed.";
            document.getElementById("<%=lblMesg.ClientID%>").style.color = "red";
        }


        var ifError = false;
        function UpLoadStarted(sender, e) {
            var fileName = e.get_fileName();
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'jpg' || fileExtension == 'jpeg' || fileExtension == 'png') {
                //file is good -- go ahead
            }
            else {
                //stop upload
                ifError = true;
                sender._stopLoad();
            }
        }
        function UploadError(sender, e) {
            if (ifError) {
                document.getElementById("<%=Note.ClientID%>").style.color = "red";
            }
        }

        //end 
    </script>
     <script type="text/javascript">
        $(function () {
            $('[id*=gvPartyList]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers", "pageLength": 5
            });
        });
    </script>
</asp:Content>

