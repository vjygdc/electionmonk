﻿<%@ Page Title="Login - Page" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
        <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#"  class="color1"> About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
     
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-6">

                
                    <img src="img/login.jpg" class="img-responsive img-rounded" />
               
            </div>
            <!-- end of message content -->
            <div class="col-sm-6 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
   <div class="default-title">
            <h3>Login</h3>
            <p>you need to Login to support your Party.</p>
        </div>
                    
                  <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_name">User ID</label> 
                                    <asp:TextBox class="form-control" data-error="Email id is required." ID="txtUserName" runat="server" name="name" placeholder="Enter your email id" required="required" type="email"></asp:TextBox>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_lastname">Password</label> 
                                        <asp:TextBox class="form-control" data-error="Password is required." ID="txtUserPwd" runat="server" name="surname" placeholder="Enter your password" required="required" type="password"></asp:TextBox>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-4">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-warning btn-send" OnClick="btnSubmit_Click" />
                            </div>
                            <div class="col-md-4">
                                <asp:CheckBox ID="RememberMe" runat="server" Text="Remember Me" />
                                </div>
                            <div class="col-md-4">
                                <a rel="forgot-password" href="ForgotPassword.aspx">Forgot Password? </a>
                            </div>
                        </div>
                        <div class="row"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
</asp:Content>



