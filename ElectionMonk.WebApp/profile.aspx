﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="profile.aspx.cs" Inherits="_Default" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="dynamic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <style>
        .readonly{
            pointer-events:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
      <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Profile</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#"  class="color1"> Profile  </a></p>
                </div>
            </div>
        </div>
    </div>
      <section id="contact-us">
        <div class="default-title">
            <h3>Profile</h3>
            
        </div>
         <%--alert code--%>
            <div class="container a" style="padding-right: 100px;" id="alert" visible="false" runat="server">
                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="alertlabel" runat="server" Text=""></asp:Label>
                    </strong>
                </div>
            </div>
            <%--end--%>
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-8">
                <form action="/php/contact.php" id="contact-form" method="post" name="contact-form" novalidate="true">
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">First Name</label> <asp:TextBox ID="txtfname" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtfname"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                                    <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Last Name</label> <asp:TextBox ID="txtlname" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtlname"></asp:RequiredFieldValidator>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                    
                            <div class="col-md-6 readonly">
                                <div class="form-group">
                                    <label for="form_phone">Email Id</label><asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                                    
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                                                        

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">Mobile</label><asp:TextBox ID="txtmob" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtmob"></asp:RequiredFieldValidator>
                                   
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
ControlToValidate="txtmob" ErrorMessage="Not valid" ForeColor="Orange" Display="Dynamic"  
ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator> 

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                              <div class="col-md-6" style="display:none">
                                <div class="form-group">
                                    <label for="form_phone">Password</label> <asp:TextBox ID="txtpwd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtpwd"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_message">Gender</label>
                                    <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                          <div class="col-md-6 readonly">
                                <div class="form-group">
                                    <label for="form_phone">DOB</label><asp:TextBox ID="txtdob" runat="server" CssClass="form-control"></asp:TextBox>
                                     <dynamic:CalendarExtender ID="CalendarExtender4"
                                                        runat="server" PopupButtonID="imgPopup"
                                                        TargetControlID="txtdob" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtdob"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                                 
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit" runat="server" Text="Update" CssClass="btn btn-warning btn-send" OnClick="btnSubmit_Click"  />
                                
                               
                            </div>
                        </div>
                        <div class="row"></div>
                    </div>
                </form>
            </div>
            <!-- end of message content -->
            <div class="col-sm-4 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-12">
                        <p class="email">hello@themekolor.co</p>
                        <p class="phone">Phone : 1-855-435-8226</p>
                        <p class="chat"><a href="#">Live Chat</a></p>
                        <p class="ticket"><a href="/contact.html">Send a Ticket</a></p>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
     <script type="text/javascript">
               var seconds = 3;
               setTimeout(function () {
                   document.getElementById("<%=alert.ClientID %>").style.display = "none";
             }, seconds * 1000);
    </script>
</asp:Content>

