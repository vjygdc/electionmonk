﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class index : System.Web.UI.Page
{
    public int PredictionTotal = 0, RemainingTotal, Result;
    Consistency con = new Consistency();
    oUserPrediction objUP = new oUserPrediction();
    ConsistencyBAL bAL = new ConsistencyBAL();
    List<Consistency> objConsList = new List<Consistency>();
    IndexBAL objIndexBAL = new IndexBAL();
    public int[] intArray;
    protected void Page_Load(object sender, EventArgs e)
    {
        alert.Visible = false;
        if (!IsPostBack)
        {
            Count();
            Bind();
        }
        if (Request.Cookies["cookieTRS"] != null)
        {
            txtTRS.Text = Request.Cookies["cookieTRS"].Value;
            txtBJP.Text = Request.Cookies["cookieBJP"].Value;
            txtKUTAMI.Text = Request.Cookies["cookieKUTAMI"].Value;
            txtOthers.Text = Request.Cookies["cookieOTHERS"].Value;
        }
        //try
        //{
        //if (Response.Cookies["temp"].Value == "login")
        //{
        //    txtTRS.Text = Session["TRS"].ToString();
        //    txtBJP.Text = Session["BJP"].ToString();
        //    txtKUTAMI.Text = Session["KUTAMI"].ToString();
        //    txtOthers.Text = Session["OTHERS"].ToString();
        //}
        //}

    }
    public void BindUserPredictions()
    {
        objUP.USER_LOGIN_ID = Convert.ToInt16(Session["SessionUserID"]);
        objIndexBAL.GetUserPredictionValuesByID(objUP);
        txtTRS.Text = objUP.TRS;
        txtBJP.Text = objUP.BJP;
        txtKUTAMI.Text = objUP.KUTAMI;
        txtOthers.Text = objUP.OTHERS;

    }

    public void Count()
    {
        int k;
        string[] stringArray = {"TRS", "KUTAMI", "BJP", "others" };
        intArray = new int[stringArray.Length];
        for (int i = 0; i < stringArray.Length - 1; i++)
        {
            con.Status = stringArray[i];
            bAL.GetPartyCount(con);

            intArray[i] = con.Count;



        }
        //TDP.Text = intArray[0].ToString();
        //TRS.Text = intArray[0].ToString();
        //KUTAMI.Text = intArray[1].ToString();
        //BJP.Text = intArray[2].ToString();
        //others.Text = intArray[3].ToString();

        TRS.Text = "50";
        KUTAMI.Text = "65";
        BJP.Text = "1";
        others.Text = "3";

    }


    public void Bind()
    {
        try
        {
            
            objConsList = bAL.Getstateslist(con);

            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvlist.DataSource = objConsList;
                    gvlist.DataBind();
                }
                else if (objConsList.Count == 0)
                {

                   // nodata.InnerText = "No Data found";
                }

            }
            else if (objConsList == null)
            {

               // nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvlist_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = gvlist.SelectedRow.RowIndex;
        string name = gvlist.SelectedRow.Cells[0].Text;
        string country = gvlist.SelectedRow.Cells[1].Text;
        string message = "Row Index: " + index + "\\nName: " + name + "\\nCountry: " + country;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + message + "');", true);
    }

    protected void gvlist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "CONSISTENCY ID");
            cell[1].Attributes.Add("data-title", "CONSISTENCY NAME");
            cell[2].Attributes.Add("data-title", "TRS");
            cell[3].Attributes.Add("data-title", "KUTAMI");
            cell[4].Attributes.Add("data-title", "BJP");
            cell[5].Attributes.Add("data-title", "Others");
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvlist, "Select$" + e.Row.RowIndex);
            e.Row.Attributes["style"] = "cursor:pointer";
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Cookies["cookieTRS"].Value = txtTRS.Text;
            Response.Cookies["cookieBJP"].Value = txtBJP.Text;
            Response.Cookies["cookieKUTAMI"].Value = txtKUTAMI.Text;
            Response.Cookies["cookieOTHERS"].Value = txtOthers.Text;

            //Session["TRS"] = txtTRS.Text;
            //Session["BJP"] = txtBJP.Text;
            //Session["KUTAMI"] = txtKUTAMI.Text;
            //Session["OTHERS"] = txtOthers.Text;
            if (Session["SessionUserID"] != null)
            {
                objUP.USER_LOGIN_ID = Convert.ToInt16(Session["SessionUserID"]);

                PredictionTotal = int.Parse(txtTRS.Text) + int.Parse(txtBJP.Text) + int.Parse(txtKUTAMI.Text) + int.Parse(txtOthers.Text);
                objUP.TRS = txtTRS.Text;
                objUP.BJP = txtBJP.Text;
                objUP.KUTAMI = txtKUTAMI.Text;
                objUP.OTHERS = txtOthers.Text;


                RemainingTotal = 119 - PredictionTotal;
                if (PredictionTotal == 119)
                {

                    objIndexBAL.InsertUserPrediction(objUP);
                    lblMessage.Text = "Prediction details saved Successfully!!";
                    alert.Visible = true;
                    

                }
                else
                if (PredictionTotal < 119)
                {
                    lblMessage.Text = "please enter" + RemainingTotal + "seats";
                    alert.Visible = true;
                }
                else if (PredictionTotal > 119)
                {
                    lblMessage.Text = "Entered seats size exceeded to 119";
                    alert.Visible = true;
                }
                //cookies
                Response.Cookies["cookieTRS"].Expires = DateTime.Now;
                Response.Cookies["cookieBJP"].Expires = DateTime.Now;
                Response.Cookies["cookieKUTAMI"].Expires = DateTime.Now;
                Response.Cookies["cookieOTHERS"].Expires = DateTime.Now;
                Response.Cookies["cookieUrl"].Expires = DateTime.Now;
            }
            else
            {
                //Response.Cookies["temp"].Value = "login";
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //Response.Cookies["temp"].Value = "login";
            string authority = HttpContext.Current.Request.Url.Authority;
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string currentUrl = authority + path;
            Response.Cookies["cookieUrl"].Value = currentUrl;
            Response.Redirect("~/login.aspx");
            //Response.Redirect(Response.Cookies["cookieUrl"].Value);
        }
    }


}