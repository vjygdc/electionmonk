﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="district.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
      <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>District</h2>
                    <p><a href="#" class="color1">Dashboard</a> / <a href="#"  class="color1">State</a></p>
                </div>
            </div>
        </div>
          
    </div>
    <section id="contact-us">
                 <div class="container a" id="alert" visible="true" runat="server">

                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="lblResult" runat="server" Text=""></asp:Label></strong>
                    <asp:Label ID="lblMessage" runat="server" Visible="true" Text=""></asp:Label>
                </div>
            </div>
        <!-- message content -->
        <div class="container fadesimple">
            <!-- end of message content -->
            <div class="col-sm-12 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                   
    <div class="table-pricing">
        <div class="row">

            <div class="default-title">
                        <h3>District</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci distinctio vel possimus aliquam.</p>
                    </div>


             <div id="addstate" runat="server">
            <center><div class="messages"><h2>ADD/Update District</h2></div></center>
                        <div class="row">
                            <div class="col-md-06">
                                 <div class="form-group">
                                    <label for="form_name">State</label> 
                                   <asp:DropDownList ID="ddlstatus" CssClass="form-control" runat="server">
                                   </asp:DropDownList>
                                </div>
                                </div>
                                <div class="col-md-06">
                                <div class="form-group">
                                    <label for="form_name">District *</label> 
                                   <asp:TextBox ID="txtDisName" runat="server" placeholder="Enter District Name" CssClass="form-control"></asp:TextBox>
                                    <div class="help-block with-errors"></div>
                                </div>
                                    </div>
                                <div class="col-md-06">
                                <div class="form-group">
                                    <label for="form_name">Uplaod Image</label> 
                                    <asp:FileUpload  ID="imgDisImage" runat="server"/>
                                    </div>
                                    </div>
                                    <div class="col-md-06">
                                    <img src="" id="imgDisImagePreview" runat="server" style="width:25%;height:25%" />
                                    <div class="help-block with-errors"></div>
                                        </div>
                                </div>
                        
                        <div class="row">
                            <div class="col-md-12" style="text-align:right">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-warning btn-send"/>
                                <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnupdate_Click" CssClass="btn btn-warning btn-send"/>
                                <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" CssClass="btn btn-danger btn-send"  />
                            </div>
                            </div>
                        <div class="row"></div>
                 </div>
                 </div>


            </div>
        </div>
                    <div class="row">
                        <asp:Button ID="adddis" runat="server" Text="+ Add District" OnClick="adddis_Click" CssClass="btn btn-success" style="float:right" />
                    </div>
                    <br />
             <div class="row">
                <div class="table-responsive"  id="no-more-tables">

                                         <asp:GridView ID="gvDistrictsList" runat="server" Width="100%" AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab datatab" OnRowCommand="gvList_RowCommand" OnRowDataBound="gvList_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="DISTRICT_ID" HeaderText="ID" SortExpression="DISTRICT_ID" />
                                        <asp:TemplateField HeaderText="Image">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="email1" ToolTip="Email" runat="server" Text='<%#Eval("DISTRICT_IMG") %>'></asp:Label>--%>
                                                <img src='<%#Eval("DISTRICT_IMG") %>' alt="" style="width:25%;height:25%" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStateID" ToolTip="State ID" runat="server" Text='<%#Eval("STATE_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="DISTRICT NAME">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDisName" ToolTip="District Name" runat="server" Text='<%#Eval("DISTRICT_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Actions">
                                            <ItemTemplate>
                                                <div class="tg-btnsactions">
                                                    <asp:LinkButton ID="lbtnEdit" class="btn bg-green radius_none" ToolTip="Editcourse" runat="server" CommandName="Edit_Click" CommandArgument="<%# Container.DataItemIndex %>" ><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="thead-dark" />
                                </asp:GridView>
                </div>
             </div>
    </div>
                    <!-- end of header info -->
                </div>
            </div>
        <!-- container -->
        
                    
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
    <script type="text/javascript">
        $(function () {
            $('[id*=gvDistrictsList]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers", "pageLength": 5
            });
        });
    </script>
   
     <script type="text/javascript">
         window.onload = function () {
             var seconds = 3;
             setTimeout(function () {
                 document.getElementById("<%=alert.ClientID %>").style.display = "none";
                 window.location.href = "district.aspx";
             }, seconds * 1000);
         };
    </script>
</asp:Content>

