﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
    <asp:GridView ID="gvConsti" runat="server" AutoGenerateColumns="false" CssClass="Grid"
    DataKeyNames="CONSTI_ID">
    <Columns>
        <asp:TemplateField>
            <ItemTemplate>
                <asp:ImageButton ID="imgOrdersShow" runat="server" OnClick="Show_Hide_OrdersGrid" ImageUrl="~/images/plus.png"
                    CommandArgument="Show" />
                <asp:Panel ID="pnlOrders" runat="server" Visible="false" Style="position: relative">
                    <asp:GridView ID="gvparty" runat="server" AutoGenerateColumns="false" PageSize="5"
                        AllowPaging="true" OnPageIndexChanging="OnOrdersGrid_PageIndexChanging" CssClass="ChildGrid"
                        DataKeyNames="PARTY_ID">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgProductsShow" runat="server" OnClick="Show_Hide_ProductsGrid" ImageUrl="~/images/plus.png"
                                        CommandArgument="Show" />
                                    <asp:Panel ID="pnlProducts" runat="server" Visible="false" Style="position: relative">
                                        <asp:GridView ID="gvcandidate" runat="server" AutoGenerateColumns="false" PageSize="2"
                                            AllowPaging="true" OnPageIndexChanging="OnProductsGrid_PageIndexChanging" CssClass="Nested_ChildGrid">
                                            <Columns>
                                                <asp:BoundField ItemStyle-Width="150px" DataField="CANDIDATE_ID" HeaderText="CandidateID" />
                                                <asp:BoundField ItemStyle-Width="150px" DataField="CANDIDATE_NAME" HeaderText="NAME" />
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField ItemStyle-Width="150px" DataField="PARTY_ID" HeaderText="party ID" />
                            <asp:BoundField ItemStyle-Width="150px" DataField="PARTY_NAME" HeaderText="name" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField ItemStyle-Width="150px" DataField="CONSTI_ID" HeaderText="constituency ID" />
        <asp:BoundField ItemStyle-Width="150px" DataField="CONSTI_NAME" HeaderText="name" />
    </Columns>
</asp:GridView>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
</asp:Content>

