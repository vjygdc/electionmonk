﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    public string previewUrl;
    oParty objParty = new oParty();
    List<oParty> objPartyCollections = new List<oParty>();
    partyBAL objPartyBAL = new partyBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {
                // btnUpdate.Visible = false;
                updateparty.Visible = false;
                alert.Visible = false;
                if (!IsPostBack)
                    BindGrid();
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
    }
    public void BindGrid()
    {
        try
        {
            objPartyCollections = objPartyBAL.GetPartyList();
            if (objPartyCollections != null)
            {
                if (objPartyCollections.Count > 0)
                {
                    gvPartyList.DataSource = objPartyCollections;
                    gvPartyList.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnAddParty_ServerClick(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#addparty').modal('show');</script>", false);
    }

    protected void btnAdd_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (FUpartylogo.HasFile)
            {
                string strpath = System.IO.Path.GetExtension(FUpartylogo.PostedFile.FileName);
                if (strpath == ".jpg" || strpath == ".png" || strpath == ".jpeg")
                {
                    string adddate = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss") + "_";
                    string path, filename;
                    filename = adddate + Path.GetFileName(FUpartylogo.FileName);
                    path = "../img/parties/" + filename;
                    FUpartylogo.PostedFile.SaveAs(Server.MapPath("~/img/parties/") + filename);
                    int Result;
                    objParty.PARTY_NAME = txtPartyName.Text;
                    objParty.PARTY_LOGO = path;
                    objParty.CREA_BY = "ADMIN";

                    Result = objPartyBAL.InsertParty(objParty);
                    if (Result > 0)
                    {
                        lblResult.Text = "Success!";
                        lblMessage.Text = "Party added Successfully!!";
                        alert.Visible = true;
                        BindGrid();
                    }
                    txtPartyName.Text = string.Empty;

                }
            }
        }
        catch (Exception ex)
        {
            Response.Redirect("lead-generation.aspx");
        }

    }

    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            // int i = 0;
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvPartyList.Rows[index];
            Session["PartyID"] = int.Parse(row.Cells[0].Text);
            Session["PartyName"] = row.Cells[1].Text;
            if (e.CommandName == "Edit_Click")
            {
                updateparty.Visible = true;
                btnAddParty.Visible = false;
                EditParties();

            }
            if (e.CommandName == "Delete_Click")
            {
                objParty.PARTY_ID = Convert.ToInt32(Session["PartyID"]);
                objPartyBAL.DeleteParty(objParty);
                BindGrid();
                alertmod.Style.Add("background-color", "#d7ecc6");
                lblResult.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
                lblMessage.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
                lblResult.Text = "Success!";
                lblMessage.Text = Session["PartyID"] + " has been Deleted";
                alert.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void EditParties()
    {
        objParty.PARTY_ID = Convert.ToInt32(Session["PartyID"]);
        objPartyBAL.GetPartyByID(objParty);
        txtPartyNameUpdate.Text = objParty.PARTY_NAME;
        Session["PartyLogoPath"] = imgPreview.Src = objParty.PARTY_LOGO;
        imgPreview.Visible = true;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string path, filename;
        try
        {
            if (FUupdatelogo.HasFile)
            {
                string strpath = System.IO.Path.GetExtension(FUupdatelogo.PostedFile.FileName);
                if (strpath == ".png" || strpath == ".jpg" || strpath == ".jpeg")
                {
                    string adddate = DateTime.Now.ToString("MM_dd_yyyy_hh_mm_ss") + "_";
                    filename = adddate + Path.GetFileName(FUupdatelogo.FileName);
                    path = "../img/parties/" + filename;
                    FUupdatelogo.PostedFile.SaveAs(Server.MapPath("~/img/parties/") + filename);

                    objParty.PARTY_ID = Convert.ToInt32(Session["PartyID"]);
                    objParty.PARTY_NAME = txtPartyNameUpdate.Text;
                    objParty.PARTY_LOGO = path;
                    objParty.UPDA_BY = "ADMIN";

                    objPartyBAL.UpdateParty(objParty);

                    lblResult.Text = "Success!";
                    lblMessage.Text = "Party updated Successfully!!";
                    updateparty.Visible = false;
                    alert.Visible = true;
                    BindGrid();

                    txtPartyName.Text = string.Empty;

                }
            }
            else if (!FUupdatelogo.HasFile)
            {

                objParty.PARTY_ID = Convert.ToInt32(Session["PartyID"]);
                objParty.PARTY_NAME = txtPartyNameUpdate.Text;
                objParty.PARTY_LOGO = Session["PartyLogoPath"].ToString();
                objParty.UPDA_BY = "ADMIN";

                objPartyBAL.UpdateParty(objParty);

                lblResult.Text = "Success!";
                lblMessage.Text = "Party updated Successfully!!";
                updateparty.Visible = false;
                alert.Visible = true;
                BindGrid();

                txtPartyName.Text = string.Empty;
            }
            btnAddParty.Visible = true;
        }
        catch (Exception ex)
        {
            throw ex;
            // Response.Redirect("../404.html");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        updateparty.Visible = false;
        btnAddParty.Visible = true;
    }



    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "Party ID");
            cell[1].Attributes.Add("data-title", "Name");
            cell[2].Attributes.Add("data-title", "Logo");
            cell[3].Attributes.Add("data-title", "Actions");
        }
    }
}