﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="testing.aspx.cs" Inherits="testing" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    
    
          <meta property="og:url"           content="https://www.your-domain.com/your-page.html" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="Your Website Title" />
  <meta property="og:description"   content="Your description" />
  <meta property="og:image"         content="<% =abc %>"" />
    <style>
        .trs {
            background-color: #ff00ff !important;
            text-align: center;
            color: #fff !important;
        }

        .kutami {
            background-color: #008000 !important;
            text-align: center;
            color: #fff !important;
        }

        .bjp {
            background-color: #ff6600 !important;
            text-align: center;
            color: #fff !important;
        }

        .other {
            background-color: #7030a0 !important;
            text-align: center;
            color: #fff !important;
        }


        .trstxt {
            color: #ff00ff !important;
            font-size: 22px !important;
            font-weight: bold !important;
        }

        .kutamitxt {
            color: #008000 !important;
            font-size: 22px !important;
            font-weight: bold !important;
        }

        .bjptxt {
            color: #ff6600 !important;
            font-size: 22px !important;
            font-weight: bold !important;
        }

        .othertxt {
            color: #7030a0 !important;
            font-size: 22px !important;
            font-weight: bold !important;
        }

        .color1 {
            color: #000;
            font-size: 22px;
            font-weight: bold;
            text-align: left;
            width: 150px;
        }
    </style>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
<%--    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>State</h2>
                    <p><a href="#" class="color1">Dashboard</a> / <a href="#" class="color1">State</a></p>
                </div>
            </div>
        </div>
    </div>--%>
    <section id="contact-us">

        <div class="container fadesimple">
            <div class="col-sm-12 contact-info">

                <div class="col-md-6">

                    <div class="default-title">
                        <h3>
                            <asp:Label ID="lblDistrict" runat="server"></asp:Label></h3>
                    </div>
                    <img src="" alt="image description" class="img-responsive" runat="server" id="imagedis"   />
                </div>
                  <div class="col-md-6">

                         <div class="col-md-6">
       
  
                <div class="box sl1 fadesimple">
                       <h3>Partys  </h3>
                    <hr>
                    <ul class="wt-list">
                         <li class="wtl-2"><span class="trstxt"> TRS</span> 
                        <li class="wtl-1"><span  class="kutamitxt"> KUTAMI</span> 
                       
                        <li class="wtl-3"><span  class="bjptxt"> BJP</span> 
                        <li class="wtl-4"><span  class="othertxt"> others</span> 
                    </ul> 
                </div> 
                              

        </div>

                       <div class="col-md-6">
       
  
                <div class="box sl1 fadesimple">
                       <h3>Leading Count  </h3>
                    <hr>
                    <ul class="wt-list">
                         <li class="wtl-2"> 
            <asp:Label ID="TRS" runat="server" CssClass="trstxt" Text="Label"></asp:Label></li>
                        <li class="wtl-1"> 
            <asp:Label ID="KUTAMI" runat="server" CssClass="kutamitxt"  Text="Label"></asp:Label></li>
                       
                        <li class="wtl-3"> 
            <asp:Label ID="BJP" runat="server" CssClass="bjptxt"  Text="Label"></asp:Label>    </li>
                        <li class="wtl-4"> 
            <asp:Label ID="others" runat="server" CssClass="othertxt"  Text="Label"></asp:Label>  </li>
                    </ul> 
                </div> 
                              

        </div>

                    
                </div>
                <div class="col-sm-12">
                     <div class="row">

                              
                        <div class="design2" style="float: right">

                                   <div class="fb-share-button" data-href="http://supremenetsoft.com/" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div>
           
                         
                         <a class="twitter-share-button"
  href="https://twitter.com/intent/tweet">
Tweet</a>
 

                        </div>
                    </div>

                    <div class="row">
                        <div class="table-responsive" id="no-more-tables">
                        <h2 id="nodata" runat="server" style="text-align:center"></h2>
                            <asp:GridView ID="gvList" runat="server" Width="100%"  AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab" OnRowDataBound="gvList_RowDataBound" OnSelectedIndexChanged="gvList_SelectedIndexChanged" DataKeyNames="CONSISTENCY" >
                                <Columns>
                                       <asp:TemplateField HeaderText="Constituency No.">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("CONSISTENCY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-CssClass="hidden" ControlStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
                                    <asp:BoundField DataField="TRS" HeaderText="TRS" HeaderStyle-CssClass="trs" />
                                    <asp:BoundField DataField="KUTAMI" HeaderText="KUTAMI" HeaderStyle-CssClass="kutami"/>
                                    <asp:BoundField DataField="BJP" HeaderText="BJP" HeaderStyle-CssClass="bjp"/>
                                    <asp:BoundField DataField="OTHERS" HeaderText="Others" HeaderStyle-CssClass="other"/>
                                    <%--<asp:TemplateField HeaderText="status">
                                        <ItemTemplate>
                                            <asp:Label ID="lblstatus" ToolTip="Email" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="other"  />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="TRS">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTRS" ToolTip="Email" runat="server" Text='<%#Eval("TRS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="trs"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="KUTAMI">
                                        <ItemTemplate>
                                            <asp:Label ID="lblKUTAMI" ToolTip="Email" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="kutami" />
                                        
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BJP">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBJP" ToolTip="Email" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="bjp"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Others">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOTHERS" ToolTip="Email" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                        <HeaderStyle HorizontalAlign="Left"  CssClass="other"  />
                                    </asp:TemplateField>--%>

                                    


                                </Columns>
                                <HeaderStyle CssClass="thead-dark" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <!-- end of header info -->
            </div>
        </div>
        <!-- container -->

        <%--<asp:HiddenField ID="hiddenfield" runat="server" />
        <asp:Button ID="btnhidden1" runat="server" CssClass="hidden" OnClick="btnhidden1_Click"/>--%>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
    <script type="text/javascript">
        $(function () {
            $('[id*=gvList]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers", "pageLength": 5
            });
        });
    </script>
    
    <script>
        $("#<%=gvList.ClientID%> tr:has(td)").click(function (e) {
            var colIndex = $(e.target).closest("tr td").prevAll("tr td").length;;
            var rowIndex = $(e.target).closest("tr").prevAll("tr").length;;
            var selValue = $(e.target).closest("td");
            var cellData = colIndex + ', ' + rowIndex + ',' + selValue.text();
            //alert(cellData);
            $.ajax({
                type: "POST",
                url: "/testing.aspx/GetPart",
                data: "{'str':'" + cellData + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {

                }
            });
        });
   </script>
    <%--<script>
        $(document).ready(function () {
            $("#btnhidden1").click(function () {
                $.ajax({
                    type: "POST",
                    url: "/testing.aspx/GetPart",
                    data: "{'str':'" + cellData + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                    }
                });
            });
        });
    </script>--%>
    <%--<script>
        '<%Session["tempSession"] = "'+selValue.text()+'"; %> ';
            alert('<%=Session["tempSession"] %>');

        '<%Session["tempSession"] = "'+cellData+'"; %> ';

        $("#<%=hiddenfield.ClientID%>").val(cellData);
            alert($("#<%=hiddenfield.ClientID%>").val());
    </script>--%>
</asp:Content>