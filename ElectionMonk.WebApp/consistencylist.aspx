﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="consistencylist.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">Aboutus  </a></p>
                </div>
            </div>
        </div>
    </div>

    <section id="contact-us">
         <div class="container">
             <div class="row">
        <div class="col-md-06">     
            <p>Search By Dist</p>
            <asp:DropDownList ID="ddldist" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddldist_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
                 </div>
            </div>
        <div class="default-title">
            
            <asp:Button ID="Button1" runat="server" Text="Add" OnClick="Button1_Click" CssClass="btn btn-success" style="float:right;margin-right:230px" />
        </div>
        <%--alert code--%>
        <div class="container a" style="padding-right: 100px;" id="alert" visible="false" runat="server">
            <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>
                    <asp:Label ID="alertlabel" runat="server" Text=""></asp:Label>
                </strong>
            </div>
        </div>
        <%--end--%>
        <!-- message content -->
       
        <div class="container fadesimple"  id="no-more-tables">
            <h2 id="nodata" runat="server" style="text-align:center"></h2>
            <asp:Label ID="lblsession" runat="server" ForeColor="Red"></asp:Label>
            <asp:GridView ID="gvlist" runat="server" AutoGenerateColumns="false" class="table table-striped custab" OnRowCommand="gvlist_RowCommand" >
           
                <Columns>
            <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID" />
                    <asp:TemplateField HeaderText="BJP">
                                            <ItemTemplate>
                                                <asp:Label ID="BJP" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="TRS">
                                            <ItemTemplate>
                                                <asp:Label ID="TRS" runat="server" Text='<%#Eval("TRS") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="KUTAMI">
                                            <ItemTemplate>
                                                <asp:Label ID="KUTAMI" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="OTHERS">
                                            <ItemTemplate>
                                                <asp:Label ID="OTHERS" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="Status" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Statename">
                                            <ItemTemplate>
                                                <asp:Label ID="Statename" runat="server" Text='<%#Eval("Statename") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dist_name">
                                            <ItemTemplate>
                                                <asp:Label ID="Dist_name" runat="server" Text='<%#Eval("Dist_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                    <asp:TemplateField HeaderText="Const_name">
                                            <ItemTemplate>
                                                <asp:Label ID="Const_name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                     <asp:TemplateField HeaderText="Actions">
                                                <ItemTemplate>
                                                    <div class="tg-btnsactions">
                                                          <asp:LinkButton ID="editconsistuency" class="btn bg-green radius_none" ToolTip="Editcourse" runat="server" CommandName="editconsistuency" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                     <asp:LinkButton ID="deleteconsistuency" ToolTip="Deletecourse" runat="server" class="btn bg-danger  radius_none" CommandName="Deleteconsistuency" CommandArgument="<%# Container.DataItemIndex %>" ><i class="fa fa-trash"></i></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                    </Columns>

            </asp:GridView>
            <!-- end of message content -->
            <div class="col-sm-4 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-12">
                        <p class="email">hello@themekolor.co</p>
                        <p class="phone">Phone : 1-855-435-8226</p>
                        <p class="chat"><a href="#">Live Chat</a></p>
                        <p class="ticket"><a href="/contact.html">Send a Ticket</a></p>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
    <script type="text/javascript">
        var seconds = 3;
        setTimeout(function () {
            document.getElementById("<%=alert.ClientID %>").style.display = "none";
        }, seconds * 1000);

    </script>

    <script>
       $("#<%=gvlist.ClientID%> tr:has(td)").click(function (e) {
           var selTD = $(e.target).closest("td");
        <%--   '<%=tempData%>' = selTD;--%>
           //alert(selTD.text());
           $("#<%=lblsession.ClientID%>").val() = selTD.text();
          var v= $.session.set($(e.target).closest("td"));
           alert(v);

           <%--$("#<%=gvList.ClientID%> td").removeClass("selected");
           selTD.addClass("selected");
           $("#<%=gvList.ClientID%>").html(
               'Selected Cell Value is: <b> ' + selTD.text() + '</b>');--%>
        });
        </script>
</asp:Content>

