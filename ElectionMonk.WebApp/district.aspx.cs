﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    string temp;
    public string stateName;
   // List<oState> objStateCollection = new List<oState>();
    List<oDistrict> objDistrictCollection = new List<oDistrict>();
    oState oStates = new oState();
    oDistrict oDist = new oDistrict();
    DistrictBAL objDisBal = new DistrictBAL();
   // StateBAL objStateBAL = new StateBAL();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        addstate.Visible = false;
        alert.Visible = false;
      
        Bind();
        if (!IsPostBack)
        {
            BindCountry();
            //addstate.Visible = true;
        }
        //addstate.Visible = true;
    }

    protected void BindCountry()
    {
        oState dd = new oState();
        DataTable dtState = DistrictBAL.GetStateList(dd);
        ddlstatus.DataSource = dtState;
        ddlstatus.DataTextField = "STATE_NAME";
        ddlstatus.DataValueField = "STATE_ID";
        ddlstatus.DataBind();
        ddlstatus.Items.Insert(0, new ListItem("--Select State--"));
    }



    public void Bind()
    {
        try
        {
            objDistrictCollection = objDisBal.GetDistrictList();
            if (objDistrictCollection != null)
            {
                if (objDistrictCollection.Count > 0)
                {
                    gvDistrictsList.DataSource = objDistrictCollection;
                    gvDistrictsList.DataBind();

                }

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        oDist.DISTRICT_NAME = txtDisName.Text;
        oDist.DISTRICT_IMG = "../img/district/" + imgDisImage.FileName;
        oDist.STATE_ID = ddlstatus.SelectedIndex;
        oDist.CREA_BY = Session["SessionUserName"].ToString();
        objDisBal.InsertDistrict(oDist);
        alertmod.Style.Add("background-color", "#d7ecc6");
        //alert.Style.Add("background-color", "#d7ecc6");
        lblResult.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        lblMessage.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        lblResult.Text = "Success";
        lblMessage.Text = "State added successfully";
        alert.Visible = true;
    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {
        oDist.DISTRICT_NAME = txtDisName.Text;
        oDist.DISTRICT_ID = Convert.ToInt16(Session["id"]);
        oDist.DISTRICT_IMG = "../img/district/" + imgDisImage.FileName;
        oDist.STATE_ID = int.Parse(ddlstatus.SelectedValue);
        oDist.UPDA_BY = Session["SessionUserName"].ToString();
        objDisBal.UpdateDistrict(oDist);
        alertmod.Style.Add("background-color", "#d7ecc6");
        //alert.Style.Add("background-color", "#d7ecc6");
        lblResult.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        lblMessage.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        lblResult.Text = "Success";
        lblMessage.Text = "State Updated successfully";
        alert.Visible = true;
        addstate.Visible = true;
    }

    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gvDistrictsList.Rows[index];
        Session["id"] = row.Cells[0].Text;
        Session["sid"]= row.Cells[2].Text;

        if (e.CommandName == "Edit_Click")
        {
            BindCountry();
            edit();
            btnupdate.Visible = true;
            btnSubmit.Visible = false;
            btnClose.Visible = true;
            addstate.Visible = true;
        }
    }

    void edit()
    {
        oDist.DISTRICT_ID = Convert.ToInt16(Session["id"]);
        objDisBal.GetDistrictDetailsByID(oDist, oStates);
        //ddlstatus.Text = entities1.STATE_NAME;
        ddlstatus.SelectedItem.Text = oStates.STATE_NAME;
        txtDisName.Text = oDist.DISTRICT_NAME;
        imgDisImagePreview.Src = oDist.DISTRICT_IMG;

    }
    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "ID");
            cell[1].Attributes.Add("data-title", "Image");
            cell[2].Attributes.Add("data-title", "State ID");
            cell[3].Attributes.Add("data-title", "Name");
            cell[4].Attributes.Add("data-title", "Action");
        }
    }

    protected void adddis_Click(object sender, EventArgs e)
    {
        btnupdate.Visible = false;
        btnSubmit.Visible = true;
        btnClose.Visible = true;
        addstate.Visible = true;
    }

 

    protected void btnClose_Click(object sender, EventArgs e)
    {

    }
}