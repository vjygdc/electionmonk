﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class registration : System.Web.UI.Page
{
    Register register = new Register();
    RegisterBAL bAL = new RegisterBAL();
    DateTime dt;
    int age;
    string name;
    DateTime nowdate;
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        register.NAME = txtfname.Text+" "+txtlname.Text;
        register.EMAIL = txtemail.Text;
        name = txtfname.Text;
        register.DOB = txtdob.Text;
        register.GENDER = ddlgender.SelectedValue;
        register.PASSWORDS = txtpwd.Text;
        register.Mobile = txtmob.Text;
        if (general.Checked)
        {
            register.CASTE_CATEGORY = "General";
        }
       else if (bc.Checked)
        {
            register.CASTE_CATEGORY = "BC";
        }
       else if (sc.Checked)
        {
            register.CASTE_CATEGORY = "SC";
        }
        else if (st.Checked)
        {
            register.CASTE_CATEGORY = "SC";
        }

        else if (others.Checked)
        {
            register.CASTE_CATEGORY = "Others";
        }
        
        nowdate = DateTime.Now;
        dt = Convert.ToDateTime(txtdob.Text);
        age =  nowdate.Year - dt.Year;
       int count= bAL.Userexistence(register);
        if(count>=1)
        {
            alert.Visible = true;
            alertlabel.Text = "Email already exists";
        }
        else if (age <= 18)
        {
            alert.Visible = true;
            alertlabel.Text = "Your not eligible for vote";
        }
        else if(age>18 && count==0)
        { 
        
            register.EMAIL = txtemail.Text;
            register.Subject = "Registration Completed";
            register.BodyMsg = "Dear" + " " + name + " " + "your registration is completed" + "\n" + "Username :" + txtemail.Text + "\n" + "Password :" + txtpwd.Text;
            bAL.Register(register);
            bAL.Mail(register);
            txtdob.Text = string.Empty;
            txtfname.Text = string.Empty;
            txtlname.Text = string.Empty;
            txtemail.Text = string.Empty;
            alert.Visible = true;
        alertlabel.Text = "Successfully Registered";
        }
       
    }
    
}