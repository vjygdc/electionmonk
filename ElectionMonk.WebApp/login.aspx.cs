﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class login : System.Web.UI.Page
{
    #region User Variables
    List<oLogin> objUserCollection = new List<oLogin>();
    oLogin objUser = new oLogin();
    LoginDAL objLoginDAL = new LoginDAL();
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.Cookies["userid"] != null)
                txtUserName.Text = Request.Cookies["userid"].Value;
            if (Request.Cookies["pwd"] != null)
                txtUserPwd.Attributes.Add("value", Request.Cookies["pwd"].Value);
            if (Request.Cookies["userid"] != null && Request.Cookies["pwd"] != null)
                RememberMe.Checked = true;
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            objUser.UserMailId = txtUserName.Text;
            objUser.UserPwd = txtUserPwd.Text;
            objLoginDAL.UserCheckUsers(objUser);
            if (objUser.UserId != 0)
            {
                objUser.UserPwd = txtUserPwd.Text;
                objLoginDAL.UserCheckLogin(objUser);
                Session["SessionUserID"] = objUser.UserId;
                Session["SessionUserName"] = objUser.UserName;
                Session["SessionUserMailID"] = objUser.UserMailId;
                Session["UserPwd"] = objUser.UserPwd;
                Session["SessionUserType"] = objUser.UserType;
                Session["SessionUserStatus"] = objUser.UserStatus;
                if (objUser.UserId != 0 && objUser.UserName != null)
                {
                    if (RememberMe.Checked == true)
                    {
                        Response.Cookies["userid"].Value = txtUserName.Text;
                        Response.Cookies["pwd"].Value = txtUserPwd.Text;
                    }
                    //else
                    //{
                    //    Response.Cookies["userid"].Expires = DateTime.Now.AddDays(-1);
                    //    Response.Cookies["pwd"].Expires = DateTime.Now.AddDays(-1);
                    //}

                    if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
                    {
                        if (Response.Cookies["cookieUrl"].Value != null)
                        {
                            Response.Redirect(Response.Cookies["cookieUrl"].Value);
                        }
                        else
                        {
                            Response.Redirect("~/index1.aspx");
                        }
                    }
                    else if (Session["SessionUserType"].ToString().ToUpper() == "USER")
                    {
                        //Change Menu
                        if (Response.Cookies["cookieUrl"].Value != null)
                        {
                            Response.Redirect(Response.Cookies["cookieUrl"].Value);
                        }
                        else
                        {
                            Response.Redirect("~/index.aspx");
                        }
                    }
                }
                else
                {
                    Response.Write("<script>alert('UserName or Password doesn't exist');</script>");
                }
            }
            else
            {
                Response.Write("<script>alert('Invalid User');</script>");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}