﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class testing : System.Web.UI.Page
{
    public string tempData;
    public string[] partyNames = new string[4];
    public string[] cellAry = new string[4];
    List<oState> objCollection = new List<oState>();
    List<oDistrict> objCollection1 = new List<oDistrict>();
    oState entites1 = new oState();
    oDistrict entites = new oDistrict();
    DistrictBAL disbal = new DistrictBAL();
    StateBAL bal1 = new StateBAL();
    IndexBAL index = new IndexBAL();
    CandidatesBAL objCanBAL = new CandidatesBAL();
    Consistency con = new Consistency();
    string estatus;
    ConsistencyBAL bAL = new ConsistencyBAL();
    List<Consistency> objConsList = new List<Consistency>();
    public int[] intArray;
    public string abc;
    protected void Page_Load(object sender, EventArgs e)
    {
        Count();
        if (!IsPostBack)
        {
            img();
            // lblDistrict.Text = "abc";
            Session["tempSession"] = "no-data";
        }
        Bind();
    }

    public void img()
    {
        //entites.DISTRICT_ID = Convert.ToInt16(Request.QueryString["id"]);
        entites.DISTRICT_ID = 2;
        index.Displaydistrictview(entites);
        lblDistrict.Text = entites.DISTRICT_NAME;
        imagedis.Src = entites.DISTRICT_IMG;
        abc = entites.DISTRICT_IMG;
    }

    public void Bind()
    {
        try
        {
            //con.DISTRICT = Convert.ToInt16(Request.QueryString["id"]);
            con.DISTRICT = 2;
            objConsList = objCanBAL.Get_follow_upLeads(con);
            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvList.DataSource = objConsList;
                    gvList.DataBind();
                }
                else if (objConsList.Count == 0)
                {
                    nodata.InnerText = "No Data found";
                }
            }
            else if (objConsList == null)
            {
                nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void Count()
    {
        string[] stringArray = { "TRS", "KUTAMI", "BJP", "others" };
        intArray = new int[stringArray.Length];
        for (int i = 0; i < stringArray.Length - 1; i++)
        {
            con.Status = stringArray[i];
            con.DISTRICT = Convert.ToInt16(Request.QueryString["id"]);
            bAL.GetPartyCountByDistrict(con);
            intArray[i] = con.Count;
        }
        //TDP.Text = intArray[0].ToString();
        TRS.Text = intArray[0].ToString();
        KUTAMI.Text = intArray[1].ToString();
        BJP.Text = intArray[2].ToString();
        others.Text = intArray[3].ToString();
    }

    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowIndex == -1)                                   //RowIndex = -1
        {
            for (int i = 0, j = 3; j < 7; i++, j++)
            {
                partyNames[i] = e.Row.Cells[j].Text;
            }
        }
        else                                                        //RowIndex > -1
        {
            for (int i = 0, j = 3; j < 7; j++, i++)
            {
                if (e.Row.Cells[2].Text.ToUpper() == partyNames[i].ToUpper())
                {
                    e.Row.Cells[j].BackColor = Color.Blue;
                    e.Row.Cells[j].ForeColor = Color.White;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvList, "Select$" + e.Row.RowIndex);
            e.Row.Attributes["style"] = "cursor:pointer";
        }

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    TableCellCollection cell = e.Row.Cells;
        //    cell[0].Attributes.Add("data-title", "Constituency No");
        //    cell[1].Attributes.Add("data-title", "Name");
        //    cell[2].Attributes.Add("data-title", "TRS");
        //    cell[3].Attributes.Add("data-title", "Status");
        //    cell[4].Attributes.Add("data-title", "KUTAMI");
        //    cell[5].Attributes.Add("data-title", "BJP");
        //    cell[6].Attributes.Add("data-title", "Others");
        //}
    }


    protected void gvList_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Session["tempSession"] = hiddenfield.Value;
        //string newStr = Request.Cookies["tempo"].ToString();
        string newStr = Session["tempSession"].ToString();
        cellAry = Session["tempSession"].ToString().Split(',');

        for (int i = 3; i < 7; i++)
        {
            string nthng = cellAry[2];
            if (gvList.SelectedRow.Cells[i].Text == cellAry[2].ToString())
            {
                gvList.SelectedRow.Cells[i].BackColor = Color.Blue;
                gvList.SelectedRow.Cells[i].ForeColor = Color.White;
            }
            else
            {
                gvList.SelectedRow.Cells[i].BackColor = new System.Drawing.Color();
                gvList.SelectedRow.Cells[i].ForeColor = new System.Drawing.Color();
            }
            //if (gvList.SelectedRow.Cells[5].Text == "MODI")
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + gvList.SelectedRow.Cells[5].Text + "');", true);
            //    gvList.SelectedRow.Cells[5].BackColor = Color.Blue;
            //    gvList.SelectedRow.Cells[5].ForeColor = Color.White;
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + gvList.SelectedRow.Cells[5].Text + "');", true);
            //    gvList.SelectedRow.Cells[5].BackColor = new System.Drawing.Color();
            //    gvList.SelectedRow.Cells[5].ForeColor = new System.Drawing.Color();
            //}
        }

        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('" + gvList.SelectedRow.Cells[4].Text + "');", true);
    }

    [WebMethod(EnableSession = true)]
    public static void GetPart(string str)
    {
        //string strs = hiddenfield.Value;
        HttpContext.Current.Session["tempSession"] = str;

        //your code goes here
    }
}
