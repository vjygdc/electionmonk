﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class constitution : System.Web.UI.Page
{
    //string constr = ConfigurationManager.ConnectionStrings["TELANGANA"].ConnectionString;

    List<oConstitution> objlist = new List<oConstitution>();
    List<oConstitution> objstatelist = new List<oConstitution>();
    oConstitution entites = new oConstitution();
    constitutionBAL bal = new constitutionBAL();
    oConstitution objstate = new oConstitution();

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {

                //BindDistricts();
                alert.Visible = false;
                if (!IsPostBack)
                {
                    bindgrid();
                    BindStates();
                }
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
    }

    [System.Web.Script.Services.ScriptMethod()]
    [System.Web.Services.WebMethod]
    public static List<string> GetSearch(string prefixText, int count)
    {

        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager
              .ConnectionStrings["TELANGANA"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "ASP_GET_CITYNAMES_TOAUTO";
                cmd.Parameters.Add("@prefixText", SqlDbType.NVarChar).Value = prefixText;
                cmd.Connection = conn;
                conn.Open();
                List<string> customers = new List<string>();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        customers.Add(sdr["CONSTI_NAME"].ToString());
                    }
                }
                conn.Close();
                return customers;
            }
        }
    }

    protected void bindgrid()
    {
        try
        {
            objlist = bal.Getcity();
            if (objlist != null)
            {
                if (objlist.Count > 0)
                {
                    gvList.DataSource = objlist;
                    gvList.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void BindDistricts()
    {
        entites.STATE_NAME = ddlState.SelectedItem.Text;
        DataTable dtdistrict = constitutionDAL.GetDistrictslist(entites);
        ddlDistrict.DataSource = dtdistrict;
        ddlDistrict.DataTextField = "DISTRICT_NAME";
        ddlDistrict.DataValueField = "DISTRICT_NAME";
        ddlDistrict.DataBind();
        ddlDistrict.Items.Insert(0, new ListItem("--Select District--"));
    }

    public void BindStates()
    {
        DataTable dtState = constitutionDAL.GetStatesList(entites);
        ddlState.DataSource = dtState;
        ddlState.DataTextField = "STATE_NAME";
        ddlState.DataValueField = "STATE_NAME";
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("--Select State--"));
    }

    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        BindDistricts();
    }

    protected void AddCity_ServerClick(object sender, EventArgs e)
    {
        btnSubmit.Visible = true;
        btnupdateCity.Visible = false;
        ddlState.ClearSelection();
        ddlState.Enabled = true;
        txtcityname.Text = string.Empty;
        BindDistricts();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#City').modal('show');</script>", false);
    }

    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gvList.Rows[index];

        Session["id"] = (row.Cells[0].Text);
        entites.CONSTI_ID = int.Parse(row.Cells[0].Text);
        objstate.STATE_NAME = row.Cells[1].Text;
        Session["sid"] = (row.Cells[2].Text);
        objstate.DISTRICT_NAME = row.Cells[2].Text;

        entites.CONSTI_NAME = row.Cells[3].Text;

        if (e.CommandName == "EditCouse_Click")
        {
            btnSubmit.Visible = false;
            btnupdateCity.Visible = true;
            BindDistricts();


            Get_city_byid();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#City').modal('show');</script>", false);

        }
        if (e.CommandName == "DeleteCouse_Click")
        {
           // btnDelete();
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#delete').modal('show');</script>", false);

        }


    }

    protected void Get_city_byid()
    {
        Get_State_byid();
        entites.CONSTI_ID = Convert.ToInt16(Session["id"]);

        bal.GetCitybyid(entites);
        txtcityname.Text = entites.CONSTI_NAME;

        ddlState.SelectedValue = objstate.STATE_NAME;


    }
    public void Get_State_byid()
    {
        entites.CONSTI_ID = Convert.ToInt16(Session["id"]);

        bal.GetStatebyid(objstate);
        ddlState.SelectedValue = objstate.CONSTI_NAME;


    }
    
    protected void btnupdateCity_ServerClick(object sender, EventArgs e)
    {
        entites.CONSTI_ID = Convert.ToInt16(Session["id"]);

        entites.CONSTI_NAME = txtcityname.Text;
        objstate.DISTRICT_NAME = ddlState.SelectedValue;
        //objstate.UPDA_BY = "admin";
        bal.UpdateCity(entites, objstate);
        alert.Visible = true;
        alertmod.Style.Add("background-color", "#d7ecc6");
        alert.Style.Add("background-color", "#d7ecc6");
        Label3.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        Label3.Text = "Success";
        Label5.Text = "constitution updated successfully";

        bindgrid();
    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        btnupdateCity.Visible = false;
        entites.CONSTI_NAME = txtcityname.Text;
        entites.DISTRICT_NAME = ddlDistrict.SelectedItem.Value;
        //objstate.CREA_BY = "admin";
        entites.CONST_NUMBER = int.Parse(txtconstnum.Text);
        bal.InsertConstituency(entites);
        Response.Write("<script>alert('constitution added successfully!');</script>");

        bindgrid();
    }


    protected void btnDelete(object sender, EventArgs e)
    {
        entites.CONSTI_ID = Convert.ToInt16(Session["id"]);
        bal.Delete_city(entites);
        alert.Visible = true;
        alertmod.Style.Add("background-color", "#d7ecc6");
        // alert.Style.Add("background-color", "#d7ecc6");
        Label3.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
        Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
        Label3.Text = "Success!";
        Label5.Text = "city  has been Deleted";
        bindgrid();
    }
}