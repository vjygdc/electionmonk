﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {
                //exec
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch(Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
    }
}