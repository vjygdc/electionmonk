﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <script type="text/javascript">
        window.onload = function () {
            var seconds = 3;
            setTimeout(function () {
                document.getElementById("<%=alertmod.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script>
        function checkTextField(field) {
            var val = document.getElementById('<%=currentPassword.ClientID%>').value;
            if (val === "") {
                document.getElementById("error").innerText =
                    (field.value !== "") ? "Please fill this field first." : "";
                document.getElementById('<%=currentPassword.ClientID%>').focus();
            }
        }
        function pwdvisible() {
            if (document.getElementById('<%=newpwdeye.ClientID%>').classList.contains('fa-eye')) {
                document.getElementById('<%=newpwdeye.ClientID%>').classList.add('fa-eye-slash');
                document.getElementById('<%=newpwdeye.ClientID%>').classList.remove('fa-eye');
                document.getElementById('<%=newpassword.ClientID%>').type = "text";
            }
            else if (document.getElementById('<%=newpwdeye.ClientID%>').classList.contains('fa-eye-slash')) {
                document.getElementById('<%=newpwdeye.ClientID%>').classList.remove('fa-eye-slash');
                document.getElementById('<%=newpwdeye.ClientID%>').classList.add('fa-eye');
                document.getElementById('<%=newpassword.ClientID%>').type = "Password";
            }
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">

    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
        <div class="default-title">
            <h3>Registration</h3>
            <div class="container a" id="alert" visible="true" runat="server">

                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label></strong>
                    <asp:Label ID="Label5" runat="server" Visible="true" Text=""></asp:Label>
                </div>
            </div>
        </div>
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-8">
                <form action="/php/contact.php" id="contact-form" method="post" name="contact-form" novalidate="true">
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="form_name">Current Password *</label>
                                    <asp:TextBox ID="currentPassword" runat="server" CssClass="form-control floating" AutoPostBack="true" TabIndex="1" OnTextChanged="currentPassword_TextChanged"  BorderWidth="1px" BorderColor="Lightgray"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="currentPassword" ID="currentPasswordRequiredFieldValidator" runat="server" ErrorMessage="Please Enter Current password" Display="Dynamic" ForeColor="#ff9933"></asp:RequiredFieldValidator>
                                    <p id="error" style="color: orange; display: inline"></p>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                           <div class="col-md-8">
                                <div class="form-group">
                                    <label for="form_lastname">New Password *</label> 
                                    <div class="input-group">
                                        <div class="input-group-addon"> 
                                         <a href="#" id="newpwdclick" runat="server"  onclick="pwdvisible();"><i id="newpwdeye" runat="server" class="fa fa-eye"></i></a></div>
                                    <asp:TextBox ID="newpassword" runat="server" CssClass="form-control " TabIndex="2" TextMode="Password" onblur="checkTextField(this);" BackColor="#FAFAFA" BorderWidth="1px" BorderColor="Lightgray"></asp:TextBox>
                                   
                                    </div>
                                     <asp:RequiredFieldValidator ControlToValidate="newpassword" ID="newpasswordRequiredFieldValidator" runat="server" ErrorMessage="Please Enter New Password" Display="Dynamic" ForeColor="#ff9933"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="form_email">Confirm Password *</label>
                                    <asp:TextBox ID="confirmpassword" runat="server" CssClass="form-control floating" TabIndex="3" TextMode="Password"  BorderWidth="1px" BorderColor="Lightgray"></asp:TextBox>
                                    <asp:RequiredFieldValidator ControlToValidate="confirmpassword" ID="confirmpasswordRequiredFieldValidator" runat="server" ErrorMessage="Please Confirm Your Password" Display="Dynamic" ForeColor="#ff9933"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="confirmpasswordCompareValidator" runat="server" ControlToCompare="newpassword" ControlToValidate="confirmpassword" ErrorMessage="Password does not match." Display="Dynamic" ForeColor="#ff9933"></asp:CompareValidator>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btnSubmit_Click" />

                        </div>
                        </div>
                    <div class="row"></div>
                    </div>
                </form>
            </div>
            <!-- end of message content -->

        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
</asp:Content>

