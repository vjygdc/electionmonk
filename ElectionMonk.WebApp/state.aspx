﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="state.aspx.cs" Inherits="registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
        <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>State</h2>
                    <p><a href="#" class="color1">Dashboard</a> / <a href="#"  class="color1">State</a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
                 <div class="container a" id="alert" visible="true" runat="server">

                <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                    <strong>
                        <asp:Label ID="Label3" runat="server" Text=""></asp:Label></strong>
                    <asp:Label ID="Label5" runat="server" Visible="true" Text=""></asp:Label>
                </div>
            </div>
        <!-- message content -->
        <div class="container fadesimple">
            <!-- end of message content -->
            <div class="col-sm-12 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                   
    <div class="table-pricing">
        <div class="row">

            <div class="default-title">
                        <h3>ADD/Update State  </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci distinctio vel possimus aliquam.</p>
                    </div>


             <div id="addstate" runat="server">
            <center><div class="messages"><h2>ADD/Update State</h2></div></center>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_name">State *</label> 
                                   <asp:TextBox ID="text" required="" runat="server" placeholder="Enter State Name" CssClass="form-control"></asp:TextBox>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                             
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="brnclose" runat="server" Text="Close" OnClick="brnclose_Click" CssClass="btn btn-danger btn-send" />
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="float:right" CssClass="btn btn-warning btn-send"/>
                                <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnupdate_Click" Visible="false" Style="float:right" CssClass="btn btn-warning btn-send"/>
                            </div>
                        </div>
                        <div class="row"></div>
                 </div>


            </div>
        </div>
                    <div class="row">
                    </div>
             <div class="row">
                     <div class="table-responsive" id="no-more-tables">
                                           <asp:GridView ID="gvList" runat="server" Width="100%" AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped custab datatab" OnRowCommand="gvList_RowCommand" OnRowDataBound="gvList_RowDataBound">
                                    <Columns>
                                        <asp:BoundField DataField="STATE_ID" HeaderText="ID" SortExpression="STATE_ID" />
                                        <asp:TemplateField HeaderText="State Name">
                                            <ItemTemplate>
                                                <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("STATE_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>

                                       
                                        <asp:TemplateField HeaderText="Actions">
                                            <ItemTemplate>
                                                <div class="tg-btnsactions">
                                                    <asp:LinkButton ID="viewreqview" class="btn bg-green radius_none" ToolTip="Editcourse" runat="server" CommandName="Edit_Click" CommandArgument="<%# Container.DataItemIndex %>" ><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="thead-dark" />
                                </asp:GridView>
                </div>
             </div>
    </div>
                    <!-- end of header info -->
                </div>
            </div>
        <!-- container -->
        
                    
    </section>
       
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
     <script type="text/javascript">
        $(function () {
            $('[id*=gvList]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers", "pageLength": 5
            });
        });
    </script>
     <script type="text/javascript">
         window.onload = function () {
             var seconds = 3;
             setTimeout(function () {
                 document.getElementById("<%=alert.ClientID %>").style.display = "none";
                 window.location.href = "state.aspx";
             }, seconds * 1000);
         };
    </script>
</asp:Content>

