﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    public string[] partyNames = new string[4];
    List<oState> objCollection = new List<oState>();
    List<oDistrict> objCollection1 = new List<oDistrict>();
    oState entites1 = new oState();
    oDistrict entites = new oDistrict();
    DistrictBAL disbal = new DistrictBAL();
    StateBAL bal1 = new StateBAL();
    IndexBAL index = new IndexBAL();
    CandidatesBAL objCanBAL = new CandidatesBAL();
    Consistency con = new Consistency();
    string estatus;
    ConsistencyBAL bAL = new ConsistencyBAL();
    List<Consistency> objConsList = new List<Consistency>();
    public int[] intArray;
    public string abc;
    protected void Page_Load(object sender, EventArgs e)
    {
        Bind();
    }

    public void Bind()
    {
        try
        {
            con.DISTRICT = 2;//Convert.ToInt16(Request.QueryString["id"]);
            objConsList = objCanBAL.Get_follow_upLeads(con);

            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvConsCanList.DataSource = objConsList;
                    gvConsCanList.DataBind();
                }
                else if (objConsList.Count == 0)
                {

                    nodata.InnerText = "No Data found";
                }
            }
            else if (objConsList == null)
            {

                nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //new
    public Int32 GenerateRandomNo()
    {
        Int32 _min = 100000;
        Int32 _max = 999999;
        Random _rdm = new Random();
        return _rdm.Next(_min, _max);
    }
    protected void btnsubmot_Click(object sender, EventArgs e)
    {
        con.SHORT_CODE =  GenerateRandomNo();
        for (int i = 0; i < gvConsCanList.Rows.Count; i++)
        {
            con.TRS= gvConsCanList.Rows[i].Cells[2].Text;
            con.KUTAMI = gvConsCanList.Rows[i].Cells[3].Text;
            con.BJP= gvConsCanList.Rows[i].Cells[4].Text;
            con.Status = gvConsCanList.Rows[i].Cells[2].Text;
            con.Const_name = gvConsCanList.Rows[i].Cells[1].Text;
            con.OTHERS = gvConsCanList.Rows[i].Cells[5].Text;
            bAL.iNSERTGridview(con);
        }
    }
}