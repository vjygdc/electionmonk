﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="candidates.aspx.cs" Inherits="_Default" %>

<script runat="server">

    protected void btn_ADDfun_ServerClick(object sender, EventArgs e)
    {

    }
</script>


<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">

        <!-- message content -->
        <div class="container fadesimple">
            <div class="row" style="float: right">
                <div class="col-md-12">
                    <button type="button" runat="server" id="candidateadd" onserverclick="candidateadd_ServerClick" class="btn btn-primary" style="float: right"><i class="fa fa-plus " style="color: white"></i>ADD CANDIDATE</button>
                </div>
            </div>
            <div class="col-sm-12 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">

                    <div class="table-pricing">
                        <div class="row">

                            <div class="default-title">
                                <h3>Adilabad - Candiate List</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci distinctio vel possimus aliquam.</p>
                            </div>

                            <div class="row">
                                <div class="table-responsive" id="no-more-tables">

                                    <asp:GridView ID="gvList" runat="server" Width="100%"
                                        AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped " OnRowCommand="gvList_RowCommand" OnRowDataBound="gvList_RowDataBound"> 
                                        <Columns>
                                            
                                            <asp:TemplateField HeaderText="CANDIDATE ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="CANDIDATE_ID" runat="server" Text='<%#Eval("CANDIDATE_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="District">
                                                <ItemTemplate>
                                                    <asp:Label ID="District" runat="server" Text='<%#Eval("Distrct") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            
                                            
                                            <asp:TemplateField HeaderText="CONSTI_NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="CONSTI_ID" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PARTY NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="PARTY_ID" runat="server" Text='<%#Eval("PARTY_NAME1") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CANDIDATE NAME">
                                                <ItemTemplate>
                                                    <asp:Label ID="CANDIDATE_NAME" runat="server" Text='<%#Eval("CANDIDATE_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CANDIDATE IMG">
                                                <ItemTemplate>
                                                    <img height="70px" width="70px" alt='<%#Eval("CANDIDATE_NAME") %>' src='<%#Eval("CANDIDATE_IMG") %>' />
                                                    
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                                <ItemTemplate>
                                                    <div class="tg-btnsactions">
                                                        <%-- <a class="btn bg-blue radius_none" data-toggle="modal" data-target="#AddStateModel"><i class="fa fa-eye"></i></a>--%>
                                                        <asp:LinkButton ID="edit" runat="server" CommandName="Edit_Click" CommandArgument="<%# Container.DataItemIndex %>" class="btn bg-green  radius_none"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="delete" runat="server" CommandName="Delete_Click" CommandArgument="<%# Container.DataItemIndex %>" class="btn bg-danger  radius_none"><i class="fa fa-trash"></i></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="thead-dark" />
                                    </asp:GridView>


                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->
        
    </section>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
</asp:Content>

