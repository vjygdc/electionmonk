﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    List<Consistency> objConsList = new List<Consistency>();
    ConsistencyBAL bAL = new ConsistencyBAL();
    Consistency consistency = new Consistency();
    protected void Page_Load(object sender, EventArgs e)
    {
        Bind();
    }

    public void Bind()
    {
        try
        {
            consistency.SHORT_CODE = Convert.ToInt16(Request.QueryString["id"]);
            objConsList = bAL.GetUserconsistencies(consistency);

            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvConsCanList.DataSource = objConsList;
                    gvConsCanList.DataBind();
                }
                else if (objConsList.Count == 0)
                {

                    nodata.InnerText = "No Data found";
                }
            }
            else if (objConsList == null)
            {

                nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}