﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    Consistency consistency = new Consistency();
    ConsistencyBAL bAL = new ConsistencyBAL();
    DistrictBAL disbal = new DistrictBAL();
    List<Consistency> objConsList = new List<Consistency>();
    oDistrict oDistrict = new oDistrict();
    oConstituencies obj = new oConstituencies();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Bind();
            Binddis();
        }
    }


    void Binddis()
    {
        oDistrict oState = new oDistrict();
        oState.STATE_ID = 36;//int.Parse( lblstate.Text);
        DataTable dtmodeofinterviewdist = ConsistencyBAL.Getdistricts(oState);
        ddldist.DataSource = dtmodeofinterviewdist;
        ddldist.DataTextField = "DISTRICT_NAME";
        ddldist.DataValueField = "DISTRICT_ID";
        ddldist.DataBind();
        ddldist.Items.Insert(0, new ListItem("--select District--"));
    }
    public void Bind()
    {
        try
        {
            
            objConsList = bAL.GetConsistencies(obj);
            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvlist.DataSource = objConsList;
                    gvlist.DataBind();
                }
                else if (objConsList.Count == 0)
                {
                    nodata.InnerText = "No Data found";
                }
            }
            else if (objConsList == null)
            {
                nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvlist_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = Convert.ToInt32(e.CommandArgument);
        GridViewRow row = gvlist.Rows[index];
        Session["ID"] = int.Parse(row.Cells[0].Text);
        if (e.CommandName == "editconsistuency")
        {
            Session["newsState"] = "update";
            Response.Redirect("consistencyy.aspx");
        }
        else if (e.CommandName == "Deleteconsistuency")
        {
            consistency.ID = Convert.ToInt16(Session["ID"]);
            bAL.Consistency_delete(consistency);
            alert.Visible = true;
            alertlabel.Text = "Deleted Successfully";
            Bind();
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        Session["newsState"] = "insert";
        Response.Redirect("consistencyy.aspx");
    }

    protected void gvlist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "ID");
            cell[1].Attributes.Add("data-title", "BJP");
            cell[2].Attributes.Add("data-title", "TRS");
            cell[3].Attributes.Add("data-title", "KUTAMI");
            cell[4].Attributes.Add("data-title", "Others");
            cell[5].Attributes.Add("data-title", "Status");
            cell[6].Attributes.Add("data-title", "State Name");
            cell[7].Attributes.Add("data-title", "Distict Name");
            cell[8].Attributes.Add("data-title", "Const_name");
            cell[9].Attributes.Add("data-title", "Actions");
        }
    }

    protected void ddldist_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlStatus = (DropDownList)sender;
        obj.Filter = ddlStatus.SelectedValue;
        this.Bind();
    }
}