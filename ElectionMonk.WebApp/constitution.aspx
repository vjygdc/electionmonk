﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="constitution.aspx.cs" Inherits="constitution" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">

                    <div class="container a" id="alert" runat="server">
                        <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>
                                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                            </strong>
                            <asp:Label ID="Label5" runat="server" Visible="true" Text=""></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <div id="addstate" runat="server">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="title" id="defaultModalLabel">Add constitution</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row clearfix">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txtconstnum" runat="server" class="form-control" placeholder="Constitution Number"></asp:TextBox>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RFVtxtconstnum" runat="server" ErrorMessage="*" ForeColor="#FF0000" Display="Dynamic" ControlToValidate="txtconstnum"></asp:RequiredFieldValidator>
                                                    <%--<asp:RegularExpressionValidator ID="REVtxtconstnum" runat="server" ErrorMessage="Invalid Constituency Number" ForeColor="#FF0000" Display="Dynamic" ValidationExpression="[0-9]{1}[0-9]{1}[0-9]{1}" ControlToValidate="txtconstnum"></asp:RegularExpressionValidator>--%>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" class="form-control mb-3 show-tick">
                                                            <asp:ListItem>--Select State--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="ddlDistrict" runat="server" class="form-control mb-3 show-tick">
                                                            <asp:ListItem>--Select District--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <asp:TextBox ID="txtcityname" runat="server" class="form-control" placeholder="City Name"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="float: right" CssClass="btn btn-warning btn-send " />
                                            <asp:Button ID="btnupdateCity" runat="server" Text="Update" OnClick="btnupdateCity_ServerClick" Visible="false" Style="float: right" CssClass="btn btn-warning btn-send  " />
                                            <%-- <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    </div>
                                </div>
                                <div class="row"></div>
                            </div>
                            <ul class="header-dropdown">
                                <li>
                                    <%--    <button type="button" id="AddCity" runat="server" onserverclick="AddCity_ServerClick" class="btn btn-info " style="float: right">Add</button>--%>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true">
                            </asp:ScriptManager>

                            <div class="table-responsive">
                                <asp:GridView ID="gvList" runat="server" Width="100%"
                                    AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped" OnRowCommand="gvList_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="CONST_NUMBER" HeaderText="CONST_NUMBER " SortExpression="CONST_NUMBER" />
                                        <asp:BoundField DataField="STATE_NAME" HeaderText="State Name" SortExpression="STATE_NAME" />
                                        <asp:BoundField DataField="DISTRICT_NAME" HeaderText="District Name" SortExpression="DISTRICT_NAME" />
                                        <asp:TemplateField HeaderText="Constituency Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblcityName" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Actions">
                                            <ItemTemplate>
                                                <div class="tg-btnsactions">
                                                    <asp:LinkButton ID="lbtnedit" class="btn bg-green radius_none" ToolTip="Editcourse" runat="server" CommandName="EditCouse_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDelete" ToolTip="Deletecourse" runat="server" class="btn bg-danger  radius_none" CommandName="DeleteCouse_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle CssClass="thead-dark" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--  <div class="modal fade" id="#City" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="title" id="defaultModalLabel">Add constitution</h6>
                </div>
                <div class="modal-body">
                    <div class="row clearfix">
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlState" runat="server" class="form-control mb-3 show-tick">
                                    <asp:ListItem>--Select State--</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <asp:TextBox ID="txtcityname" runat="server" class="form-control" placeholder="Name"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnaddCity" runat="server" onserverclick="btnaddCity_ServerClick" class="btn btn-primary">Add</button>
                    <button type="button" id="btnupdateCity" runat="server" onserverclick="btnupdateCity_ServerClick" class="btn btn-primary">Update</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>--%>
    <!--delete popup-->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog">
        <div class="sweet-alert showSweetAlert  " style="display: block; margin-top: -167px;">
            <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                <span class="sa-body pulseWarningIns"></span>
                <span class="sa-dot pulseWarningIns"></span>
            </div>
            <div class="sa-icon sa-custom" style="display: none;"></div>
            <h2>Are you sure?</h2>
            <p style="display: block;">You will not be able to recover this imaginary file!</p>
            <div class="sa-button-container">
                <button id="btnno" class="cancel" tabindex="2" style="display: inline-block; box-shadow: none;">Cancel</button>
                <div class="sa-confirm-button-container">
                    <%--    <button id="btnyes" class="confirm" runat="server" onserverclick="btnyes_ServerClick" tabindex="1" style="display: inline-block; background-color: rgb(220, 53, 69); box-shadow: rgba(220, 53, 69, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset;">Yes, delete it!</button>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
    <script type="text/javascript">
        $(function () {
            $('[id*=gvList]').prepend($("<thead></thead>").append($(this).find("tr:first"))).DataTable({
                "responsive": true,
                "sPaginationType": "full_numbers", "pageLength": 5
            });
        });

        //Alert function
        window.setTimeout(function () {
            $(".alert").fadeTo(500, 0).slideUp(500, function () {
                $(this).remove();
            });
        }, 4000);
         //end alert function
    </script>

</asp:Content>

