﻿<%@ Page Title="Registered - Users" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="registered-users.aspx.cs" Inherits="registered_users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
        <div class="container fadesimple">
            <div class="col-sm-12 contact-info">
                <div class="col-sm-12">
                    <div class="table-pricing">
                        <div class="row">
                            <div class="default-title">
                                <h3>Registered - Users</h3>
                                <p>The followed Users are registered in our application</p>
                            </div>

                            <div class="row"  id="no-more-tables">
                                <div class="table-responsive">
                                    <asp:GridView ID="gvList" runat="server" Width="100%" OnRowCommand="gvList_RowCommand"
                                        AutoGenerateColumns="False" ClientIDMode="Static" class="table table-striped" OnRowDataBound="gvList_RowDataBound">
                                        <Columns>
                                            <asp:BoundField DataField="UserId" HeaderText="User ID" SortExpression="UserId" />
                                            <asp:TemplateField HeaderText="User Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Email ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("UserMailId") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Gender">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGender" runat="server" Text='<%#Eval("UserGender") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Actions">
                                                <ItemTemplate>
                                                    <div class="tg-btnsactions">
                                                        <asp:LinkButton ID="lnkbtnedit" class="btn bg-green radius_none" ToolTip="Edit User" runat="server" CommandName="EditUser_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbtndelete" ToolTip="Delete User" runat="server" class="btn bg-danger  radius_none" CommandName="DeleteUser_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-trash"></i></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="thead-dark" />
                                    </asp:GridView>

                                    <%--                                    <table class="table table-striped custab">
                                        <thead>
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Constituency</th>
                                                <th>State</th>
                                                <th>State</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Constituency</th>
                                            <th>State</th>
                                            <th>State</th>
                                            <td>
                                                <a class="btn   btn-warning radius5 btn-sm"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                <a class="btn   btn-red radius5 btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->

        <!--delete popup-->
        <div class="modal fade" id="deleteconfirm" tabindex="-1" role="dialog">
            <div class="sweet-alert showSweetAlert  " style="display: block; margin-top: -167px;">
                <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                    <span class="sa-body pulseWarningIns"></span>
                    <span class="sa-dot pulseWarningIns"></span>
                </div>
                <div class="sa-icon sa-custom" style="display: none;"></div>
                <h2>Are you sure?</h2>
                <p style="display: block;">You will not be able to recover this imaginary file!</p>
                <div class="sa-button-container">
                    <button id="btnno" class="cancel" tabindex="2" style="display: inline-block; box-shadow: none;">Cancel</button>
                    <div class="sa-confirm-button-container">
                        <button id="btnyes" class="confirm" runat="server" onserverclick="btnyes_ServerClick" tabindex="1" style="display: inline-block; background-color: rgb(220, 53, 69); box-shadow: rgba(220, 53, 69, 0.8) 0px 0px 2px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px inset;">Yes, delete it!</button>
                    </div>
                </div>
            </div>
        </div>
        <!--end-->

    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
    <%--<script>
        $(document).ready(function () {
            alert('<%=currentUrl %>');
        });
    </script>--%>
</asp:Content>



