﻿<%@ Page Title="" Language="C#" MasterPageFile="cpc.master" AutoEventWireup="true" CodeFile="registration.aspx.cs" Inherits="registration" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="dynamic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">
    <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Register</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#" class="color1">Aboutus  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
        <div class="default-title">
            <h3>Registration</h3>

        </div>
        <%--alert code--%>
        <div class="container a" style="padding-right: 100px;" id="alert" visible="false" runat="server">
            <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <strong>
                    <asp:Label ID="alertlabel" runat="server" Text=""></asp:Label>
                </strong>
            </div>
        </div>
        <%--end--%>
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-8">

                <div class="messages">
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">First Name</label>
                                    <asp:TextBox ID="txtfname" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtfname"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Last Name</label>
                                    <asp:TextBox ID="txtlname" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtlname"></asp:RequiredFieldValidator>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">Email Id</label><asp:TextBox ID="txtemail" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ErrorMessage="*" ForeColor="#ff9933" Display="Dynamic" ControlToValidate="txtemail"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="empemailRegularExpressionValidator" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="#ff9933" Display="Dynamic" ControlToValidate="txtemail" ErrorMessage=" *Invalid Email Format"></asp:RegularExpressionValidator>
                                    <div class="help-block with-errors"></div>
                                </div>

                            </div>



                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">Password</label>
                                    <asp:TextBox ID="txtpwd" runat="server" TextMode="Password" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtpwd"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegExp1" runat="server" ForeColor="Orange"
                                        ErrorMessage="Password length must be between 7 to 10 characters" Display="Dynamic"
                                        ControlToValidate="txtpwd"
                                        ValidationExpression="^[a-zA-Z0-9'@&#.\s]{7,10}$" />

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_message">Confirm Password</label>
                                    <asp:TextBox ID="txtcnfpwd" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server"
                                        ControlToValidate="txtcnfpwd"
                                        CssClass="ValidationError"
                                        ControlToCompare="txtpwd"
                                        ErrorMessage="Password and Confirm password should be same"
                                        ToolTip="Password must be the same" ForeColor="Orange" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldator6" runat="server"
                                        ErrorMessage="*"
                                        ControlToValidate="txtcnfpwd"
                                        CssClass="ValidationError"
                                        ToolTip="Compare Password is a REQUIRED field" ForeColor="Orange">
                                    </asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_message">Gender</label>
                                    <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="Male">Male</asp:ListItem>
                                        <asp:ListItem Value="Female">Female</asp:ListItem>
                                    </asp:DropDownList>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">DOB</label><asp:TextBox ID="txtdob" runat="server" CssClass="form-control"></asp:TextBox>
                                    <dynamic:CalendarExtender ID="CalendarExtender4"
                                        runat="server" PopupButtonID="imgPopup"
                                        TargetControlID="txtdob" />

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtdob"></asp:RequiredFieldValidator>

                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="form_phone">Mobile</label><asp:TextBox ID="txtmob" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic" runat="server" ErrorMessage="*" ForeColor="Orange" ControlToValidate="txtmob"></asp:RequiredFieldValidator>

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                        ControlToValidate="txtmob" ErrorMessage="Not valid" ForeColor="Orange" Display="Dynamic"
                                        ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>

                                    <div class="help-block with-errors"></div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <h4>Category</h4>
                                <div class="col-md-2">
                                    <input type="radio" name="Category" runat="server" id="general" />General
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="Category" runat="server" id="bc" />BC
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="Category" runat="server" id="sc" />SC
                                </div>
                                <div class="col-md-2">
                                    <input type="radio" name="Category" runat="server" id="st" />ST
                                </div>

                                <div class="col-md-2">
                                    <input type="radio" name="Category" runat="server" id="others" />Others
                                </div>
                               
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="gender" runat="server" ErrorMessage="*" ForeColor="Orange"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit" runat="server" Style="float: right" Text="Submit" CssClass="btn btn-warning btn-send" OnClick="btnSubmit_Click" />


                            </div>
                        </div>
                        <div class="row"></div>
                    </div>
                </div>

            </div>
            <!-- end of message content -->
            <div class="col-sm-4 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-12">
                        <p class="email">hello@themekolor.co</p>
                        <p class="phone">Phone : 1-855-435-8226</p>
                        <p class="chat"><a href="#">Live Chat</a></p>
                        <p class="ticket"><a href="/contact.html">Send a Ticket</a></p>
                    </div>
                    <!-- end of header info -->
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">

    <script type="text/javascript">
        var seconds = 3;
        setTimeout(function () {
            document.getElementById("<%=alert.ClientID %>").style.display = "none";
        }, seconds * 1000);
    </script>


</asp:Content>

