﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class candidateslist : System.Web.UI.Page
{
    Consistency con = new Consistency();

    ConsistencyBAL bAL = new ConsistencyBAL();
    List<Consistency> objConsList = new List<Consistency>();
    
    protected void Page_Load(object sender, EventArgs e)
    { 
        Bind();
    }
    public void Bind()
    {
        try
        {
            
            objConsList = bAL.Getstateslist(con);

            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvCandidatelist.DataSource = objConsList;
                    gvCandidatelist.DataBind();
                }
                else if (objConsList.Count == 0)
                {

                   // nodata.InnerText = "No Data found";
                }

            }
            else if (objConsList == null)
            {

               // nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }




    protected void gvCandidatelist_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            


            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "Constituency No");
            cell[1].Attributes.Add("data-title", "Name");
            cell[2].Attributes.Add("data-title", "TRS");
            cell[3].Attributes.Add("data-title", "KUTAMI");
            cell[4].Attributes.Add("data-title", "BJP");
            cell[5].Attributes.Add("data-title", "Others");
        }
         
    }
}