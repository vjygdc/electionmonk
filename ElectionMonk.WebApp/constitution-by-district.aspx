﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="constitution-by-district.aspx.cs" Inherits="constitution_by_district" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
    
        <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#"  class="color1"> About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
     
        <!-- message content -->
        <div class="container fadesimple">
           
            <!-- end of message content -->
            <div class="col-sm-12 contact-info">
                <!-- header contact info -->
                <div class="col-sm-12">
   <div class="default-title">
            <h3>Login</h3>
            <p>you need to Login to support your Party.</p>
        </div>
                     
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">

                                     <h4 runat="server" id="nodata" style="text-align:center"></h4>
            <asp:GridView ID="gvList" runat="server" Width="100%"  
                                        AutoGenerateColumns="False" ClientIDMode="Static" CssClass="table table-hover js-basic-example dataTable table-custom m-b-0 c_list" >
                                        <Columns>
                                            <asp:TemplateField HeaderText="ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAffiliatorName" runat="server" Text='<%#Eval("CONSTI_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAffiliatorName" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAffiliatorName" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                          
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAffiliatorName" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                          
                                            <asp:TemplateField HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAffiliatorName" runat="server" Text='<%#Eval("CONSTI_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                          
                                          
											
											
                                            <asp:TemplateField HeaderText="Actions">
                                                <ItemTemplate>
                                                    <div class="tg-btnsactions">
                                                        <asp:LinkButton ID="lbtnEdit" class="btn bg-primary radius_none" runat="server"  CommandName="View_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-eye" style="color:white"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton1" class="btn bg-primary radius_none" runat="server"  CommandName="Edit_Click" CommandArgument="<%# Container.DataItemIndex %>"><i class="fa fa-pencil" style="color:white"></i></asp:LinkButton>
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>
                <HeaderStyle CssClass="thead-dark" />
                                    </asp:GridView>
                                </div>
                            </div>
                     
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
</asp:Content>

