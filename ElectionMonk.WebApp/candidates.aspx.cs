﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    List<Candidates> objCandidateslist = new List<Candidates>();
    Candidates objcandidates = new Candidates();
    CandidatesBAL objcandidatesbal = new CandidatesBAL();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        gvlistbind();
    }

    public void gvlistbind()
    {
        try
        {
            objCandidateslist = objcandidatesbal.GetCandidates();
            if (objCandidateslist != null)
            {
                if (objCandidateslist.Count > 0)
                {
                    gvList.DataSource = objCandidateslist;
                    gvList.DataBind();
                }
                else
                {
                    Response.Write("<script>alert('No Data Found')</script>");
                }
            }

        }
        catch (Exception ex)
        {
            //Response.Redirect("404.html", false);
            throw ex;
        }

    }

   
       
    protected void gvList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gvList.Rows[index];
            //Session["id"] = int.Parse(row.Cells[0].Text);
            Session["id"] = Convert.ToInt32(((Label)(gvList.Rows[index].FindControl("CANDIDATE_ID"))).Text);
            
            if (e.CommandName == "Edit_Click")
            {
                //EditPanel.Visible = true;
                Session["newsState"] = "update";

                Response.Redirect("candidates-add.aspx");

            }
            else if (e.CommandName == "Delete_Click")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ModalView", "<script>$('#confirm').modal('show');</script>", false);

            }

        }


        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void candidateadd_ServerClick(object sender, EventArgs e)
    {
        Session["newsState"] = "insert";
        Response.Redirect("candidates-add.aspx");

    }

    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "ID");
            cell[1].Attributes.Add("data-title", "District");
            cell[2].Attributes.Add("data-title", "CONSTI_NAME");
            cell[3].Attributes.Add("data-title", "PARTY NAME"); 
            cell[4].Attributes.Add("data-title", "CANDIDATE NAME");
            cell[5].Attributes.Add("data-title", "CANDIDATE IMG");
            cell[6].Attributes.Add("data-title", "Action");
        }
    }
}