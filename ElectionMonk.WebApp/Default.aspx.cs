﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int ID = Convert.ToInt16(Request.QueryString["id"]);
            gvConsti.DataSource = GetData("select * FROM CONSTITUENCIES WHERE DISTRICT_ID='"+ID+"'" );
            gvConsti.DataBind();
        }
    }
    private static DataTable GetData(string query)
    {
        string constr = ConfigurationManager.ConnectionStrings["ATSDBSTRING"].ConnectionString;
        using (SqlConnection con = new SqlConnection(constr))
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = query;
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataSet ds = new DataSet())
                    {
                        DataTable dt = new DataTable();
                        sda.Fill(dt);
                        return dt;
                    }
                }
            }
        }
    }
    protected void Show_Hide_OrdersGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlOrders").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            string customerId = gvConsti.DataKeys[row.RowIndex].Value.ToString();
            GridView gvOrders = row.FindControl("gvparty") as GridView;
            BindOrders(customerId, gvOrders);
        }
        else
        {
            row.FindControl("pnlOrders").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindOrders(string customerId, GridView gvparty)
    {
        gvparty.ToolTip = customerId;
        gvparty.DataSource = GetData(string.Format("select * from POLITICAL_PARTIES "));
        gvparty.DataBind();
    }

    protected void OnOrdersGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvparty = (sender as GridView);
        gvparty.PageIndex = e.NewPageIndex;
        BindOrders(gvparty.ToolTip, gvparty);
    }
    protected void Show_Hide_ProductsGrid(object sender, EventArgs e)
    {
        ImageButton imgShowHide = (sender as ImageButton);
        GridViewRow row = (imgShowHide.NamingContainer as GridViewRow);
        if (imgShowHide.CommandArgument == "Show")
        {
            row.FindControl("pnlProducts").Visible = true;
            imgShowHide.CommandArgument = "Hide";
            imgShowHide.ImageUrl = "~/images/minus.png";
            int orderId = Convert.ToInt32((row.NamingContainer as GridView).DataKeys[row.RowIndex].Value);
            GridView gvProducts = row.FindControl("gvcandidate") as GridView;
            BindProducts(orderId, gvProducts);
        }
        else
        {
            row.FindControl("pnlProducts").Visible = false;
            imgShowHide.CommandArgument = "Show";
            imgShowHide.ImageUrl = "~/images/plus.png";
        }
    }

    private void BindProducts(int orderId, GridView gvcandidate)
    {
        gvcandidate.ToolTip = orderId.ToString();
        gvcandidate.DataSource = GetData(string.Format("select CANDIDATE_NAME,CANDIDATE_ID from CANDIDATES  JOIN  POLITICAL_PARTIES ON POLITICAL_PARTIES.PARTY_ID = CANDIDATES.PARTY_ID AND CANDIDATES.PARTY_ID = '{0}'", orderId));
        gvcandidate.DataBind();
    }

    protected void OnProductsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvProducts = (sender as GridView);
        gvProducts.PageIndex = e.NewPageIndex;
        BindProducts(int.Parse(gvProducts.ToolTip), gvProducts);
    }
}