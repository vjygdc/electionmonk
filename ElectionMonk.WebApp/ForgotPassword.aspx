﻿<%@ Page Title="" Language="C#" MasterPageFile="~/cpc.master" AutoEventWireup="true" CodeFile="ForgotPassword.aspx.cs" Inherits="_Default" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" Runat="Server">
     <script type="text/javascript">
                window.onload = function () {
                    var seconds = 4;
                    setTimeout(function () {
                        document.getElementById("<%=alertmod.ClientID %>").style.display = "none";
                    }, seconds * 1000);
                };
            </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" Runat="Server">
     <div class="jumbotron header-simple">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>About us</h2>
                    <p><a href="#" class="color1">Home</a> / <a href="#"  class="color1"> About us  </a></p>
                </div>
            </div>
        </div>
    </div>
    <section id="contact-us">
        <div class="default-title">
            <h3>Forgot Password</h3>
            <div class="alert alert-success" role="alert" runat="server" id="alertmod" visible="false">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>
                                    <asp:Label ID="alertlabel" runat="server"></asp:Label></strong>
                            </div>
        </div>
        <!-- message content -->
        <div class="container fadesimple">
            <div class="col-sm-8">
                <form action="/php/contact.php" id="contact-form" method="post" name="contact-form" novalidate="true">
                    <div class="messages"></div>
                    <div class="controls">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_name">Enter your registered Email ID *</label><asp:Textbox ID="txtemailid" class="form-control" runat="server" placeholder="Your Email Id *" required="required" data-error="Email ID is required." /> 
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            </div>
                           <div class="col-md-12">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn btn-primary"  />
                             
                               
                            </div>
                      <%--  <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Email *</label> <input class="form-control" data-error="Valid email is required." id="form_email" name="email" placeholder="Your email *" required="required" type="email">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">Phone</label> <input class="form-control" id="form_phone" name="phone" placeholder="Your phone" type="tel">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="form_message">Message *</label>
                                    <textarea class="form-control" data-error="Please,leave us a message." id="form_message" name="message" placeholder="Message*" required="required" rows="4"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-warning btn-send disabled"  />
                               
                            </div>
                        </div>
                        <div class="row"></div>--%>
                    </div>
                </form>
            </div>
            <!-- end of message content -->
             <%--<asp:LinkButton ID="capture" runat="server" CssClass="btn btn-primary" OnClick="capture_Click" />--%>
        </div>
        <!-- container -->
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" Runat="Server">
    
        <script src="js/jquery.min-1.11.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/login-backstretch.min.js"></script>
    <script src="js/login-typica.js"></script>
</asp:Content>

