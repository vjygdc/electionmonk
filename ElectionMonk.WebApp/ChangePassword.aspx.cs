﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    List<Register> objchangeselect = new List<Register>();
    Register objchange = new Register();
    ChangePasswordBAL objchangebal = new ChangePasswordBAL();

    protected void Page_Load(object sender, EventArgs e)
    {
        alert.Visible = false;
        try
        {
            if (Session["SessionUserType"].ToString() != null && Session["SessionUserID"].ToString() != null)
            {
                //exec
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }

    }
    protected void currentPassword_TextChanged(object sender, EventArgs e)
    {
        currentPassword.Attributes.Add("onblur", "this.type='password';");
        objchange.PASSWORDS = currentPassword.Text;
         objchange.Reg_id =Convert.ToInt16( Session["SessionUserID"]);
        objchangebal.Edit(objchange);
        if (objchange.Count == 1)
        {
            currentPassword.Attributes.Add("onfocus", "this.type='text';");
            currentPassword.Attributes.Add("onfocus", "this.type='password';");
            currentPassword.Attributes["TextMode"] = "Password";
            newpassword.Focus();
        }
        else
        {
            currentPasswordRequiredFieldValidator.Text = "Please Enter Correct Password";
            currentPasswordRequiredFieldValidator.IsValid = false;
            currentPassword.Text = string.Empty.ToString();
            currentPassword.Focus();
        }
    }
    protected void SentEmailforpwdchanging()
    {
        try
        {

            using (MailMessage mm = new MailMessage("helpdesk.snpl@gmail.com", Session["SessionUserMailID"].ToString()))
            {
                mm.Subject = "Password Changing";
                mm.Body = "Your Password changed successfully";
                //mm.Body = "Password Changed Successfully..!";
                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.UseDefaultCredentials = true;
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                System.Net.NetworkCredential NetworkCred = new NetworkCredential("helpdesk.snpl@gmail.com", "jdzydiyrrvfnrjbf");
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
                alertmod.Style.Add("background-color", "#d7ecc6");
                alert.Style.Add("background-color", "#d7ecc6");
                Label3.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
                Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
                Label3.Text = "Success";
                Label5.Text = "Password Changed successfully";
                alert.Visible = true;
                currentPassword.Text = string.Empty;
                //alertlabel.Text = "Password Changed Successfully!!";
                //alertmod.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

   
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
             objchange.EMAIL = Session["SessionUserMailID"].ToString();
            //objchange.EMAIL = "eswardatta277@gmail.com";
            objchange.PASSWORDS = newpassword.Text;
            objchangebal.UpdatePassword(objchange);
            SentEmailforpwdchanging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}