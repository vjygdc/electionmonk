﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    public string[] partyNames = new string[4];
    List<oState> objCollection = new List<oState>();
    List<oDistrict> objCollection1 = new List<oDistrict>();
    oState entites1 = new oState();
    oDistrict entites = new oDistrict();
    DistrictBAL disbal = new DistrictBAL();
    StateBAL bal1 = new StateBAL();
    IndexBAL index = new IndexBAL();
    CandidatesBAL objCanBAL = new CandidatesBAL();
    Consistency con = new Consistency();
    string estatus;
    ConsistencyBAL bAL = new ConsistencyBAL();
    List<Consistency> objConsList = new List<Consistency>();
    public int[] intArray;
    public string abc;
    protected void Page_Load(object sender, EventArgs e)
    {
        Count();
        if (!IsPostBack)
        {
            img();
            Bind();
            // lblDistrict.Text = "abc";
        }
    }

    public void img()
    {
        entites.DISTRICT_ID = Convert.ToInt16(Request.QueryString["id"]);
        index.Displaydistrictview(entites);
        lblDistrict.Text = entites.DISTRICT_NAME;
        imagedis.Src = entites.DISTRICT_IMG;
        abc = entites.DISTRICT_IMG;
    }

    public void Bind()
    {
        try
        {
            con.DISTRICT = Convert.ToInt16(Request.QueryString["id"]);
            objConsList = objCanBAL.Get_follow_upLeads(con);

            if (objConsList != null)
            {
                if (objConsList.Count > 0)
                {
                    gvConsCanList.DataSource = objConsList;
                    gvConsCanList.DataBind();
                }
                else if (objConsList.Count == 0)
                {

                    nodata.InnerText = "No Data found";
                }
            }
            else if (objConsList == null)
            {

                nodata.InnerText = "No Data found";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void Count()
    {
        int k;
        string[] stringArray = { "TRS", "KUTAMI", "BJP", "others" };
        intArray = new int[stringArray.Length];
        for (int i = 0; i < stringArray.Length - 1; i++)
        {
            con.Status = stringArray[i];
            con.DISTRICT = Convert.ToInt16(Request.QueryString["id"]);
            bAL.GetPartyCountByDistrict(con);
            intArray[i] = con.Count;
        }
        //TDP.Text = intArray[0].ToString();
        txtTRS.Text = intArray[0].ToString();
        txtKUTAMI.Text = intArray[1].ToString();
        txtBJP.Text = intArray[2].ToString();
        txtothers.Text = intArray[3].ToString();

    }
    public Int32 GenerateRandomNo()
    {
        Int32 _min = 100000;
        Int32 _max = 999999;
        Random _rdm = new Random();
        return _rdm.Next(_min, _max);
    }
    protected void gvList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1)                                   //RowIndex = -1
        {
            for (int i = 0, j = 2; j < 6; i++, j++)
            {
                partyNames[i] = e.Row.Cells[j].Text;
            }
        }
        else                                                        //RowIndex > -1
        {
            for (int i = 0, j = 2; j < 6; j++, i++)
            {
                if (e.Row.Cells[6].Text.ToUpper() == partyNames[i].ToUpper())
                {
                    e.Row.Cells[j].Attributes["class"] = "greencolor";
                    e.Row.Cells[j].ForeColor = Color.White;
                }
            }
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TableCellCollection cell = e.Row.Cells;
            cell[0].Attributes.Add("data-title", "Constituency No");
            cell[1].Attributes.Add("data-title", "Name");
            cell[2].Attributes.Add("data-title", "TRS");
            cell[3].Attributes.Add("data-title", "KUTAMI");
            cell[4].Attributes.Add("data-title", "BJP");
            cell[5].Attributes.Add("data-title", "Others");
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserID"].ToString() != null && Session["SessionUserStatus"].ToString().ToUpper() == "ADMIN")
            {
                con.SHORT_CODE = GenerateRandomNo();
                con.UserID = Convert.ToInt16(Session["SessionUserID"]);
                for (int i = 0; i < gvConsCanList.Rows.Count; i++)
                {
                    con.Constid = int.Parse(gvConsCanList.Rows[i].Cells[0].Text);
                    con.TRS = gvConsCanList.Rows[i].Cells[2].Text;
                    con.KUTAMI = gvConsCanList.Rows[i].Cells[3].Text;
                    con.BJP = gvConsCanList.Rows[i].Cells[4].Text;
                    con.Status = gvConsCanList.Rows[i].Cells[2].Text;
                    con.Const_name = gvConsCanList.Rows[i].Cells[1].Text;
                    con.OTHERS = gvConsCanList.Rows[i].Cells[5].Text;
                    bAL.iNSERTGridview(con);
                }
                alert.Visible = true;
                alertlabel.Text = "Saved Successfully";
                Response.Cookies["cookieUrl"].Expires = DateTime.Now;
            }
        }
        catch (Exception ex)
        {
            string authority = HttpContext.Current.Request.Url.Authority;
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            string currentUrl = authority + path;
            Response.Cookies["cookieUrl"].Value = currentUrl;
            //Response.Redirect(Response.Cookies["cookieUrl"].Value);
            Response.Redirect("~/login.aspx");
            //throw ex;
        }
    }
}
