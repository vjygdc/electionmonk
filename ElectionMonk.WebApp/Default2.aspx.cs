﻿using ASPSnippets.FaceBookAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;

public partial class Default2 : System.Web.UI.Page
{
    List<oDistrict> objCollection1 = new List<oDistrict>();
    oState entites1 = new oState();
    oDistrict entites = new oDistrict();
    DistrictBAL disbal = new DistrictBAL();
    StateBAL bal1 = new StateBAL();
    IndexBAL index = new IndexBAL();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        img();
        FaceBookConnect.API_Key = "<API Key>";
        FaceBookConnect.API_Secret = "<API Secret>";
        if (!IsPostBack)
        {
            if (Request.QueryString["login"] == "1")
            {
                FaceBookConnect.Authorize("publish_actions", Request.Url.AbsoluteUri.Split('?')[0]);
                return;
            }
            if (Request.QueryString["code"] != null)
            {
                Session["Code"] = Request.QueryString["code"];
                ClientScript.RegisterStartupScript(GetType(), "script", "window.opener.location.reload(); window.close();", true);
                return;
            }
            if (Session["Code"] != null)
            {
              //  imgFaceBook.AlternateText = "share";
              //  imgFaceBook.ImageUrl = "~/img/fbshare.png";
            }
           
        }
    }

    public void img()
    {
        entites.DISTRICT_ID = 1;
        index.Displaydistrictview(entites);
        lblDistrict.Text = entites.DISTRICT_NAME;
        imagedis.Src = entites.DISTRICT_IMG;
    }

    protected void image_Click(object sender, EventArgs e)
    {
        Session["File"] = entites.DISTRICT_IMG;
        Session["Message"] = entites.DISTRICT_NAME;
        FaceBookConnect.Authorize("user_photos,publish_actions", Request.Url.AbsoluteUri.Split('?')[0]);
    }
}