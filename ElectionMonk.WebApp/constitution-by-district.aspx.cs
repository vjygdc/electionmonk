﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class constitution_by_district : System.Web.UI.Page
{
    List<oConstituencies> objEventslist = new List<oConstituencies>();
    oConstituencies entities = new oConstituencies();
    IndexBAL eventbal = new IndexBAL();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["SessionUserType"].ToString().ToUpper() == "ADMIN")
            {
                //exec
                if(!IsPostBack)
                gveventsbind();
            }
            else
            {
                Response.Redirect("~/login.aspx");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            Response.Redirect("~/login.aspx");
        }
    }
    public void gveventsbind()
    {
        try
        {

            entities.DISTRICT_ID = Convert.ToInt16(Request.QueryString["id"]);
            objEventslist = eventbal.Displayrequirementview(entities);
            if (objEventslist != null)
            {
                if (objEventslist.Count > 0)
                {
                    gvList.DataSource = objEventslist;
                    gvList.DataBind();

                }
                else if (objEventslist.Count <= 0)
                {
                    nodata.InnerHtml = "No Records Found";
                }
            }
            else if (objEventslist == null)
            {
                nodata.InnerHtml = "No Records Found";
            }
        }
        catch (Exception ex)
        {
            // Response.Redirect("404.html", false);
            throw ex;
        }

    }
}