﻿<%@ Page Title="" Language="C#" MasterPageFile="cpc.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="Server">
    <style>
        img[usemap] {
            border: none;
            height: auto;
            max-width: 100%;
            width: auto;
        }


        .trs {
            background-color: #ff00ff !important;
            text-align: center;
            color: #fff !important;
        }

        .kutami {
            background-color: #008000 !important;
            text-align: center;
            color: #fff !important;
        }

        .bjp {
            background-color: #ff6600 !important;
            text-align: center;
            color: #fff !important;
        }

        .other {
            background-color: #7030a0 !important;
            text-align: center;
            color: #fff !important;
        }


        .trstxt {
            color: #ff00ff !important;
            font-size: 16px !important;
            font-weight: bold !important;
        }

        .kutamitxt {
            color: #008000 !important;
            font-size: 16px !important;
            font-weight: bold !important;
        }

        .bjptxt {
            color: #ff6600 !important;
            font-size: 16px !important;
            font-weight: bold !important;
        }

        .othertxt {
            color: #7030a0 !important;
            font-size: 16px !important;
            font-weight: bold !important;
        }

        .color1 {
            display: inline-block;
            line-height: 33px;
            color: #000;
            font-size: 16px;
            font-weight: bold;
            text-align: left;
            width: 84px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="Server">

    <div class="container">
        <div class="col-md-5">
            <img src="img/tel.png" width="500" height="426" usemap="#Map">
            <map name="Map">
                <area shape="poly" href="voters.aspx?id=2" alt="adilabad" coords="121,120,114,116,109,112,105,107,110,98,112,85,109,77,115,76,131,85,141,84,141,71,151,66,153,55,153,44,160,37,153,25,156,22,167,33,179,32,195,33,201,43,210,48,221,55,235,48,245,56,256,58,273,61,282,51,293,55,305,65,306,79,302,91,300,102,303,112,304,127,306,137,295,135,286,140,279,147,275,139,265,136,255,128,250,124,246,129,239,125,232,122,229,118,224,108,209,104,201,108,192,107,188,109,183,111,176,115,170,115,167,113,161,114,156,113,151,114,144,112,141,109,128,118,124,122,119,121" />
                <area shape="poly" href="voters.aspx?id=3" alt="nizamabad" coords="83,178,82,167,87,160,89,154,98,152,100,144,103,138,107,134,110,128,119,125,130,117,140,112,160,114,173,115,175,122,177,130,180,135,184,140,182,150,171,160,172,171,173,175,178,182,179,189,171,188,165,189,153,188,146,190,142,185,137,190,139,195,128,194,122,191,121,185,106,184,96,181,92,175" />
                <area shape="poly" href="voters.aspx?id=4" alt="karimnagar" coords="183,182,189,179,198,181,206,180,214,188,215,199,215,202,226,204,235,205,243,205,251,201,256,198,267,194,269,191,269,177,268,166,271,161,281,156,287,161,296,164,294,158,299,156,311,154,314,160,317,167,324,171,327,166,326,162,329,155,338,150,335,145,333,137,324,139,321,142,314,138,306,138,282,144,273,142,255,129,238,125,227,116,218,106,197,107,187,108,173,115,176,127,178,135,184,140,182,148,172,159,171,169,174,176,178,182" />
                <area shape="poly" href="voters.aspx?id=5" alt="medak" coords="82,181,88,177,91,177,100,181,113,184,125,193,136,197,138,194,141,184,149,189,161,189,168,188,176,188,180,184,188,181,206,181,212,183,214,192,214,199,206,207,201,213,201,218,204,226,191,236,178,233,167,233,155,239,153,248,148,252,148,256,138,254,131,250,128,253,122,256,114,246,106,241,99,241,92,250,89,251,86,248,79,243,75,239,76,230,82,227,80,221,86,211,85,203,80,200,89,194,87,185" />
                <area shape="poly" href="voters.aspx?id=6" alt="warangal" coords="202,220,203,212,213,202,229,204,246,204,259,198,271,193,270,173,270,163,280,158,289,163,295,163,303,156,314,159,320,168,328,170,329,158,336,152,340,151,339,160,347,164,351,172,358,178,365,184,370,190,365,194,355,200,337,202,331,214,330,226,322,232,315,233,310,239,314,246,319,253,321,263,310,268,302,265,296,264,290,269,282,262,272,253,269,249,261,254,252,251,246,249,243,244,236,244,234,241,230,241,226,238,223,233,216,227,210,224" />
                <area shape="poly" href="voters.aspx?id=7" alt="rangareddy" coords="95,280,82,281,65,284,63,278,73,264,82,257,93,248,100,242,106,242,122,253,130,252,139,255,148,256,157,245,160,236,176,231,187,235,191,236,185,243,189,249,190,259,187,265,179,262,170,256,162,262,161,269,165,277,177,281,185,274,187,273,192,283,198,292,192,299,185,302,177,298,167,297,158,294,153,287,150,282,142,285,136,289,130,279,123,281,123,287,117,292,119,299,118,307,113,315,104,311,99,306,103,297,102,287,100,283" />
                <area shape="poly" href="voters.aspx?id=1" alt="hyderabad" coords="163,267,167,261,171,259,178,262,181,268,182,274,176,278,171,278,166,276,161,269" />
                <area shape="poly" href="voters.aspx?id=10" alt="nalgonda" coords="195,312,190,301,197,296,198,288,191,278,191,264,189,249,190,238,204,228,210,223,217,229,226,234,230,239,241,242,253,251,265,251,270,249,278,258,285,264,285,269,285,272,292,275,296,275,298,280,294,284,300,288,305,290,309,295,311,300,309,311,314,315,313,327,301,339,289,332,282,330,274,334,271,335,258,340,256,341,243,345,237,354,232,365,223,355,211,348,202,345,198,343,187,346,184,340,183,333,191,327,197,317" />
                <area shape="poly" href="voters.aspx?id=8" alt="khamam" coords="287,269,296,264,308,268,323,262,315,250,310,238,322,232,332,228,335,211,341,203,357,200,368,193,367,186,356,176,347,166,338,160,339,149,352,145,359,150,367,160,372,167,376,171,378,185,381,190,386,187,388,197,395,191,394,201,396,221,402,226,414,221,429,223,451,223,457,223,461,219,471,215,475,220,460,228,451,234,444,246,444,261,436,268,427,265,418,274,413,281,402,282,391,284,385,288,385,295,381,298,369,295,362,289,354,292,349,299,339,294,338,304,346,306,352,308,359,313,358,321,348,321,333,309,325,304,320,296,312,304,304,291,290,283,294,278,286,273" />
                <area shape="poly" href="voters.aspx?id=9" alt="mahbubnagar" coords="70,314,73,306,72,297,68,287,77,283,85,281,95,279,99,285,103,295,98,307,116,314,120,304,120,293,125,286,136,288,148,282,156,293,172,295,189,304,192,317,186,333,188,344,200,346,226,356,230,364,228,374,218,374,210,378,203,388,196,388,191,395,177,390,166,391,146,391,144,405,134,408,130,409,122,408,113,406,105,410,94,406,85,404,69,398,70,381,77,368,80,363,57,359,46,359,62,343,67,335,72,326,69,316" />
            </map>

        </div>

        <div class="col-md-7">
            <div class="text-content">
                <div class="typing-title" style="display: none;">

                    <h3>
                        <a href="" class="typewrite" data-period="800" data-type='[ "Adilabad", "Nizamabad", "Karimnagar", "Medak", "warangal", "rangareddy", "hyderabad", "nalgonda", "khamam", "mahbubnagar" ]'>
                            <span class="wrap"></span>
                        </a>
                    </h3>
                </div>


                <div class="row">
                    <p>

                        <h4>A visual tool to forecast your own election results </h4>
                        <br />
                        <b>Forecast</b> your own Telangana State Assembly election results with a simple tap. See the current trends on the parties, build your own exit polls and share it with your social network

                    </p>
                </div>

                <div id="divPredTable">
                    <div style="width:300px;">
                           <div class="container" id="alert" visible="true" runat="server">
                        <div class="alert alert-success alert-dismissable" id="alertmod" runat="server">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                            <strong>
                                <asp:Label ID="lblResult" runat="server" Text=""></asp:Label></strong>
                            <asp:Label ID="lblMessage" runat="server" Visible="true"></asp:Label>
                        </div>
                    </div>
                    </div>
                 
                    <asp:Label ID="Label1" runat="server" Visible="true" Text=""></asp:Label>
                    <div class="col-md-6">
                        <div class="box sl1 fadesimple mtb-10">
                            <h3>Your Predict  Seats </h3>
                            <hr>
                            <div class="form-inline">

                                <div class="form-group mtb-5">
                                    <label for="lbltrs" class="trstxt color1">TRS </label>
                                    <asp:TextBox ID="txtTRS" runat="server" CssClass="form-control" placeholder="Enter Value"></asp:TextBox>
                                </div>


                                <div class="form-group mtb-10">
                                    <label for="lblkutami" class="trstxt color1">KUTAMI </label>
                                    <asp:TextBox ID="txtKUTAMI" runat="server" CssClass="form-control" placeholder="Enter Value"></asp:TextBox>
                                </div>


                                <div class="form-group mtb-10">
                                    <label for="lblbjp" class="trstxt color1">BJP </label>
                                    <asp:TextBox ID="txtBJP" runat="server" CssClass="form-control" placeholder="Enter Value"></asp:TextBox>
                                </div>

                                <div class="form-group mtb-10">
                                    <label for="lnlothers" class="trstxt color1">Others </label>
                                    <asp:TextBox ID="txtOthers" runat="server" CssClass="form-control" placeholder="Enter Value"></asp:TextBox>
                                </div>
                                <div class="form-group mtb-10">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-warning btn-send" OnClick="btnSubmit_Click" />
                                    <%--<asp:Button ID="btnSubmit" runat="server" CssClass="form-control" OnClick="btnSubmit_Click">Save</asp:Button>--%>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="box sl1 fadesimple mtb-10">
                        <h3>Total  Seats  - 119 </h3>
                        <hr>

                        <div class="color1 trstxt" style="width: 150px !important;">TRS</div>

                        <asp:Label ID="TRS" runat="server" CssClass="trstxt" Text="Label"></asp:Label>
                        <br />
                        <div class="color1 kutamitxt" style="width: 150px !important;">KUTAMI</div>
                        <asp:Label ID="KUTAMI" runat="server" CssClass="kutamitxt" Text="Label"></asp:Label>
                        <br />

                        <div class="color1 bjptxt" style="width: 150px !important;">BJP</div>
                        <asp:Label ID="BJP" runat="server" CssClass="bjptxt" Text="Label"></asp:Label>
                        <br />
                        <div class="color1 othertxt" style="width: 150px !important;">Others</div>
                        <asp:Label ID="others" runat="server" CssClass="othertxt" Text="Label"></asp:Label>

                    </div>
                    <span id="spSLC">Show All Leading Candiates</span>

                </div>
            </div>
        </div>

        <div class="Col-md-12" id="no-more-tables">
            <div style="margin-top: 20px;" id="divStateList">
                <asp:GridView ID="gvlist" AutoGenerateColumns="false" runat="server" OnRowDataBound="gvlist_RowDataBound" class="table table-striped custab" OnSelectedIndexChanged="gvlist_SelectedIndexChanged">
                    <Columns>
                        <asp:TemplateField HeaderText="Constituency No.">
                            <ItemTemplate>
                                <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("CONSISTENCY") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label ID="leadname" ToolTip="Lead Name" runat="server" Text='<%#Eval("Const_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TRS">
                            <ItemTemplate>
                                <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("TRS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="trs" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="KUTAMI">
                            <ItemTemplate>
                                <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("KUTAMI") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="kutami" />

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BJP">
                            <ItemTemplate>
                                <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("BJP") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="bjp" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Others">
                            <ItemTemplate>
                                <asp:Label ID="email" ToolTip="Email" runat="server" Text='<%#Eval("OTHERS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" CssClass="other" />
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

                <%-- <div class="row">
    
                <b class="mtb-10">Click on each district for individual constituency results</b>
                    </div>--%>
            </div>
        </div>

    </div>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="Server">
    <script type="text/javascript">
        window.onload = function () {
            var seconds = 4;
            setTimeout(function () {
                document.getElementById("<%=alert.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script type="text/javascript">
        $(document).ready(function (e) {
            $('img[usemap]').rwdImageMaps();
        });

        $(document).ready(function () {
           <%-- $(<%gvlist%>).click(function () {

            });--%>
            $("#spSLC").click(function () {
                $("#divStateList").slideToggle(1000);
            });
        });


    </script>

</asp:Content>



