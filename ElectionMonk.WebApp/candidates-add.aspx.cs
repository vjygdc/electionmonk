﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    Candidates objcandidates = new Candidates();
    CandidatesBAL objcandidatesbal = new CandidatesBAL();
    oConstituencies objcons = new oConstituencies();
    constitutionBAL consbal = new constitutionBAL();
    
    partyBAL parbal = new partyBAL();
    oParty objpar = new oParty();
    string a, b;
    protected void Page_Load(object sender, EventArgs e)
    {
        alert.Visible = false;
       

        if (Session["newsState"].ToString() == "update")
        {
            if (!IsPostBack)
            {
                bindParty();
                bindCons();

            }
            Get_Candidatess_byid();
            btnadd.Visible = false;
            update.Visible = true;
        }
        else if(Session["newsState"].ToString()== "insert")
        {
            if (!IsPostBack)
            {
                bindParty();
                bindCons();

            }
            btnadd.Visible = true;
            update.Visible = false;
        }
    }

    public void bindParty()
    {
        DataTable dtState = partyDAL.GetPartyName(objpar);
        ddlparty.DataSource = dtState;
        ddlparty.DataTextField = "PARTY_NAME";
        ddlconst.DataValueField = "PARTY_ID";
        //ddlManager.DataValueField = "EMP_ID";
        ddlparty.DataBind();
        ddlparty.Items.Insert(0, new ListItem("-- Select Party--"));

    }
    public void bindCons()
    {
        DataTable dtState = constitutionDAL.GetConsName(objcons);
        ddlconst.DataSource = dtState;
        ddlconst.DataTextField = "CONSTI_NAME";
        ddlconst.DataValueField = "CONSTI_ID";
        //ddlManager.DataValueField = "EMP_ID";
        ddlconst.DataBind();
        ddlconst.Items.Insert(0, new ListItem("-- Select Constituences--"));

    }

    protected void btnadd_ServerClick(object sender, EventArgs e)
    {
        int result;
        string adddate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + "_";
        string filename;
       
        try
        {
                if (AsyncFileUpload1.HasFile)
                {
                    string strpath = System.IO.Path.GetExtension(AsyncFileUpload1.PostedFile.FileName);
                    string filewotextension = Path.GetFileNameWithoutExtension(AsyncFileUpload1.FileName);
                    filename = filewotextension + adddate + strpath;
                    decimal size = Math.Round(((decimal)AsyncFileUpload1.PostedFile.ContentLength / (decimal)1024), 2);
                    AsyncFileUpload1.PostedFile.SaveAs(Server.MapPath("~/candi/") + filename);
                    filename = "../candi/" + filename;
                    if (strpath == ".jpg" || strpath == ".png" || strpath == ".jpeg" || strpath == ".JPG" || strpath == ".JPEG" || strpath == ".gif")
                    {
                        System.Drawing.Image imgFile = System.Drawing.Image.FromStream(AsyncFileUpload1.PostedFile.InputStream);
                        if (imgFile.PhysicalDimension.Width < 300 && imgFile.PhysicalDimension.Height < 300 && size < 200)
                        {
                        Response.Write("<script>alert('" + ddlparty.DataValueField + "')</script>");
                        objcandidates.CANDIDATE_NAME = txtcandidatename.Text;
                        objcandidates.CANDIDATE_IMG = filename;
                        objcandidates.Partyname = ddlparty.SelectedValue;
                        objcandidates.CONSTI_ID = Convert.ToInt32(ddlconst.SelectedValue);
                        objcandidates.STATUS = 1;
                        objcandidates.CREA_BY1 = "ADMIN";
                        objcandidatesbal.InsertCandidate(objcandidates);
                        
                            //clear();
                            alertmod.Style.Add("background-color", "#d7ecc6");
                            alert.Style.Add("background-color", "#d7ecc6");
                            Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
                            Label6.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
                            Label5.Text = "Success!";
                            Label6.Text = "Candidate has been registered";
                            alert.Visible = true;
                            llberrormsg.Text = string.Empty;

                        }
                        else
                        {
                            llberrormsg.Visible = true;
                            llberrormsg.Text = "Image resolution should be 300*300 and lessthan 200kB";
                        }
                    }
                    else
                    {
                        lblMesg.ForeColor = System.Drawing.ColorTranslator.FromHtml("orange");
                        lblMesg.Text = "Upload only images";
                    }

                }
                else
                {
                    llberrormsg.Visible = true;
                    llberrormsg.Text = "Please upload image";
                }
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void Get_Candidatess_byid()
    {
        

        bindParty();
        bindCons();
        imgdiv.Visible = false;
        objcandidates.CANDIDATE_ID = Convert.ToInt16(Session["id"]);

        objcandidatesbal.EditCandidate(objcandidates);
        txtcandidatename.Text = objcandidates.CANDIDATE_NAME;
        string hi = objcandidates.Constname;
        ddlconst.SelectedItem.Text = objcandidates.Constname;
        ddlparty.SelectedItem.Text = objcandidates.Partyname;
        

    }




  
   

    public void Updatecandidate()
    {
        try
        {
           
            string filename;
            string adddate = DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss") + "_";
            objcandidates.CANDIDATE_ID = Convert.ToInt16(Session["id"]);

            //filename = Label10.Text;
            objcandidates.Partyname = ddlparty.SelectedValue;
            objcandidates.Constname = ddlconst.SelectedValue;
            objcandidates.UPDA_BY = "admin";
            objcandidatesbal.UpdateCandidate(objcandidates);
          //  objcandidatesbal.UpdateCandidate(objcandidates);
                alertmod.Style.Add("background-color", "#d7ecc6");
                alert.Style.Add("background-color", "#d7ecc6");
                Label5.ForeColor = System.Drawing.ColorTranslator.FromHtml("green");
                Label6.ForeColor = System.Drawing.ColorTranslator.FromHtml("black");
                Label5.Text = "Success!";
                Label6.Text = "Record has been updated";
                alert.Visible = true;
                // clear();
            
            //string update = Convert.ToString(DateTime.Now);
            


            }
            catch (Exception ex)
            {
                throw ex;
            }
    }





    protected void update_ServerClick(object sender, EventArgs e)
    {
        Updatecandidate();
    }
}