﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    Consistency consistency = new Consistency();
    StateBAL stateBAL = new StateBAL();
    ConsistencyBAL bAL = new ConsistencyBAL();
    int state, district;
    public int[] intArray;
    string str;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        alert.Visible = false;
        if (!Page.IsPostBack == true)
        {
            BindState();
            Count();

        }
        if (Session["newsState"].ToString() == "update")
        {
            BindState();
            BindDistrict();
            Bindconst();
            Editconsistency();
            btnSubmit.Visible = false;
            btnupdate.Visible = true;
        }
        else if (Session["newsState"].ToString() == "insert")
        {
            btnupdate.Visible = false;
            btnSubmit.Visible = true;
        }
    }

    public void BindState()
    {
        oState oState = new oState();
        DataTable dtmodeofinterview = ConsistencyBAL.Getstates(oState);
        ddlstate.DataSource = dtmodeofinterview;
        ddlstate.DataTextField = "STATE_NAME";
        ddlstate.DataValueField = "STATE_ID";
        ddlstate.DataBind();
        ddlstate.Items.Insert(0, new ListItem("--select  State--"));

    }

    public void BindDistrict()
    {
        oDistrict oState = new oDistrict();
        oState.STATE_ID = state;//int.Parse( lblstate.Text);
        DataTable dtmodeofinterviewdist = ConsistencyBAL.Getdistricts(oState);
        ddldist.DataSource = dtmodeofinterviewdist;
        ddldist.DataTextField = "DISTRICT_NAME";
        ddldist.DataValueField = "DISTRICT_ID";
        ddldist.DataBind();

        ddldist.Items.Insert(0, new ListItem("--select District--"));
    }


    public void Bindconst()
    {
        oConstituencies oState = new oConstituencies();
        oState.DISTRICT_ID = district;//Convert.ToInt16(Session["dist_id"]);//1;
        DataTable dtmodeofinterviewconst = ConsistencyBAL.Getconsist(oState);
        ddlconst.DataSource = dtmodeofinterviewconst;
        ddlconst.DataTextField = "CONSTI_NAME";
        ddlconst.DataValueField = "CONSTI_ID";
        ddlconst.DataBind();
        ddlconst.Items.Insert(0, new ListItem("--select Consistency--"));
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        consistency.DISTRICT = Convert.ToInt16(ddldist.SelectedItem.Value);
        consistency.STATE = Convert.ToInt16(ddlstate.SelectedValue);
        consistency.CONSISTENCY = Convert.ToInt16(ddlconst.SelectedItem.Value);
        consistency.TRS = txttrs.Text;
        consistency.KUTAMI = txtkutami.Text;
        consistency.BJP = txtbjp.Text;
        consistency.OTHERS = txtothers.Text;
        consistency.Status = ddlstatus.Text;
        consistency.Statename = ddlstate.SelectedItem.Text;
        consistency.Dist_name = ddldist.SelectedItem.Text;
        consistency.Const_name = ddlconst.SelectedItem.Text;
        bAL.Consistency_insert(consistency);
        alert.Visible = true;
        alertlabel.Text = "Saved Successfully";
    }

    protected void ddlstate_SelectedIndexChanged1(object sender, EventArgs e)
    {
        state = Convert.ToInt16(ddlstate.SelectedItem.Value);
        BindDistrict();
    }

    protected void ddldist_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //Session["dist_id"] = Convert.ToInt16(ddldist.SelectedItem.Value);
        district = Convert.ToInt16(ddldist.SelectedItem.Value);
        Bindconst();
    }
    public void Count()
    {
        int k;
        string[] stringArray = { "TDP", "TRS", "KUTAMI", "BJP", "others" };
        intArray = new int[stringArray.Length];
        for (int i = 0; i < stringArray.Length - 1; i++)
        {
            consistency.Status = stringArray[i];
            bAL.GetPartyCount(consistency);
            intArray[i] = consistency.Count;
        }
        // TDP.Text = intArray[0].ToString();
        //TRS.Text = intArray[1].ToString();
        //KUTAMI.Text = intArray[2].ToString();
        //BJP.Text = intArray[3].ToString();
        //others.Text = intArray[4].ToString();
    }

    public void Editconsistency()
    {
        consistency.ID = Convert.ToInt16(Session["ID"]);
        bAL.Editconsistency(consistency);
        ddlstate.SelectedItem.Text = consistency.Statename;
        ddldist.SelectedItem.Text = consistency.Dist_name;
        ddlconst.SelectedItem.Text = consistency.Const_name;
        txttrs.Text = consistency.TRS;
        txtbjp.Text = consistency.BJP;
        txtkutami.Text = consistency.KUTAMI;
        txtothers.Text = consistency.OTHERS;
        ddlstatus.SelectedValue = consistency.Status;
        ddlstatus.SelectedValue = consistency.Status;
        ddlstate.SelectedItem.Value =consistency.STATE.ToString();
        ddldist.SelectedItem.Value = consistency.DISTRICT.ToString();
        ddlconst.SelectedItem.Value = consistency.CONSISTENCY.ToString();


    }

    public void Update()
    {
        consistency.ID = Convert.ToInt16(Session["ID"]);
        str = txtothers.Text;
        consistency.TRS = txttrs.Text;
        consistency.KUTAMI = txtkutami.Text;
        consistency.BJP = txtbjp.Text;
        consistency.OTHERS = txtothers.Text;
        consistency.Statename = ddlstate.SelectedItem.Text;
        consistency.Dist_name = ddldist.SelectedItem.Text;
        consistency.Const_name = ddlconst.SelectedItem.Text;
        consistency.Status = ddlstatus.SelectedValue;
        bAL.Consistency_update(consistency);
        alert.Visible = true;
        alertlabel.Text = "Updated Successfully";
    }
    protected void btnupdate_Click(object sender, EventArgs e)
    {
        Update();
    }

   
}