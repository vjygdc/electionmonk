﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for partyBAL
/// </summary>
public class partyBAL
{
    public partyBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int InsertParty(oParty objParty)
    {
        return (new partyDAL()).InsertParty(objParty);
    }
    public List<oParty> GetPartyList()
    {
        return (new partyDAL()).GetPartyList();
    }
    public void DeleteParty(oParty objParty)
    {
        new partyDAL().DeleteParty(objParty);
    }
    public int UpdateParty(oParty objParty)
    {
        return (new partyDAL()).UpdateParty(objParty);
    }
    public void GetPartyByID(oParty objParty)
    {
        new partyDAL().GetPartyByID(objParty);
    }
    public static DataTable GetPartyName(oParty objpar)
    {
        return partyDAL.GetPartyName(objpar);
    }

}