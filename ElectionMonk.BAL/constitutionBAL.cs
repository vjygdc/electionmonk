﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for constitutionBAL
/// </summary>
public class constitutionBAL
{
    public constitutionBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static DataTable GetConsName(oConstituencies objcons)

    {
        return constitutionDAL.GetConsName(objcons);
    }


    public void Delete_city(oConstitution oConstitution)
    {
        new constitutionDAL().Delete_City(oConstitution);
    }
    public List<oConstitution> Getcity()
    {
        return (new constitutionDAL()).GetCityList();
    }
    public List<oConstitution> GetCity_Search(oConstitution oCity)
    {
        return (new constitutionDAL()).GetCitiesBySearch(oCity);
    }
    //public List<oConstitution> Getstate()
    //{
    //    return (new constitutionDAL()).Getstate();
    //}

    public void GetStatebyid(oConstitution objstate)
    {
        new constitutionDAL().GetStateByID(objstate);
    }

    public void GetCitybyid(oConstitution objcity)
    {
        new constitutionDAL().GetCityByID(objcity);
    }

    public int UpdateCity(oConstitution objcity, oConstitution oState)
    {
        return (new constitutionDAL()).Updatecity(objcity, oState);
    }

    public int InsertConstituency(oConstitution obj)
    {
        return (new constitutionDAL()).InsertConstituency(obj);
    }

    //public List<oDistrict> Getstatelist()
    //{
    //    return (new constitutionDAL()).Getstatelist();
    //}
    //public List<oCountry> getc()
    //{
    //    return (new cityDAL()).GetCountry();
    //}


}