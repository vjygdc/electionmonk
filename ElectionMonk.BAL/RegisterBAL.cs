﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for RegisterBAL
/// </summary>
public class RegisterBAL
{
    public RegisterBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Register(Register entities)
    {
        new RegisterDAL().RegisterInsert(entities);
    }


    public void Mail(Register entities)
    {
        new RegisterDAL().MailsendtoJobseeker(entities);
    }

    public int Userexistence(Register register)
    {
        return (new RegisterDAL()).Userexistence(register);
    }
}