﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IndexBAL
/// </summary>
public class IndexBAL
{
	public IndexBAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public List<oConstituencies> Displayrequirementview(oConstituencies entity)
    {
        return (new indexDAL()).Displayrequirementview(entity);
    }
    public void Displaydistrictview(oDistrict entity)
    {
       (new indexDAL()).Displaydistrictview(entity);
    }
    public int InsertUserPrediction(oUserPrediction objUP)
    {
        return (new indexDAL()).InsertUserPrediction(objUP);
    }
    public void GetUserPredictionValuesByID(oUserPrediction objUP)
    {
        (new indexDAL()).GetUserPredictionValuesByID(objUP);
    }
    public int GetLoginCount(oUserPrediction objUP)
    {
        return (new indexDAL()).GetLoginCount(objUP);

    }
}