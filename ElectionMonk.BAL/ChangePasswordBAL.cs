﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChangePasswordBAL
/// </summary>
public class ChangePasswordBAL
{
    public ChangePasswordBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void Edit(Register objedit)
    {
        new ChangePasswordDAL().Edit(objedit);
    }
    public int UpdatePassword(Register objChangePWD)
    {
        return (new ChangePasswordDAL()).UpdatePassword(objChangePWD);
    }
}