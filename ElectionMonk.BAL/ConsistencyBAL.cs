﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConsistencyBAL
/// </summary>
public class ConsistencyBAL
{
    public ConsistencyBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    //public static DataTable GetConList(oConstituencies obj)
    //{
    //    return ConsistencyDAL.GetconList(obj);
    //}
    public List<Consistency> GetUserconsistencies(Consistency consistency)
    {
        return (new UserDAL()).RetrieveUserconsistencies(consistency);
    }

    public void Edit(Consistency objedit)
    {
        new ConsistencyDAL().Edit(objedit);
    }
    public List<Consistency> GetConsistencies(oConstituencies obj)
    {
        return (new ConsistencyDAL()).GetConsistencies(obj);
    }


    public void Consistency_insert(Consistency entities)
    {
        new ConsistencyDAL().Insert(entities);
    }

    public void Consistency_delete(Consistency entities)
    {
        new ConsistencyDAL().DeleteConsistencyyy(entities);
    }

    public void iNSERTGridview(Consistency ocon)
    {
        new UserDAL().InsertGridview(ocon);
    }


    public void Editconsistency(Consistency ocon)
    {
        new ConsistencyDAL().EditConsistency(ocon);
    }

    public void Consistency_update(Consistency entities)
    {
        new ConsistencyDAL().UpdateConsistency(entities);
    }

    public static DataTable Getstates(oState omode)
    {
        return ConsistencyDAL.Getstates(omode);
    }


    public static DataTable Getdistricts(oDistrict omode)
    {
        return ConsistencyDAL.Getdist(omode);
    }


    public static DataTable Getconsist(oConstituencies omode)
    {
        return ConsistencyDAL.GetConsistency(omode);
    }
    public void GetPartyCount(Consistency ocon)
    {
        new ConsistencyDAL().GetCountParties(ocon);
    }
    public void GetPartyCountByDistrict(Consistency ocon)
    {
        new ConsistencyDAL().GetCountPartiesByDistrict(ocon);
    }


    public List<Consistency> Getstateslist(Consistency consistency)
    {
        return (new ConsistencyDAL()).RetrieveSTATES(consistency);
    }
}