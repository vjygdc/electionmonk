﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ElectionMonk.Common.Entities;

/// <summary>
/// Summary description for CandidatesBAL
/// </summary>
public class CandidatesBAL
{
    public CandidatesBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public List<Consistency> Get_follow_upLeads(Consistency consistency)
    {
        return (new ConsistencyDAL()).Retrieveconsistencies(consistency);
    }


    public List<Candidate> GetCandidates()
    {
        return (new CandidatesDAL().GetCandidates());
    }
    public int InsertCandidate(Candidate can)
    {
        return ( new CandidatesDAL().InsertCandidate(can));
    }
    public void EditCandidate(Candidate can)
    {
        new CandidatesDAL().Edit(can);
    }
    public void UpdateCandidate(Candidate can)
    {
        new CandidatesDAL().UpdateCandidates(can);
    }
}