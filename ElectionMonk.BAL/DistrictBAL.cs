﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DistrictBAL
/// </summary>
public class DistrictBAL
{
    public DistrictBAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable GetStateList(oState objState)
    {
        return DistrictDAL.GetStateList(objState);
    }

    public List<oDistrict> GetDistrictList()
    {
        return (new DistrictDAL()).GetDistrictList();
    }

    public void InsertDistrict(oDistrict objDis)
    {
        new DistrictDAL().InsertDistrict(objDis);
    }
    public void GetDistrictDetailsByID(oDistrict objDis, oState objState)
    {
        new DistrictDAL().GetDistrictDetailsByID(objDis, objState);
    }
    public void UpdateDistrict(oDistrict objDis)
    {
        new DistrictDAL().UpdateDistrict(objDis);
    }
}